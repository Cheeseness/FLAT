# 0. CONTENTS

1. Introduction
2. Building from source
3. Story
4. Controls
5. Launch options
6. Credits
7. Resources
8. Known Issues
9. Dependencies



# 1. INTRODUCTION

Hey and thanks for checking out FLAT!

FLAT was created by Johan "SteelRaven7" Hassel, Josh "Cheeseness" Bush, Johannes "jo_shadow" Peter, and Anton Riehl for the 7 Day FPS Challenge 2012.

Inspired by early first person shooters, arcade genres and popular sci-fi, we aimed to create a unique and hopefully memorable gameplay experience.

In the week following the conclusion of the 7 Day FPS Challenge, we spent time polishing numerous gameplay aspects and adding additional audio to the game. At this stage, we still consider it a work in progress, but we believe that it is enjoyable and playable in its current state.

A month or so after the 7DFPS challenge ended, we released FLAT under the GPL. Since then, the game has received a number of contributions, including a port to the OpenPandora handheld console. You can find FLAT's source here: https://gitlab.com/Cheeseness/FLAT

Thanks again, and don't hesitate to leave feedback if you feel like it!

/The FLAT Team



# 2. BUILDING FROM SOURCE

FLAT uses `cmake` to automatically build the required makefiles, so `cmake` is a required dependency if you want to build FLAT from source using this method. The libraries and headers for the dependencies listed in section 9 area required as well.

In the root directory (FLAT):

`$ mkdir build && cd build`

`$ cmake ../`

`$ make`

The binary should be located in a `FLAT_{platform}` folder (eg: `FLAT_Linux` after compilation).



# 3. STORY

You play as the last remaining individual of a race who has fallen under the mind control of the evil Pulse Dragon. Marooned on a crystaline world, you must fight your way through hordes of your own kind to slay the mighty Pulse Dragon and free your people.

(Seriously, we didn't have a lot of time to flesh out story ♥)



# 4. CONTROLS

Look around using the mouse, arrow keys, or right stick. See the in-game Bindings screen in the Settings menu for current bindings and customsiation options.

Hold Forward and alternate between Left/Right to move forward (think ice-skating!).
Holding Backward allows you to accelerate backwards in the same manner.
When you have gained sufficient speed, you can "carve" or slide left/right by holding down Left or Right for longer periods.

Holding Left or Right and pressing space allows you to jump and dodge projectiles.

You can pick up new weapons and health kits from supply capsules. They play a sound when they spawn, so keep an eye out for the white beam that indicates one. Pressing Heal will use a health kit.

Press Fire to shoot.

The Escape key, or back button will pause the game.



# 5. LAUNCH OPTIONS

There are a number of launch options available. Most of these are configurable via the Settings menu in-game. Note that all of these modify persistent settings.

## Rendering launch options

* `--width N` Specifies the horizontal resolution for the game's window (where N is width in pixels, defaulting to 1280)
* `--height N` Specifies the vertical resolution for the game's window (where N is height in pixels, defaulting to 720)

## Audio launch options

* `--global-volume N` Specifies the desired volume for the application (where N is desired volume between 0 and 10, defaulting to 10)
* `--music-volume N` Specifies the desired volume for music (where N is desired volume between 0 and 10, defaulting to 7)

## Gameplay launch options

* `--turn-sensitivity N` Specifies the desired sensitivity for turning (where N is desired sensitivity between 1 and 10, defaulting to 6)
* `--auto-heal` Causes a health kit to be automatically consumed if available when dying
* `--hints N` Specifies whether hints should be enabled (where 0 is disabled and 1 is enabled, defaulting to 1)

## Debug launch options

* `--show-fps` Shows the FPS counter

## Joystick launch options

* `--disable-joystick` Disables joystick and gamepad input
* `--joystick-index N` Specifies the joystick or gamepad to use (where N is the enumerated device index, defaulting to 0)
* `--joystick-deadzone N` Specifies the joystick or gamepad to use (wwhere N is desired deadzone sensitivity between 1 and 100, defaulting to 50)

## Experimental launch options

* `--fps N` Specifies the target frame rate for the game (where N is the number of frames per second)


# 6. CREDITS

You can find us on twitter:
* Johan "SteelRaven7" Hassel (code): @SteelRaven7
* Josh "Cheeseness" Bush (3D art + bit of code + bit of audio): @ValiantCheese
* Johannes "jo_shadow" Peter (concept art + audio): @jo_shadow
* Anton Riehl (music): @antonriehl

* Special thanks to @slime73 for assistance with MacOS builds!
* Special thanks to aaorris
* Special thanks to Trent Gamblin for upstream fixes and misc contributions
* Special thanks to ptitSeb for the OpenPandora port



# 7. RESOURCES

FLAT includes the Orbitron and Bitstream Vera Sans fonts. Licence details for each of these can be found in assets/fonts/ folder.

The following sound files were created with or from modified versions of existing samples, available under Creative Commons or in the Public Domain. Details of each can be found in an accompanying text file of the same name.

* assets/sfx/dragon_ambience.ogg
* assets/sfx/dragon_death.ogg
* assets/sfx/dragon_pain.ogg
* assets/sfx/dragon_roar_1.ogg
* assets/sfx/dragon_roar_2.ogg
* assets/sfx/pickup_spawn.ogg
* assets/sfx/shotgun.ogg
* assets/sfx/smg.ogg
* assets/sfx/zap_2.ogg

The Blender material used for FLAT's crystals came from the Blender Open Material Repository, via Markus Rothe's free Sweblend Material Library vol.1
http://matrep.parastudios.de/index.php?action=view&material=290-crystal



# 8. KNOWN ISSUES

* Object reflections do not occlude more distant objects



# 9. DEPENDENCIES

FLAT uses Allegro 5 ( http://alleg.sourceforge.net/ ). You will need the following packages.

Please note that the current Linux build of FLAT is 64 bit only. Historic 32 bit builds may require additional 32 bit compatibility libraries when run on a 64 bit system.

allegro5
allegro5-addon-ttf
allegro5-addon-audio
allegro5-addon-acodec
allegro5-addon-image
