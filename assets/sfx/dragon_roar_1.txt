dragon_roar_1.ogg is made from of samples including the following external sources

"Turbine Room.wav" by Sea Fury
CC BY 3.0 http://creativecommons.org/licenses/by/3.0/
https://freesound.org/people/Sea%20Fury/sounds/54638/

"Zap.wav" by Sea Fury
CC BY 3.0 http://creativecommons.org/licenses/by/3.0/
https://freesound.org/people/Sea%20Fury/sounds/48711/

"Melt down 1.wav" by chimerical
CC BY-NC 3.0 http://creativecommons.org/licenses/by-nc/3.0/
https://freesound.org/people/chimerical/sounds/103687/

"HF Computer Hum C.wAv" by hello_flowers
Public domain
https://freesound.org/people/hello_flowers/sounds/32522/
