/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIScreen.h"
#include "Game.h"

GUIScreen::GUIScreen()
{
	backgroundImage = NULL;
	focusedWidget = -1;
	clickedWidget = -1;
	clickReady = false;
	defaultFocusWidget = -1;
	dirty = true;
	closing = false;
}

GUIScreen::GUIScreen(ALLEGRO_BITMAP* image)
{
	backgroundImage = image;
	focusedWidget = -1;
	clickedWidget = -1;
	clickReady = false;
	defaultFocusWidget = -1;
	dirty = true;
	closing = false;
}

GUIScreen::~GUIScreen()
{
	closing = true;
	for (unsigned int i = 0; i < widgets.size(); i++)
	{
		delete widgets[i];
	}
	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{
		delete focusableWidgets[i];
	}
	widgets.clear();
	focusableWidgets.clear();
}

void GUIScreen::draw(vec2 aspectMultiplier)
{
	if (closing || !dirty)
	{
		return;
	}

	if (backgroundImage != NULL)
	{
		al_draw_bitmap(backgroundImage, 0, 0, 0);
	}
	for (unsigned int i = 0; i < widgets.size(); i++)
	{
		widgets[i]->draw(aspectMultiplier);
	}
	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{
		focusableWidgets[i]->draw(aspectMultiplier);
	}
	dirty = false;
}

void GUIScreen::close()
{
	closing = true;
}

bool GUIScreen::isClosing()
{
	return closing;
}

bool GUIScreen::isDirty()
{
	return dirty;
}

void GUIScreen::setDirty(bool d)
{
	dirty = d;
}

void GUIScreen::show()
{
	clickReady = false;
	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{
		focusableWidgets[i]->setFocusState(false);
		focusableWidgets[i]->setHoverState(false);
	}
	focusedWidget = defaultFocusWidget;
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		focusableWidgets[focusedWidget]->setFocusState(true, true);
	}
	dirty = true;
}

void GUIScreen::addWidget(GUIWidget* w)
{
	widgets.push_back(w);
	if (focusableWidgets.size() == 1)
	{
		defaultFocusWidget = 0;
	}
	dirty = true;
}

void GUIScreen::addFocusableWidget(GUIWidget* w)
{
	focusableWidgets.push_back(w);
	if (focusableWidgets.size() == 1)
	{
		defaultFocusWidget = 0;
	}
	dirty = true;
}

void GUIScreen::setDefaultFocusWidget(int i)
{
	if (i >=0 && i < (int)focusableWidgets.size())
	{
		defaultFocusWidget = i;
	}
}

void GUIScreen::setDefaultFocusWidget(GUIWidget* w)
{
	std::vector<GUIWidget*>::iterator it = std::find(focusableWidgets.begin(), focusableWidgets.end(), w);
	if (it != focusableWidgets.end())
	{
		defaultFocusWidget = std::distance(focusableWidgets.begin(), it);
	}
}

void GUIScreen::processMouse(vec2 pos, bool click)
{
	if (closing)
	{
		return;
	}

	if (!click)
	{
		clickReady = true;
		clickedWidget = -1;
	}

	int oldFocusedWidget = focusedWidget;

	if (clickedWidget >= 0)
	{
		vec2 tempMin = focusableWidgets[clickedWidget]->getPositionPlus();
		vec2 tempMax = tempMin + focusableWidgets[clickedWidget]->getSize();
		pos.x = max(pos.x, tempMin.x);
		pos.x = min(pos.x, tempMax.x);
		pos.y = max(pos.y, tempMin.y);
		pos.y = min(pos.y, tempMax.y);

		focusableWidgets[clickedWidget]->checkCoords(pos, true, true);
		dirty = true;
	}
	else
	{
		for (unsigned int i = 0; i < focusableWidgets.size(); i++)
		{
			if (focusableWidgets[i]->checkCoords(pos, click, click))
			{
				if (click && clickReady)
				{
					focusableWidgets[i]->doAction();
					if (Game::getGame()->currentScreen != this)
					{
						return;
					}
					clickedWidget = i;
					focusedWidget = clickedWidget;
				}
			}
			//HACK: This is a quick and dirty way to prevent crashes when hitting the reset button
			if (closing || i > focusableWidgets.size())
			{
				Alert("Current GUIWidget does not exist. GUIScreen has likely been destroyed.");
				return;
			}
			if (focusableWidgets[i]->isDirty())
			{
				dirty = true;
			}
		}
	}

	if (focusedWidget != oldFocusedWidget)
	{
		if (oldFocusedWidget >=0 && oldFocusedWidget < (int)focusableWidgets.size())
		{
			focusableWidgets[oldFocusedWidget]->setFocusState(false);
		}
	}
}

void GUIScreen::setFocusedWidget(int i)
{
	if (i >=0 && i < (int)focusableWidgets.size())
	{
		if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
		{
			focusableWidgets[focusedWidget]->setFocusState(false);
		}
		focusedWidget = i;
		focusableWidgets[focusedWidget]->setFocusState(true);
	}
	dirty = true;
}

void GUIScreen::setFocusedWidget(GUIWidget* w)
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		focusableWidgets[focusedWidget]->setFocusState(false);
	}

	std::vector<GUIWidget*>::iterator it = std::find(focusableWidgets.begin(), focusableWidgets.end(), w);
	if (it != focusableWidgets.end())
	{
		focusedWidget = std::distance(focusableWidgets.begin(), it);
		w->setFocusState(true);
	}
	else
	{
		focusedWidget = -1;
	}
	dirty = true;
}

GUIWidget* GUIScreen::getFocusedWidget()
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		return focusableWidgets[focusedWidget]->getFocusedWidget();
	}
	return NULL;
}

int GUIScreen::getFocusedWidgetIndex()
{
	return focusedWidget;
}

void GUIScreen::doFocusedAction()
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		focusableWidgets[focusedWidget]->doAction();
		if (Game::getGame()->currentScreen != this)
		{
			return;
		}
		dirty = true;
	}
}


void GUIScreen::focusNextWidget(int d)
{
	if (focusableWidgets.size() == 0)
	{
		return;
	}

	int tempFocusedWidget = focusedWidget;
	if (focusedWidget < 0)
	{
		 tempFocusedWidget = 0;
	}
	else
	{
		GUIWidget* f = focusableWidgets[focusedWidget];
		if (f->getFocusedWidget() != f)
		{
			if (((GUIScroller*)f)->focusNextWidget(d))
			{
				dirty = true;
				return;
			}
			else
			{
				((GUIScroller*)f)->clearFocusedWidget();
			}
		}

		//TODO: Move this out to its own function for checking before we call focusNextWidget()
		if (d == FOCUS_DIRECTION_LEFT)
		{
			if (f->doLeft())
			{
				if (f->isDirty())
				{
					dirty = true;
				}
				return;
			}
		}
		else if (d == FOCUS_DIRECTION_RIGHT)
		{
			if (f->doRight())
			{
				if (f->isDirty())
				{
					dirty = true;
				}
				return;
			}
		}
	}

	int widgetClose = tempFocusedWidget;
	int widgetWrap = tempFocusedWidget;
	int buffer = 200;

	//Loop through all focusable widgets, and track the closest next and the farthest first widget in the seek direction that align within a buffer distance of perpendicular to the seek direction

	bool forwards = (d == FOCUS_DIRECTION_LEFT || d == FOCUS_DIRECTION_UP) ? false : true;

	int wDir = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[tempFocusedWidget]->getPosition().x : focusableWidgets[tempFocusedWidget]->getPosition().y;
	int wPer = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[tempFocusedWidget]->getPosition().y : focusableWidgets[tempFocusedWidget]->getPosition().x;

	int closeDir = wDir;
	int closePer = wPer;

	int wrapDir = wDir;

	int iPer;
	int iDir;

	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{

		if (focusedWidget == (int) i)
		{
			continue;
		}

		//Get the widget's posiition coordinate in the direction of focus travel and perpendicular to direction
		iDir = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[i]->getPosition().x : focusableWidgets[i]->getPosition().y;
		iPer = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[i]->getPosition().y : focusableWidgets[i]->getPosition().x;

		int dirStart = iPer;
		int dirEnd = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[i]->getPosition().y + focusableWidgets[i]->getSizePlus().y: focusableWidgets[i]->getPosition().x + focusableWidgets[i]->getSizePlus().x;

		if (dirStart <= wPer && dirEnd >= wPer)
		{
			//If the width of the widget in the perpendicular dimensions encompasses the current widget's position, treat that as valid regardless of how far away its position is
			iPer = wPer;
		}

		//If we're moving vaguely in the same direction as the widget
		if (iPer < wPer + buffer && iPer > wPer - buffer && iDir != wDir)
		{
			if ((iDir > wDir) == forwards)
			{
				if (iDir == closeDir)
				{
					if (abs(wPer - iPer) < abs(wPer - closePer))
					{
						widgetClose = i;
						closeDir = iDir;
						closePer = iPer;
					}
				}
				else if ((iDir < closeDir) == forwards || widgetClose == focusedWidget)
				{
					widgetClose = i;
					closeDir = iDir;
					closePer = iPer;
				}
			}
			else
			{
				if ((iDir < wrapDir) == forwards)
				{
					widgetWrap = i;
					wrapDir = iDir;
				}
			}
		}
	}

	if (widgetClose != focusedWidget)
	{
		setFocusedWidget(widgetClose);
	}
	else if (widgetWrap != focusedWidget)
	{
		setFocusedWidget(widgetWrap);
	}
	else
	{
		if (focusedWidget < 0)
		{
			setFocusedWidget(0);
		}
		Print("Can't seek anywhere?");
	}
	GUIWidget* f = focusableWidgets[focusedWidget];
	if (f->getFocusedWidget() != f)
	{
		((GUIScroller*)f)->focusNextWidget(d);
	}
	dirty = true;
}
