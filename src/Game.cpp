/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2012-2023 Josh "Cheeseness" Bush
Copyright 2013 ptitSeb

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cstdio>

#include "Game.h"

Game* Game::instance = NULL;

Game* Game::getGame()
{
	if (instance == NULL)
	{
		instance = new Game();
	}
	return instance;
}

Game::Game()
{
	Print("Running", VERSION_NUMBER);
	if (instance == NULL)
	{
		instance = this;
	}

	al_set_app_name("flat");

	//al_init() must be called before SettingsManager is instantiated
	Print("Initialising Allegro...");
	if(al_init())
	{
		graphicsMan = NULL;
		inputMan = new InputManager();
		settingsMan = new SettingsManager();
		settingsMan->loadSettings();
		settingsMan->loadBindings();

		screenMenu = NULL;
		screenHowToPlay = NULL;
		screenStats = NULL;
		screenSettings = NULL;
		screenBindings = NULL;
		screenPause = NULL;
		screenDeath = NULL;
		screenEnd = NULL;
		screenCredits = NULL;


		testcase = false;
		quit = false;

		paused = false;
		unpausing = false;

		player = NULL;
		dragonHead = NULL;
		floor = NULL;
		world3D = NULL;

		enemySpawnCount = 0;

	}
	else
	{
		Error("Failed to initialize allegro!");
	}
}

Game::~Game()
{
	endGame();

	splatBitmap.clear();
	sparkBitmap.clear();

	delete screenMenu;
	delete screenNew;
	delete screenHowToPlay;
	delete screenStats;
	delete screenSettings;
	delete screenBindings;
	delete screenPause;
	delete screenDeath;
	delete screenEnd;
	delete screenCredits;

	for (unsigned int i = 0; i < RULES_COUNT; i++)
	{
		delete rules[i];
	}

	settingsMan->saveSettings();
	settingsMan->saveBindings();

	delete audioMan;
	delete graphicsMan;
	delete settingsMan;
	delete inputMan;
	delete fpsText;
	delete hintText;
	Print("Exiting.");
}

void Game::setRules(int d)
{
	if (d < 0 || d >= RULES_COUNT)
	{
		Print("Invalid ruleset", d);
		currentRules = RULES_NORMAL;
	}
	Print("Setting rules ", d);
	currentRules = d;
}

void Game::setupRules()
{
	/*EASY*/
	rules[RULES_EASY] = new Rules();
	rules[RULES_EASY]->crateHealthCount = 4;
	rules[RULES_EASY]->crateEnemyCountStep = 2;
	rules[RULES_EASY]->enemyHealth = 0.9f;
	rules[RULES_EASY]->dragonHealth1 = 25.0f;
	rules[RULES_EASY]->dragonHealth2 = 10.0f;
	rules[RULES_EASY]->dragonHealthTail = 0.6f;
	rules[RULES_EASY]->dragonTailLength = 30;
	rules[RULES_EASY]->dragonKBForce = 0.6f;
	rules[RULES_EASY]->dragonKBAir = 4.0f;
	rules[RULES_EASY]->dragonKBAirtime = 0.2f;
	rules[RULES_EASY]->dragonFireTime2 = 2.0f;
	rules[RULES_EASY]->enemyMax = 6;
	rules[RULES_EASY]->enemyMaxBoss = 1;
	rules[RULES_EASY]->dragonProjectileSpeed = 0.8f;
	rules[RULES_EASY]->dragonAttackDamage = 0.2f;



	/*NORMAL*/
	rules[RULES_NORMAL] = new Rules();



	/*CHALLENGING*/
	rules[RULES_CHALLENGING] = new Rules();
	rules[RULES_CHALLENGING]->crateHealthCount = 1;
	rules[RULES_CHALLENGING]->crateWeaponFreq = 75;
	rules[RULES_CHALLENGING]->startingEnemies = 2;
	rules[RULES_CHALLENGING]->crateEnemyCountStep = 4;
	rules[RULES_CHALLENGING]->enemyHealth = 1.65f;
	rules[RULES_CHALLENGING]->dragonProjectileDamage = 0.4f;
	rules[RULES_CHALLENGING]->enemyProjectileDamage = 0.3f;
	rules[RULES_CHALLENGING]->enemyMax = 16;
	rules[RULES_CHALLENGING]->enemyMaxBoss = 3;
	rules[RULES_CHALLENGING]->dragonHealth1 = 50.0f;
	rules[RULES_CHALLENGING]->dragonHealthTail = 1.2f;
	rules[RULES_CHALLENGING]->dragonAttackDamage = 0.45f;



	/*FREE SKATE*/
	rules[RULES_FREE_SKATE] = new Rules();
	rules[RULES_FREE_SKATE]->crateWeaponFreq = -1;
	rules[RULES_FREE_SKATE]->enemyFreq = -1;
	rules[RULES_FREE_SKATE]->crateFreq = -1;
	rules[RULES_FREE_SKATE]->startingCrates = 0;
	rules[RULES_FREE_SKATE]->startingEnemies = 0;
	rules[RULES_FREE_SKATE]->suppressHints = true;


	/*DRAGON*/
	rules[RULES_DRAGON] = new Rules();



	/*DEBUG*/
	rules[RULES_DEBUG] = new Rules();
	rules[RULES_DEBUG]->startingCrates = 3;
	rules[RULES_DEBUG]->startingDragons = 3;
	rules[RULES_DEBUG]->startingEnemies = 5;
/*	rules[RULES_DEBUG]->enemySpeed = 0.0f;
	rules[RULES_DEBUG]->enemySpeedCircle = 0.0f;
	rules[RULES_DEBUG]->enemySpeedChase = 0.0f;
	rules[RULES_DEBUG]->enemySpeedBrake = 0.0f;
	rules[RULES_DEBUG]->enemyFireRange = 0.0f;
*/
}

void Game::setQuit(bool q)
{
	quit = q;
}

void Game::setFPSVisibility(bool v)
{
	if (fpsText != NULL)
	{
		fpsText->setVisibility(v);
	}
}

void Game::setHintVisibility(bool v)
{
	hintText->setVisibility(v);
}

Player* Game::getPlayer()
{
	return player;
}

void Game::processHintState()
{
	if (player != NULL)
	{
		std::stringstream s;
		if (player->healthpacks > 0 && player->passingOut)
		{
			if (hintState != HINT_HEAL)
			{
				hintState = HINT_HEAL;
				s << "Press " << inputMan->getDefaultBoundName(INPUT_HEAL) << " to heal.";
				hintText->setTextCentered(s.str());
				setHintVisibility(true);
			}
			return;
		}
		if (dragonHead == NULL)
		{
			if (!player->hasMoved)
			{
				if (hintState != HINT_MOVE)
				{
					hintState = HINT_MOVE;
					s << "Alternate between " << inputMan->getDefaultBoundName(INPUT_LEFT) << " and " << inputMan->getDefaultBoundName(INPUT_RIGHT) << " whilst holding " << inputMan->getDefaultBoundName(INPUT_FORWARD) + " to skate forwards";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}

			if (player->weaponPickup < 0)
			{
				if (hintState != HINT_PICKUP)
				{
					hintState = HINT_PICKUP;
					s << "Find new weapons and health kits in hidden capsules. Press " << inputMan->getDefaultBoundName(INPUT_HEAL) << " to heal";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}

			if (pickup.size() > 0)
			{
				if (hintState != HINT_PICKUP2)
				{
					hintState = HINT_PICKUP2;
					s << "Find and collect the supply capsule";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}

			if (npc.size() > 0)
			{
				if (hintState != HINT_ENEMY)
				{
					hintState = HINT_ENEMY;
					s << "Watch out for enemy skaters";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}

			if (player->getTime() - lastEventTime >= 5 && !player->isReallyMoving()) //npc.size() == 0 && pickup.size() == 0 //Don't need to check for this since previous hint states return if those are true
			{
				if (hintState != HINT_SPEED)
				{
					hintState = HINT_SPEED;
					s << "Hold " << inputMan->getDefaultBoundName(INPUT_FORWARD) + " and pace yourself to skate faster";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}
		}
		else
		{
			if (dragonHead->stage == 0)
			{
				if (hintState != HINT_DRAGON)
				{
					hintState = HINT_DRAGON;
					s << "Look to the skies for the Pulse Dragon";
					hintText->setTextCentered(s.str());
					setHintVisibility(true);
				}
				return;
			}
		}
	}
	hintState = HINT_NONE;
	setHintVisibility(false);
}

void Game::doStuff() {
	if (player == NULL)
	{
		return;
	}

	vec2 pos = player->getPos();
	float dist = distanceBetween(lastPosition, vec2(0.0f, 0.0f));
	if (!player->hasMoved && dist > 1.5)
	{
		player->hasMoved = true;
	}
	if(safe && dist > SAFE_RADIUS) {
		safe = false;
	}

	if(distanceBetween(lastPosition, pos) < 20 || safe) {
		lastPosition = pos;
		return;
	}

	lastPosition = pos;

	if(!bossSpawned && npc.size() == 0 && pickup.size() == 0)
	{
		if (npc.size() < rules[currentRules]->enemyMax && rand() % 100 < rules[currentRules]->enemyFreq)
		{
			float angle = atan2(player->getVel().y, player->getVel().x);

			float addon = random(1.0f);
			addon *= addon*0.75f;

			angle += random(PI)*(addon+0.25f);

			float distance = ENEMY_SPAWN_DISTANCE + random(ENEMY_SPAWN_DISTANCE_RANDOM);

			unsigned int newEnemyCount = std::min(float(enemySpawnCount), float(rules[currentRules]->enemyMax - npc.size()));
			Print("Spawning new skaters:", newEnemyCount);
			for(unsigned int i = 0; i < newEnemyCount; i++)
			{
				spawnCharacter(vec3(player->getPos().x+cos(angle)*distance, player->getPos().y+sin(angle)*distance, 0.0f), 20.0f);
			}
		}
	}

	//We use negative values to set an upper limit on pickup spawn time
//	if(pickupTimer >= 0) {
		pickupTimer -= PICKUP_TIME_STEP;
//	}

	//Print("Pickup?", pickupTimer);
	if((pickup.size() < 1 && pickupTimer <= 0) || pickupTimer < rules[currentRules]->crateTimeLimit)
	{
		if (!bossSpawned && pickups >= MIN_BOSS_SPAWN_PICKUPS && (rand()%100 < BOSS_SPAWN_CHANCE || pickups >= MAX_BOSS_SPAWN_PICKUPS))
		{
			float angle = atan2(player->getVel().y, player->getVel().x);
			dragonHead = spawnDragon(vec3(player->getPos().x+cos(angle) * BOSS_SPAWN_DISTANCE, player->getPos().y+sin(angle) * BOSS_SPAWN_DISTANCE, 0.0f), 20.0f);
			pickupTimer = rules[currentRules]->crateTime;
		}
		else if (rules[currentRules]->crateFreq > 0 && ((rand()%100 < rules[currentRules]->crateFreq || pickupTimer < rules[currentRules]->crateTimeLimit)))
		{
			float angle = atan2(player->getVel().y, player->getVel().x);

			float addon = random(1.0f);
			addon *= addon*0.5f;

			angle += random(PI)*(addon+0.25f);

			float distance = ENEMY_SPAWN_DISTANCE + random(ENEMY_SPAWN_DISTANCE_RANDOM);

			Print("Spawning pickup");
			pickup.push_back(new Pickup(world3D, vec3(player->getPos().x+cos(angle)*distance +random(10.0f), player->getPos().y+sin(angle)*distance +random(10.0f), 0.0f)));
			pickup.back()->setHumSample(audioMan->sounds[SOUND_PICKUP_HUM]);
			pickup.back()->healthKitCount = rules[currentRules]->crateHealthCount;
			pickup.back()->hasNewWeapon = rules[currentRules]->crateWeaponFreq >= rand() % 100 || pickupsSinceLastWeapon >= 100 / rules[currentRules]->crateWeaponFreq;

			unsigned int maxEnemies;
			if (bossSpawned)
			{
				maxEnemies = rules[currentRules]->enemyMaxBoss + dragonHead->tailSegmentCount + 1;
			}
			else
			{
				maxEnemies = rules[currentRules]->enemyMax;
			}

			if (npc.size() < maxEnemies)
			{
				unsigned int newEnemyCount = std::min(float(enemySpawnCount + rules[currentRules]->crateEnemyCountStep), float(maxEnemies - npc.size()));
				Print("Spawning pickup protectors:", newEnemyCount);
				for(unsigned int i = 0; i < newEnemyCount; i++) {
					spawnCharacter(vec3(player->getPos().x+cos(angle)*distance, player->getPos().y+sin(angle)*distance, 0.0f), 10.0f);
				}
			}
			pickupTimer = rules[currentRules]->crateTime;
		}
	}
}

Character* Game::spawnCharacter(vec3 position, float variance)
{
	Character* c = new Character();
	c->setFrame(generateCharacterSuperContainer());
	c->setPosition(position + vec3(random(variance), random(variance), 0.0f));
	c->setProjectileStarburstBitmap(projectileBitmapStarburst);
	c->setProjectileHaloBitmap(projectileBitmapHalo);
	c->setFireSample(audioMan->sounds[SOUND_ENEMY_FIRE]);
	c->setSkateSample(audioMan->sounds[SOUND_ENEMY_SKATE]);
	c->setPainSample(audioMan->sounds[SOUND_ENEMY_PAIN]);
	c->setDeathSample(audioMan->sounds[SOUND_ENEMY_DEATH]);

	c->projectileDamage = rules[currentRules]->enemyProjectileDamage;
	c->projectileSpeed = rules[currentRules]->enemyProjectileSpeed;
	c->fireTime = rules[currentRules]->enemyFireTime;
	c->fireRange = rules[currentRules]->enemyFireRange;
	c->health = rules[currentRules]->enemyHealth;
	c->speed = rules[currentRules]->enemySpeed;
	c->speedChase = rules[currentRules]->enemySpeedChase;
	c->speedCircle = rules[currentRules]->enemySpeedCircle;
	c->speedBrake = rules[currentRules]->enemySpeedBrake;
	c->surroundDir = random(rules[currentRules]->enemySpeedCircle);

	world3D->push(c);
	npc.push_back(c);

	audioMan->setGameState(GAME_STATE_COMBAT);
	return c;
}

DragonHead* Game::spawnDragon(vec3 position, float variance)
{
	Print("Spawning dragon!");
	DragonHead* d = new DragonHead(world3D, &npc, rules[currentRules]->dragonTailLength, rules[currentRules]->dragonHealthTail);

	d->terminalVelocity = rules[currentRules]->dragonTerminalVelocity1;
	d->accelBase = rules[currentRules]->dragonAccelBase1;
	d->accelExtra = rules[currentRules]->dragonAccelExtra1;
	d->chargeDist = rules[currentRules]->dragonChargeDist;
	d->awareDist = rules[currentRules]->dragonAwareDist;

	d->terminalVelocityFlee = rules[currentRules]->dragonTerminalVelocity2;
	d->accelBaseFlee = rules[currentRules]->dragonAccelBase2;
	d->fireTimeFlee = rules[currentRules]->dragonFireTime2;

	d->health1 = rules[currentRules]->dragonHealth1;
	d->health2 = rules[currentRules]->dragonHealth2;

	d->kbForce = rules[currentRules]->dragonKBForce;
	d->kbAir = rules[currentRules]->dragonKBAir;
	d->kbAirtime = rules[currentRules]->dragonKBAirtime;
	d->kbRange = rules[currentRules]->dragonKBRange;

	d->projectileDamage = rules[currentRules]->dragonProjectileDamage;
	d->projectileSpeed = rules[currentRules]->dragonProjectileSpeed;

	d->attackDamage = rules[currentRules]->dragonAttackDamage;

	d->setPosition(position + vec3(random(variance), random(variance), 0.0f));
	d->setProjectileStarburstBitmap(projectileBitmapStarburst2);
	d->setProjectileHaloBitmap(projectileBitmapHalo2);

	d->setFireSample(audioMan->sounds[SOUND_ENEMY_FIRE]);
	d->setPainSample(audioMan->sounds[SOUND_DRAGON_PAIN]);
	d->setDeathSample(audioMan->sounds[SOUND_DRAGON_DEATH]);

	d->setRoar1Sample(audioMan->sounds[SOUND_DRAGON_ROAR1]);
	d->setRoar2Sample(audioMan->sounds[SOUND_DRAGON_ROAR2]);
	d->setAmbienceSample(audioMan->sounds[SOUND_DRAGON_AMBIENCE]);

	d->setPlayer(player);

	d->display = graphicsMan->getDisplay();

	bossSpawned = true;
	audioMan->setGameState(GAME_STATE_DRAGON);

	return d;
}


Splat* Game::spawnSplat(vector <DrawableBitmap> frameList, vec3 pos)
{
	FrameContainer c = FrameContainer();
	int speedOffset = rand() % 2;

	for(int i = 0; i < 3 + speedOffset; i++) {
		for(unsigned int k = 0; k < frameList.size(); k++) {
			c.push(frameList[i]);
		}
	}

	Splat* s = new Splat(c);
	s->setPosition(pos);

	world3D->push(s);
	splat.push_back(s);

	return s;
}


void Game::openURI(std::string uri)
{
	std::string command = "xdg-open ";

	#ifdef _WIN32
		command = "start ";
	#elif defined __APPLE__
		command = "open ";
	#endif

	command += uri;

	if (system(NULL))
	{
		system(command.c_str());
	}
}

int Game::initialize(int argc, char *argv[]) {

	if (!settingsMan)
	{
		Error(strerror(errno));
		return EXIT_FAILURE;
	}

	//TODO: Windows is a special snowflake and returns false always (possibly because this is a folder and not a file?)
	if (!al_filename_exists("gfx"))
	{
		quit = true;
		Error("Unable to find gfx folder!");
		return EXIT_FAILURE;
	}

	if (!al_filename_exists("gfx/skate_anim_yellow"))
	{
		quit = true;
		Error("Unable to find gfx/skate_anim_yellow folder!");
		return EXIT_FAILURE;
	}

	if (!al_filename_exists("gui"))
	{
		Alert("Unable to find gui folder!");
	}

	if (!al_filename_exists("fonts"))
	{
		Alert("Unable to find fonts folder!");
	}

	if (!al_filename_exists("sfx"))
	{
		Alert("Unable to find sfx folder!");
	}

	if (!al_filename_exists("music"))
	{
		Alert("Unable to find music folder!");
	}

	//TODO: Break arguments/argument processing out into an iteterable list and implement --help
	for(int i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "--width") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				int tempValue = std::atoi(argv[i]);
				if (tempValue > 0)
				{
					settingsMan->setResX((unsigned int)tempValue);
					continue;
				}
			}
			Alert("Bad value for --width. Falling back to default.");
		}
		else if (strcmp(argv[i], "--height") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				int tempValue = std::atoi(argv[i]);
				if (tempValue > 0)
				{
					settingsMan->setResY((unsigned int)tempValue);
					continue;
				}
			}
			Alert("Bad value for --height. Falling back to default.");
		}
		else if (strcmp(argv[i], "--music-volume") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				float tempValue = float(std::atoi(argv[i])) * 0.1f;
				settingsMan->setMusicVolume(tempValue);
				continue;
			}
			Alert("Bad value for --music-volume. Falling back to default.");
		}
		else if (strcmp(argv[i], "--global-volume") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				float tempValue = float(std::atoi(argv[i])) * 0.1f;
				settingsMan->setGlobalVolume(tempValue);
				continue;
			}
			Alert("Bad value for --global-volume. Falling back to default.");
		}
		else if (strcmp(argv[i], "--turn-sensitivity") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				float tempValue = float(std::atoi(argv[i])) * 0.001f;
				settingsMan->setTurnSensitivity(tempValue);
				continue;
			}
			Alert("Bad value for --turn-sensitivity. Falling back to default.");
		}
		else if (strcmp(argv[i], "--auto-heal") == 0)
		{
			settingsMan->setAutoHeal(true);
		}
		else if (strcmp(argv[i], "--hints") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				int tempValue = std::atoi(argv[i]);
				if (tempValue > 0)
				{
					settingsMan->setShowHints(true);
					continue;
				}
				else
				{
					settingsMan->setShowHints(false);
					continue;
				}
			}
			Alert("Bad value for --hints. Falling back to default.");
		}
		else if (strcmp(argv[i], "--show-fps") == 0)
		{
			settingsMan->setShowFPS(true);
		}
		else if (strcmp(argv[i], "--fps") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				int tempValue = std::atoi(argv[i]);
				if (tempValue > 0)
				{
					settingsMan->setFPS(tempValue);
					continue;
				}
			}
			Alert("Bad value for --fps. Falling back to default.");
		}
		else if (strcmp(argv[i], "--disable-joystick") == 0)
		{
			settingsMan->setEnableJoystick(false);
		}
		else if (strcmp(argv[i], "--joystick-index") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				int tempValue = std::atoi(argv[i]);
				if (tempValue > 0)
				{
					settingsMan->setJoystickIndex(tempValue);
					continue;
				}
			}
			Alert("Bad value for --joystick-index. Falling back to default.");
		}
		else if (strcmp(argv[i], "--joystick-deadzone") == 0)
		{
			if (i + 1 < argc)
			{
				i++;
				float tempValue = float(std::atoi(argv[i])) * 0.01f;
				settingsMan->setAxisDeadzone(tempValue);
				continue;
			}
			Alert("Bad value for --joystick-deadzone. Falling back to default.");
		}
		else
		{
			Alert("Unknown argument");
			Alert(argv[i]);
		}
	}
	screenMenu = NULL;
	screenNew = NULL;
	screenHowToPlay = NULL;
	screenStats = NULL;
	screenSettings = NULL;
	screenBindings = NULL;
	screenPause = NULL;
	screenDeath = NULL;
	screenEnd = NULL;
	screenCredits = NULL;
	currentScreen = NULL;


	uiColour = al_map_rgb(255, 255, 255);
	uiColourBad = al_map_rgb(128, 64, 64);
	uiColourActive = al_map_rgb(64, 64, 64);

	graphicsMan = new GraphicsManager(settingsMan->getResX(), settingsMan->getResY());

	graphicsMan->setFullscreen(settingsMan->getFullscreen());
	settingsMan->setFullscreen(settingsMan->getFullscreen());
	audioMan = new AudioManager();
	audioMan->setGlobalVolume(settingsMan->getGlobalVolume());
	audioMan->setMusicVolume(settingsMan->getMusicVolume());
	audioMan->setGameState(GAME_STATE_NORMAL);
	audioMan->duckMusicVolume(true);


	event_queue = NULL;
	updateTimer = NULL;

	inputMan->initialiseJoystick();

	al_set_window_title(graphicsMan->getDisplay(), "FLAT");

	hintFont = NULL;
	buttonFont = NULL;
	settingFont = NULL;
	bindingFont = NULL;
	titleFont = NULL;
	titleSmallFont = NULL;
	headingFont = NULL;
	textFont = NULL;

	fpsText = new DrawableText(hintFont, uiColour);
	fpsText->setText("FPS:");

	hintText = new DrawableText(hintFont, uiColour);
	hintText->setPosition(vec2(NATIVE_RES_X * 0.5, 600.0f));

	currentRules = RULES_NORMAL;
	setupRules();

	setupScreens();
	showScreen(screenMenu);

	return runMainLoop();
}

void Game::setupScreens()
{
	//TODO: Restore current screen after doing this?

	float multiplier = graphicsMan->getMinAspectMultiplier().y;

	if (hintFont != NULL)
	{
		al_destroy_font(hintFont);
	}
	if (buttonFont != NULL)
	{
		al_destroy_font(buttonFont);
	}
	if (settingFont != NULL)
	{
		al_destroy_font(settingFont);
	}
	if (bindingFont != NULL)
	{
		al_destroy_font(bindingFont);
	}
	if (titleFont != NULL)
	{
		al_destroy_font(titleFont);
	}
	if (titleSmallFont != NULL)
	{
		al_destroy_font(titleSmallFont);
	}
	if (headingFont != NULL)
	{
		al_destroy_font(headingFont);
	}
	if (textFont != NULL)
	{
		al_destroy_font(textFont);
	}

	hintFont = al_load_font("fonts/bitstream-vera-sans.ttf", (int)max(14 * multiplier, 14.0f), 0);
	buttonFont = al_load_font("fonts/orbitron-medium.ttf", (int)(36 * multiplier), 0);
	settingFont = al_load_font("fonts/orbitron-medium.ttf", (int)(26 * multiplier), 0);
	bindingFont = al_load_font("fonts/orbitron-medium.ttf", (int)(20 * multiplier), 0);
	titleFont = al_load_font("fonts/orbitron-medium.ttf", (int)(240 * multiplier), 0);
	titleSmallFont = al_load_font("fonts/orbitron-medium.ttf", (int)(120 * multiplier), 0);
	headingFont = al_load_font("fonts/orbitron-medium.ttf", (int)(40 * multiplier), 0);
	textFont = al_load_font("fonts/orbitron-medium.ttf", (int)(20 * multiplier), 0);

	fpsText->setFont(hintFont);
	hintText->setFont(hintFont);

	setupScreenMenu();
	setupScreenNewGame();
	setupScreenHowToPlay();
	setupScreenStats();


	//HACK: This is a quick and dirty way to prevent crashes when hitting the reset button
	if (screenSettings != NULL && screenSettings == currentScreen)
	{
		screenSettings->close();
	}
	else
	{
		setupScreenSettings();
	}

	//HACK: This is a quick and dirty way to prevent crashes when hitting the reset button
	if (screenBindings != NULL && screenBindings == currentScreen)
	{
		screenBindings->close();
	}
	else
	{
		setupScreenBindings(INPUT_TYPE_KEY1);
	}


	setupScreenPause();
	setupScreenDeath();
	setupScreenEnd();
	setupScreenCredits();
}

void Game::setupScreenNewGame()
{
	if (screenNew != NULL)
	{
		delete screenNew;
		screenNew = NULL;
	}

	screenNew = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_skater2.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-30,-80) * graphicsMan->getAspectMultiplier(), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	i->setReflection(true);
	screenNew->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "NEW GAME");
	t->setPosition(vec2(45, -10), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	//GUIWidget* lastWidget = t;
	screenNew->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Easy", ActionManager::getActionManager()->doNewEasy);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(60, 60), ANCHOR_TL, vec2(0, 0));
	screenNew->addFocusableWidget(b);
	GUIWidget* lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Normal", ActionManager::getActionManager()->doNewNormal);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenNew->addFocusableWidget(b);
	lastWidget = b;

	screenNew->setDefaultFocusWidget(b);

	b = new GUIButton(buttonFont, uiColour, "Challenging", ActionManager::getActionManager()->doNewChallenging);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenNew->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Free Skate", ActionManager::getActionManager()->doNewFreeSkate);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenNew->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doBack);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenNew->addFocusableWidget(b);
	lastWidget = b;

	#ifdef DEBUG
		b = new GUIButton(buttonFont, uiColour, "Debug", ActionManager::getActionManager()->doNewDebug);
		b->setPadding(vec2(0, 5));
		b->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
		screenNew->addFocusableWidget(b);
		lastWidget = b;
	#endif

}


void Game::setupScreenMenu()
{
	if (screenMenu != NULL)
	{
		delete screenMenu;
		screenMenu = NULL;
	}

	screenMenu = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_skater1.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-40,-110) * graphicsMan->getAspectMultiplier(), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	i->setReflection(true);
	screenMenu->addWidget(i);

	GUIText* t = new GUIText(titleFont, uiColour, "FLAT");
	t->setMargin(vec2(60, 60));
	screenMenu->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Quit", ActionManager::getActionManager()->doQuit);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(60, -40), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	screenMenu->addFocusableWidget(b);
	GUIWidget* lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Website", ActionManager::getActionManager()->doWebsite);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, -5), ANCHOR_BL, lastWidget, ANCHOR_TL);
	screenMenu->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Credits", ActionManager::getActionManager()->doCredits);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, -5), ANCHOR_BL, lastWidget, ANCHOR_TL);
	screenMenu->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Settings", ActionManager::getActionManager()->doSettings);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, -5), ANCHOR_BL, lastWidget, ANCHOR_TL);
	screenMenu->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "How to play", ActionManager::getActionManager()->doHowToPlay);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, -5), ANCHOR_BL, lastWidget, ANCHOR_TL);
	screenMenu->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "New game", ActionManager::getActionManager()->doNewGame);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, -5), ANCHOR_BL, lastWidget, ANCHOR_TL);
	screenMenu->addFocusableWidget(b);
	lastWidget = b;

	screenMenu->setDefaultFocusWidget(b);
}


void Game::setupScreenPause()
{
	if (screenPause != NULL)
	{
		delete screenPause;
		screenPause = NULL;
	}

	screenPause = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_skater2.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(30,-80) * graphicsMan->getAspectMultiplier(), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	i->setReflection(true);
	screenPause->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "PAUSED");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenPause->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Resume", ActionManager::getActionManager()->doResume);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenPause->addFocusableWidget(b);
	GUIWidget* lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Statistics", ActionManager::getActionManager()->doStats);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenPause->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "How to play", ActionManager::getActionManager()->doHowToPlay);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenPause->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Settings", ActionManager::getActionManager()->doSettings);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenPause->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Forfeit", ActionManager::getActionManager()->doEndGame);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenPause->addFocusableWidget(b);
	lastWidget = b;
}


void Game::setupScreenSettings()
{
	if (screenSettings != NULL)
	{
		delete screenSettings;
		screenSettings = NULL;
	}

	vec2 vPaddingSmall = vec2(0, 5) * graphicsMan->getAspectMultiplier();
	vec2 vPaddingLarge = vec2(0, 10) * graphicsMan->getAspectMultiplier();

	screenSettings = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_capsule.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(80,-80) * graphicsMan->getAspectMultiplier(), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	i->setReflection(true);
	screenSettings->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "SETTINGS");
	t->setPosition(vec2(-45, -10) * graphicsMan->getAspectMultiplier(), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	GUIWidget* lastWidget = t;
	screenSettings->addWidget(t);


	//Back button column.
	int columnWidth = al_get_text_width(buttonFont, "Reset");

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doBack);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(columnWidth, -1));
	b->setPosition(vec2(-60, 40) * graphicsMan->getAspectMultiplier(), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenSettings->addFocusableWidget(b);
	lastWidget = b;
	screenSettings->setDefaultFocusWidget(b);

	b = new GUIButton(buttonFont, uiColour, "Reset", ActionManager::getActionManager()->doResetSettings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(columnWidth, -1));
	b->setPosition(vPaddingSmall, ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenSettings->addFocusableWidget(b);
	lastWidget = b;

	t = new GUIText(hintFont, uiColour, VERSION_NUMBER);
	t->setPosition(vec2(0, 0), ANCHOR_TR, lastWidget, ANCHOR_BR);
	lastWidget = t;
	screenSettings->addWidget(t);


	//Input column
	columnWidth = al_get_text_width(settingFont, "Joystick axis deadzone");

	GUIToggle* b2 = new GUIToggle(settingFont, uiColour, "Auto heal", ActionManager::getActionManager()->updateAutoHeal);
	b2->setValue(settingsMan->getAutoHeal());
	b2->setSize(vec2(columnWidth, -1));
	b2->setPadding(vPaddingSmall);
	b2->setPosition(vec2(120, 90) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	screenSettings->addFocusableWidget(b2);
	lastWidget = b2;

	b2 = new GUIToggle(settingFont, uiColour, "Enable joystick", ActionManager::getActionManager()->updateEnableJoystick);
	b2->setValue(settingsMan->getEnableJoystick());
	b2->setSize(vec2(columnWidth, -1));
	b2->setPadding(vPaddingSmall);
	b2->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b2);
	lastWidget = b2;

	GUISlider* b3 = new GUISlider(settingFont, uiColour, "Joystick axis deadzone", ActionManager::getActionManager()->updateAxisDeadzone);
	b3->setValue(settingsMan->getAxisDeadzone());
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;

	b3 = new GUISlider(settingFont, uiColour, "Turn sensitivity", ActionManager::getActionManager()->updateTurnSensitivity);
	b3->setValue(settingsMan->getTurnSensitivity() * 100.0f);
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;

	b = new GUIButton(settingFont, uiColour, "Configure bindings", ActionManager::getActionManager()->doBindings);
	b->setSize(vec2(columnWidth, -1));
	b->setPadding(vPaddingSmall);
	b->setPosition(vec2(0, 40) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b);
	lastWidget = b;


	b = new GUIButton(settingFont, uiColour, "Open settings folder", ActionManager::getActionManager()->doSettingsFolder);
	b->setSize(vec2(columnWidth, -1));
	b->setPadding(vPaddingSmall);
	b->setPosition(vec2(0, 40) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b);
	lastWidget = b;



	//Volume column
	b2 = new GUIToggle(settingFont, uiColour, "Enable hints", ActionManager::getActionManager()->updateShowHints);
	b2->setValue(settingsMan->getShowHints());
	b2->setSize(vec2(columnWidth, -1));
	b2->setPadding(vPaddingSmall);
	b2->setPosition(vec2(round(settingsMan->getResX() * 0.5), 90 * graphicsMan->getAspectMultiplier().y), ANCHOR_TL, vec2(0, 0));
	screenSettings->addFocusableWidget(b2);
	lastWidget = b2;

	b2 = new GUIToggle(settingFont, uiColour, "Show FPS", ActionManager::getActionManager()->updateShowFPS);
	b2->setValue(settingsMan->getShowFPS());
	b2->setSize(vec2(columnWidth, -1));
	b2->setPadding(vPaddingSmall);
	b2->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b2);
	lastWidget = b2;

	b2 = new GUIToggle(settingFont, uiColour, "Fullscreen", ActionManager::getActionManager()->updateFullscreen);
	b2->setValue(settingsMan->getFullscreen());
	b2->setSize(vec2(columnWidth, -1));
	b2->setPadding(vPaddingSmall);
	b2->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b2);
	lastWidget = b2;

	b3 = new GUISlider(settingFont, uiColour, "Global volume", ActionManager::getActionManager()->updateGlobalVolume);
	b3->setValue(settingsMan->getGlobalVolume());
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;

	b3 = new GUISlider(settingFont, uiColour, "Music volume", ActionManager::getActionManager()->updateMusicVolume);
	b3->setValue(settingsMan->getMusicVolume());
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;


	b3 = new GUISlider(settingFont, uiColour, "Effects volume", ActionManager::getActionManager()->updateEffectsVolume);
	b3->setValue(settingsMan->getEffectsVolume());
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;


	b3 = new GUISlider(settingFont, uiColour, "UI volume", ActionManager::getActionManager()->updateUIVolume);
	b3->setValue(settingsMan->getUIVolume());
	b3->setSize(vec2(columnWidth, -1));
	b3->setPadding(vPaddingSmall);
	b3->setPosition(vPaddingLarge, ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenSettings->addFocusableWidget(b3);
	lastWidget = b3;

}


void Game::setupScreenBindings(int mode)
{

	if (screenBindings != NULL)
	{
		delete screenBindings;
		screenBindings = NULL;
	}

	vec2 vPaddingSmall = vec2(0, 5) * graphicsMan->getAspectMultiplier();
	vec2 vPaddingLarge = vec2(0, 10) * graphicsMan->getAspectMultiplier();

	screenBindings = new GUIScreen();

	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_capsule.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(80,-80) * graphicsMan->getAspectMultiplier(), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	i->setReflection(true);
	screenBindings->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "BINDINGS");
	t->setPosition(vec2(-45, -10) * graphicsMan->getAspectMultiplier(), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	GUIWidget* lastWidget = t;
	screenBindings->addWidget(t);


	//Back button column.
	int columnWidth = al_get_text_width(buttonFont, "Reset");
	int marginX = -60 * graphicsMan->getAspectMultiplier().x;
	int marginY = 40 * graphicsMan->getAspectMultiplier().y;



	GUIButton* b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doSettings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(columnWidth, -1));
	b->setPosition(vec2(marginX, marginY), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenBindings->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Reset", ActionManager::getActionManager()->doResetBindings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(columnWidth, -1));
	b->setPosition(vPaddingSmall, ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenBindings->addFocusableWidget(b);
	lastWidget = b;

	columnWidth = al_get_text_width(bindingFont, "APPROPRIATE LENGTH");
	int columnWidth2 = al_get_text_width(bindingFont, "WBACKSPACE");
	marginX = 200 * graphicsMan->getAspectMultiplier().x;
	marginY = 60 * graphicsMan->getAspectMultiplier().x;

	int w = al_get_text_width(settingFont, "Keyboard");
	b = new GUIButton(settingFont, uiColour, "Keyboard", ActionManager::getActionManager()->doKeyboardBindings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(w, -1));
	b->setPosition(vec2(marginX, marginY), ANCHOR_TL, vec2(0, 0));
	screenBindings->addFocusableWidget(b);
	lastWidget = b;
	if (mode == INPUT_TYPE_KEY1)
	{
		screenBindings->setDefaultFocusWidget(b);
		GUILine* l = new GUILine(uiColour, 2);
		l->setSize(vec2(w + al_get_font_ascent(settingFont) + vPaddingSmall.y * 2, 0));
		l->setPosition(vec2(0, 0), ANCHOR_TL, lastWidget, ANCHOR_BL);
		screenBindings->addWidget(l);
	}

	w = al_get_text_width(settingFont, "Mouse");
	b = new GUIButton(settingFont, uiColour, "Mouse", ActionManager::getActionManager()->doMouseBindings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(w, -1));
	b->setPosition(vec2(marginX + columnWidth, marginY), ANCHOR_TL, vec2(0, 0));
	screenBindings->addFocusableWidget(b);
	lastWidget = b;
	if (mode == INPUT_TYPE_MOUSE1)
	{
		screenBindings->setDefaultFocusWidget(b);
		GUILine* l = new GUILine(uiColour, 2);
		l->setSize(vec2(w + al_get_font_ascent(settingFont) + vPaddingSmall.y * 2, 0));
		l->setPosition(vec2(0, 0), ANCHOR_TL, lastWidget, ANCHOR_BL);
		screenBindings->addWidget(l);
	}

	w = al_get_text_width(settingFont, "Gamepad");
	b = new GUIButton(settingFont, uiColour, "Gamepad", ActionManager::getActionManager()->doGamepadBindings);
	b->setPadding(vPaddingSmall);
	b->setSize(vec2(w, -1));
	b->setPosition(vec2(marginX + columnWidth * 2, marginY), ANCHOR_TL, vec2(0, 0));
	screenBindings->addFocusableWidget(b);
	lastWidget = b;
	if (mode == INPUT_TYPE_GAMEPAD1)
	{
		screenBindings->setDefaultFocusWidget(b);
		GUILine* l = new GUILine(uiColour, 2);
		l->setSize(vec2(w + al_get_font_ascent(settingFont) + vPaddingSmall.y * 2, 0));
		l->setPosition(vec2(0, 0), ANCHOR_TL, lastWidget, ANCHOR_BL);
		screenBindings->addWidget(l);
	}

	lastWidget = NULL;
	GUIWidget* lastWidget2 = NULL;
	GUIWidget* lastWidget3 = NULL;
	GUIBindingButton* bb = NULL;
	marginX = 60 * graphicsMan->getAspectMultiplier().x;
	marginY = 30 * graphicsMan->getAspectMultiplier().y;

	GUIScroller* scroller = new GUIScroller(uiColour);
	scroller->setPadding(vPaddingLarge);
	scroller->setSize(vec2(800, 300) * graphicsMan->getAspectMultiplier());
	scroller->setPosition(vec2(200, 160) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	screenBindings->addFocusableWidget(scroller);

	//Binding list
	for (int i = 0; i < INPUT_MAX; i++)
	{
		InputAction* input = inputMan->input[i];
		//Print("Showing bindings for  ", input->name);
		//Print(al_keycode_to_name(input->key1));

		GUIText* t = new GUIText(bindingFont, uiColour, input->name.c_str());
		t->setPadding(vPaddingSmall);
		t->setSize(vec2(columnWidth - 2 * graphicsMan->getAspectMultiplier().x, -1));
		if (lastWidget == NULL)
		{
			t->setPosition(vec2(marginX, marginY), ANCHOR_TL, vec2(0, 0));
		}
		else
		{
			t->setPosition(vPaddingSmall, ANCHOR_TL, lastWidget, ANCHOR_BL);
		}
		scroller->addWidget(t);
		lastWidget = t;

		int col1Type;
		int col2Type;

		if (mode == INPUT_TYPE_KEY1)
		{
			col1Type = INPUT_TYPE_KEY1;
			col2Type = INPUT_TYPE_KEY2;
		}
		else if (mode == INPUT_TYPE_GAMEPAD1)
		{
			col1Type = INPUT_TYPE_GAMEPAD1;
			col2Type = INPUT_TYPE_GAMEPAD2;
		}
		else// if (mode == INPUT_TYPE_MOUSE1)
		{
			col1Type = INPUT_TYPE_MOUSE1;
			col2Type = INPUT_TYPE_MOUSE2;
		}

		bb = new GUIBindingButton(bindingFont, uiColour, uiColourActive, uiColourBad, input, col1Type, ActionManager::getActionManager()->doResetBindings);
		bb->setPadding(vPaddingSmall);
		bb->setSize(vec2(columnWidth2, -1));
		if (lastWidget2 == NULL)
		{
			bb->setPosition(vec2(marginX + columnWidth, marginY), ANCHOR_TL, vec2(0, 0));
		}
		else
		{
			bb->setPosition(vPaddingSmall, ANCHOR_TL, lastWidget2, ANCHOR_BL);
		}
		scroller->addFocusableWidget(bb);
		lastWidget2 = bb;

		bb = new GUIBindingButton(bindingFont, uiColour, uiColourActive, uiColourBad, input, col2Type, ActionManager::getActionManager()->doResetBindings);
		bb->setPadding(vPaddingSmall);
		bb->setSize(vec2(columnWidth2, -1));
		if (lastWidget3 == NULL)
		{
			bb->setPosition(vec2(marginX + columnWidth + columnWidth2 + vPaddingLarge.y * 4, marginY), ANCHOR_TL, vec2(0, 0));
		}
		else
		{
			bb->setPosition(vPaddingSmall, ANCHOR_TL, lastWidget3, ANCHOR_BL);
		}
		scroller->addFocusableWidget(bb);
		lastWidget3 = bb;
	}
}


void Game::setupScreenStats()
{
	if (screenStats != NULL)
	{
		delete screenStats;
		screenStats = NULL;
	}

	screenStats = new GUIScreen();
	GUIText* t = new GUIText(titleSmallFont, uiColour, "STATISTICS");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenStats->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doBack);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenStats->addFocusableWidget(b);
	GUIWidget* lastWidget = b;


	GUIImage* i = new GUIImage(al_load_bitmap("gui/weapon_pistol.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(200, 130) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	weaponStatImages[0] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "1001 shots");
	t->setPosition(vec2(0, -al_get_font_line_height(textFont)), ANCHOR_TL, i, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatShots[0] = t;
	lastWidget = t;

	t = new GUIText(textFont, uiColour, "(100%)");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatHits[0] = t;
	lastWidget = t;


	i = new GUIImage(al_load_bitmap("gui/weapon_shotgun.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-round(al_get_text_width(textFont, "1002 shots") * 0.5), 130 * graphicsMan->getAspectMultiplier().y), ANCHOR_TL, vec2(settingsMan->getResX() * 0.5, 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	weaponStatImages[1] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "1001 shots");
	t->setPosition(vec2(0, -al_get_font_line_height(textFont)), ANCHOR_TL, i, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatShots[1] = t;
	lastWidget = t;

	t = new GUIText(textFont, uiColour, "(100%)");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatHits[1] = t;
	lastWidget = t;


	i = new GUIImage(al_load_bitmap("gui/weapon_smg.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-200, 130) * graphicsMan->getAspectMultiplier(), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	weaponStatImages[2] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "1001 shots");
	t->setPosition(vec2(0, -al_get_font_line_height(textFont)), ANCHOR_TL, i, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatShots[2] = t;
	lastWidget = t;

	t = new GUIText(textFont, uiColour, "(100%)");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	weaponStatHits[2] = t;
	lastWidget = t;


	i = new GUIImage(al_load_bitmap("gui/enemy_skater.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(150, 350) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	enemyStatImages[TYPE_GRUNT] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "64");
	t->setPosition(vec2(92 * graphicsMan->getMinAspectMultiplier().y, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	enemyStatKills[TYPE_GRUNT] = t;
	lastWidget = t;

	i = new GUIImage(al_load_bitmap("gui/enemy_dragon_spine.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-round(al_get_text_width(textFont, "1002 shots") * 0.8), 350 * graphicsMan->getAspectMultiplier().y), ANCHOR_TL, vec2(settingsMan->getResX() * 0.5, 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	enemyStatImages[TYPE_DRAGON_SPINE] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "64");
	t->setPosition(vec2(92 * graphicsMan->getMinAspectMultiplier().y, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	enemyStatKills[TYPE_DRAGON_SPINE] = t;
	lastWidget = t;

	i = new GUIImage(al_load_bitmap("gui/enemy_dragon_head.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-225, 350) * graphicsMan->getAspectMultiplier(), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	i->setReflection(true);
	screenStats->addWidget(i);
	enemyStatImages[TYPE_DRAGON] = i;
	lastWidget = i;

	t = new GUIText(textFont, uiColour, "1");
	t->setPosition(vec2(92 * graphicsMan->getMinAspectMultiplier().y, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenStats->addWidget(t);
	enemyStatKills[TYPE_DRAGON] = t;
	lastWidget = t;


	t = new GUIText(buttonFont, uiColour, std::string("00:00:00"));
	t->setPosition(vec2(60, 40), ANCHOR_TL, vec2(0, 0));
	screenStats->addWidget(t);
	timeStat = t;
	lastWidget = t;
}


void Game::setupScreenHowToPlay()
{
	if (screenHowToPlay != NULL)
	{
		delete screenHowToPlay;
		screenHowToPlay = NULL;
	}

	screenHowToPlay = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_crystal.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-40,-100) * graphicsMan->getAspectMultiplier(), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	i->setReflection(true);
	screenHowToPlay->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "HOW TO PLAY");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenHowToPlay->addWidget(t);
	GUIWidget* lastWidget = t;

	t = new GUIText(headingFont, uiColour, "SKATE");
	t->setPosition(vec2(60, 100) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, std::string("Alternate between ") + inputMan->getDefaultBoundName(INPUT_LEFT) + " and " + inputMan->getDefaultBoundName(INPUT_RIGHT) + " whilst holding " + inputMan->getDefaultBoundName(INPUT_FORWARD) + " to skate forwards.");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, std::string("Skate backwards by holding ") + inputMan->getDefaultBoundName(INPUT_BACKWARD) + " and alternating between " + inputMan->getDefaultBoundName(INPUT_LEFT) + " and " + inputMan->getDefaultBoundName(INPUT_RIGHT) + ".");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, std::string("Press ") + inputMan->getDefaultBoundName(INPUT_JUMP) + " to jump.");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(headingFont, uiColour, "COLLECT");
	t->setPosition(vec2(0, 60), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, std::string("Find new weapons and health kits in hidden capsules. Press ") + inputMan->getDefaultBoundName(INPUT_HEAL) + " to heal.");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(headingFont, uiColour, "DEFEAT");
	t->setPosition(vec2(0, 60), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, "Slay the mighty Pulse Dragon to free your people from its mind control.");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenHowToPlay->addWidget(t);
	lastWidget = t;


	GUIButton* b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doBack);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenHowToPlay->addFocusableWidget(b);
	lastWidget = b;
}


void Game::setupScreenDeath()
{
	if (screenDeath != NULL)
	{
		delete screenDeath;
		screenDeath = NULL;
	}

	screenDeath = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_skater4.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(100, 0) * graphicsMan->getAspectMultiplier(), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	i->setReflection(true);
	screenDeath->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "GAME OVER");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenDeath->addWidget(t);
	GUIWidget* lastWidget = t;

	t = new GUIText(buttonFont, uiColour, "The Pulse Dragon's reign continues...");
	t->setPosition(vec2(0, 0), ANCHOR_BR, lastWidget, ANCHOR_TR);
	screenDeath->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Statistics", ActionManager::getActionManager()->doStats);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenDeath->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Menu", ActionManager::getActionManager()->doEndGame);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenDeath->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Quit", ActionManager::getActionManager()->doQuit);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenDeath->addFocusableWidget(b);
	lastWidget = b;
}


void Game::setupScreenEnd()
{
	if (screenEnd != NULL)
	{
		delete screenEnd;
		screenEnd = NULL;
	}

	screenEnd = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_skater3.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(30,-100) * graphicsMan->getAspectMultiplier(), ANCHOR_BL, vec2(0, settingsMan->getResY()));
	i->setReflection(true);
	screenEnd->addWidget(i);

	GUIText* t = new GUIText(titleSmallFont, uiColour, "SUCCESS");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenEnd->addWidget(t);
	GUIWidget* lastWidget = t;

	t = new GUIText(buttonFont, uiColour, "Thank you for playing");
	t->setPosition(vec2(0, 0), ANCHOR_BR, lastWidget, ANCHOR_TR);
	screenEnd->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Statistics", ActionManager::getActionManager()->doStats);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenEnd->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Menu", ActionManager::getActionManager()->doEndGame);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenEnd->addFocusableWidget(b);
	lastWidget = b;

	b = new GUIButton(buttonFont, uiColour, "Quit", ActionManager::getActionManager()->doQuit);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(0, 5), ANCHOR_TR, lastWidget, ANCHOR_BR);
	screenEnd->addFocusableWidget(b);
	lastWidget = b;
}


void Game::setupScreenCredits()
{
	if (screenCredits != NULL)
	{
		delete screenCredits;
		screenCredits = NULL;
	}

	screenCredits = new GUIScreen();
	GUIImage* i = new GUIImage(al_load_bitmap("gui/background_moon1.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(40, 170) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	screenCredits->addWidget(i);

	i = new GUIImage(al_load_bitmap("gui/background_moon2.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-250, 40) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(settingsMan->getResX() * 0.5, 0));
	screenCredits->addWidget(i);

	i = new GUIImage(al_load_bitmap("gui/background_moon3.png"));
	i->draw(graphicsMan->getAspectMultiplier()); //HACK: This makes sure that images cache the aspect diff and use it to calculate sizePlus
	i->setPosition(vec2(-320, 220) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(settingsMan->getResX(), 0));
	screenCredits->addWidget(i);

	GUIText* t = new GUIText(headingFont, uiColour, "ORIGINAL DEVELOPERS");
	t->setPosition(vec2(80, 130) * graphicsMan->getAspectMultiplier(), ANCHOR_TL, vec2(0, 0));
	screenCredits->addWidget(t);
	GUIWidget* lastWidget = t;

	t = new GUIText(textFont, uiColour, "Josh \"Cheeseness\" Bush");
	t->setPosition(vec2(10, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Johan \"SteelRaven7\" Hassel");
	t->setPosition(vec2(0, 10), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Johannes \"JohannesMP\" Peter");
	t->setPosition(vec2(0, 10), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Anton Riehl");
	t->setPosition(vec2(0, 10), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;

	t = new GUIText(headingFont, uiColour, "CONTRIBUTORS");
	t->setPosition(vec2(-10, 70), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Thanks to everybody who supported and");
	t->setPosition(vec2(10, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "contributed to FLAT's development after");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "7DFPS!");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "See the CONTRIBUTORS and CHANGES");
	t->setPosition(vec2(0, 10), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "files for details");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;


	t = new GUIText(headingFont, uiColour, "LIBRARIES AND");
	t->setSize(vec2(al_get_text_width(headingFont, "3RD PARTY ASSETS"), al_get_font_line_height(headingFont)));
	t->setPosition(vec2(-80, 130) * graphicsMan->getAspectMultiplier(), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenCredits->addWidget(t);
	lastWidget = t;

	t = new GUIText(headingFont, uiColour, "3RD PARTY ASSETS");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;

	t = new GUIText(textFont, uiColour, "Allegro 5");
	t->setPosition(vec2(10, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Copyright © 2008-2010 the Allegro 5");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Development Team");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;


	t = new GUIText(textFont, uiColour, "Orbitron");
	t->setPosition(vec2(0, 15), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Copyright (c) 2009, Matt McInerney");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "with Reserved Font Name Orbitron");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;


	t = new GUIText(textFont, uiColour, "Bitstream Vera Sans");
	t->setPosition(vec2(0, 15), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Copyright (c) 2003 by Bitstream, Inc. All Rights");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Reserved. Bitstream Vera is a trademark of");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "Bitstream, Inc.");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;


	t = new GUIText(textFont, uiColour, "See licences in the assets/sfx folder for");
	t->setPosition(vec2(0, 15), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;
	t = new GUIText(textFont, uiColour, "licence details of external sound samples");
	t->setPosition(vec2(0, 5), ANCHOR_TL, lastWidget, ANCHOR_BL);
	screenCredits->addWidget(t);
	lastWidget = t;


	t = new GUIText(titleSmallFont, uiColour, "CREDITS");
	t->setPosition(vec2(-45, -10), ANCHOR_BR, vec2(settingsMan->getResX(), settingsMan->getResY()));
	screenCredits->addWidget(t);

	GUIButton* b = new GUIButton(buttonFont, uiColour, "Back", ActionManager::getActionManager()->doBack);
	b->setPadding(vec2(0, 5));
	b->setPosition(vec2(-60, 40), ANCHOR_TR, vec2(settingsMan->getResX(), 0));
	screenCredits->addFocusableWidget(b);
	lastWidget = b;
}

void Game::endGame()
{
	inputMan->clearInputs();

	for (unsigned int i = 0; i < pickup.size(); i++)
	{
		delete pickup[i];
	}
	pickup.clear();

	for (unsigned int i = 0; i < detail.size(); i++)
	{
		delete detail[i];
	}
	detail.clear();
	stat.clear();

	for (unsigned int i = 0; i < npc.size(); i++)
	{
		delete npc[i];
	}
	npc.clear();

	for (unsigned int i = 0; i < splat.size(); i++)
	{
		delete splat[i];
	}
	splat.clear();

	for (unsigned int i = 0; i < projectile.size(); i++)
	{
		delete projectile[i];
	}
	projectile.clear();

	if (player != NULL)
	{
		graphicsMan->removeDrawableObject(player);
		delete player;
		player = NULL;
	}

	if (floor != NULL)
	{
		graphicsMan->removeDrawableObject(floor);
		delete floor;
		floor = NULL;
	}

	if (world3D != NULL)
	{
		graphicsMan->removeDrawableObject(world3D);
		delete world3D;
		world3D = NULL;
	}
}


void Game::newGame()
{
	dragonHead = NULL;

	enemySpawnCount = 0;
	safe = true;

	hintState = HINT_NONE;

	pickups = 0;
	pickupsSinceLastWeapon = 0;

	gameEnded = false;
	bossSpawned = false;

	pickupTimer = 0.0f;

	player = new Player();

	player->setAutoHeal(settingsMan->getAutoHeal());
	player->setTurnSensitivity(settingsMan->getTurnSensitivity());
	player->initKillStats(MAX_ENEMY_TYPE);

	player->speed = rules[currentRules]->playerSpeed;
	player->speedCarving = rules[currentRules]->playerSpeedCarving;
	player->speedBackMultiplier = rules[currentRules]->playerSpeedBackMultiplier;
	player->healthMax = rules[currentRules]->playerHealth;



	world3D = new DrawStack3D();

	loadBitmaps();

	float crateDist = 30.0f;
	for (unsigned int i = 0; i < rules[currentRules]->startingCrates; i++)
	{
		pickup.push_back(new Pickup(world3D, vec3(crateDist, 5.0f, 0.0f)));
		pickup.back()->forceScale();
		pickup.back()->setHumSample(audioMan->sounds[SOUND_PICKUP_HUM]);
		pickup.back()->healthKitCount = rules[currentRules]->crateHealthCount;
		pickup.back()->hasNewWeapon = true;
		crateDist += 3;
	}

	for(int i = 0; i < DETAIL_OBJECT_NUMBER; i++) {
		DetailObject* o;

		float angle = random(PI);

		if(rand()%100 > STATIC_OBJECT_PERCENTAGE) {
			o = new DetailObject();
			o->setBitmap(detailBitmap[0]);
			o->setPosition(vec3(sin(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), cos(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), 3.5f));

			for(int j = 1; j < 6; j++) {
				if(rand()%6-j == 0) {
					o->setBitmap(detailBitmap[j]);
					break;
				}
			}
		}
		else {
			o = new StaticObject();

			if(rand()%3 == 0) {
				for(int i = 0; i < 16; i++) {
					o->pushImage(crystalA[i]);
					o->setPrescale(30.0f);
					o->setPosition(vec3(sin(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), cos(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), -4.0f));
				}
			}
			else if(rand()%2 == 0) {
				for(int i = 0; i < 16; i++) {
					o->pushImage(crystalB[i]);
					o->setPrescale(30.0f);
					o->setPosition(vec3(sin(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), cos(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), -4.0f));
				}
			}
			else {
				for(int i = 0; i < 16; i++) {
					o->pushImage(crystalC[i]);
					o->setPrescale(30.0f);
					o->setPosition(vec3(sin(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), cos(angle)*random(DETAIL_OBJECT_CREATION_DISTANCE), -2.0f));
					o->setJumpable();
				}
			}
			stat.push_back(o);
		}
		o->forceScale();
		world3D->push(o);
		detail.push_back(o);
	}

	floor = new Floor();

	graphicsMan->pushDrawableObject(floor);
	graphicsMan->pushDrawableObject(world3D);
	graphicsMan->pushDrawableObject(player);
	graphicsMan->pushDrawableObject(fpsText);
	graphicsMan->pushDrawableObject(hintText);
	setFPSVisibility(settingsMan->getShowFPS());
	setHintVisibility(false); // Turn off hints until we work through the game loop enough to process
	audioMan->setGameState(GAME_STATE_NORMAL);

	for(unsigned int i = 0; i < rules[currentRules]->startingEnemies; i++) {
		float angle = random(PI) * 0.24 + PI * 0.12;
		float distance = 30.0f;
		Character* c = spawnCharacter(vec3(player->getPos().x + cos(angle) * distance, player->getPos().y + sin(angle) * distance, 0.0f), 0.0f);
		c->fireTimer = 4.0f + random(1.0f); //Delay firing by 4-ish seconds to give the player time to get their bearings
	}


	for (unsigned int i = 0; i < rules[currentRules]->startingDragons; i++)
	{
		float angle = random(PI) * 0.24 + PI * 0.12;
		dragonHead = spawnDragon(vec3(player->getPos().x+cos(angle) * BOSS_SPAWN_DISTANCE, player->getPos().y+sin(angle) * BOSS_SPAWN_DISTANCE, 0.0f), 20.0f);
	}
}

void Game::gameTick()
{
	if (!paused && !quit && currentScreen == NULL)
	{
		//TODO: Update TIME_STEP in Player, Character, DragonHead, Pickup, Projectile, and Weapon to account for SettingsManager::targetFPS
		player->update();

		vec2 targetPos = player->getPos();
		vec2 targetVel = player->getVel();

		int len = npc.size();

		if(player->firing) {
			bool hit = false;
			for(int j = 0; j < player->getWeaponRounds(); j++) {
				float closestDistance = 500;
				int closestIndex = -1;
				float damage = 0.0f;

				float randomSpread = player->getWeaponSpread()*random(1.0f);

				for(int i = 0; i < len; i++) {
					Character *c = npc[i];

					float distance = c->getPlayerDistance();
					float angle = abs(c->getPlayerAngle())*distance+randomSpread;

					if(angle < 0.31f && distance < closestDistance && !npc[i]->isInvurnable()) {
						closestIndex = i;
						closestDistance = distance;
						damage = player->getWeaponDamage(distance, angle);
					}
				}

				if(closestIndex != -1 && damage > 0) {

					vec3 dPosition = npc[closestIndex]->getPosition() - player->getCameraPosition();

					float creationAngle = atan2(dPosition.y, dPosition.x);
					dPosition = player->getCameraPosition() + vec3(cos(creationAngle) * (closestDistance-0.3) + random(0.5), sin(creationAngle) * (closestDistance-0.3) + random(0.5), 0.5 + random(0.5));

					spawnSplat(splatBitmap, dPosition);
					spawnSplat(sparkBitmap, dPosition);

					npc[closestIndex]->dealDamage(damage);

					lastEventTime = player->getTime();
					hit = true;
				}
			}
			player->updateWeaponStats(hit);
		}

		for (unsigned int i = 0; i < npc.size(); i++) {
			Character* c = npc[i];

			c->updateTarget(targetPos, targetVel);
			c->update();

			if(c->isFiring()) {
				Projectile* o = c->getProjectile();

				float speed = o->speed;
				float distance = c->getPlayerDistance();

				float time = (distance/speed)/2;


				vec2 targetPosition = vec2(player->getCameraPosition().x, player->getCameraPosition().y) + player->getVel() * time;
				vec2 originPosition = vec2(c->getPosition().x, c->getPosition().y);
				vec2 vel = normalize(targetPosition - originPosition)*speed;
				o->setVelocity(vel);
				world3D->push(o);
				projectile.push_back(o);
			}

			if (c->countDeath)
			{
				player->updateKillStats(c->enemyType);
				c->countDeath = false;
			}

			if(c->destroy) {
				if(c->dragon) {
					Print("We killed a dragon!");
					al_rest(0.5f);
					gameEnded = true;
					showScreen(screenEnd);
					doPause(true);
				}
				npc.erase(npc.begin()+i);
				world3D->remove(c);
				delete c;
				i--;
			}

			if (npc.size() <= 0)
			{
				//Audio man, audio man. Doing the things an audio can.
				audioMan->setGameState(GAME_STATE_NORMAL);
			}
		}

		for (unsigned int i = 0; i < projectile.size(); i++) {
			Projectile* p = projectile[i];

			p->update();

			float distance = distanceBetween(p->getPos(), player->getPos());

			if(distance < 1.0f) {
				player->takeDamage(p->damage);
				p->destroy = true;
			}

			if(p->destroy) {
				projectile.erase(projectile.begin()+i);
				world3D->remove(p);
				delete p;
				i--;
			}
		}

		for (unsigned int i = 0; i < splat.size(); i++) {
			Splat* s = splat[i];

			if(s->destroy) {
				splat.erase(splat.begin()+i);
				world3D->remove(s);
				delete s;
				i--;
				continue;
			}

			s->update();
		}

		for (unsigned int i = 0; i < detail.size(); i++) {
			DetailObject* o = detail[i];
			if(o->destroy) {
				detail.erase(detail.begin()+i);
				std::vector<DetailObject*>::iterator it = std::find(stat.begin(), stat.end(), o);
				if (it != stat.end())
				{
					stat.erase(it);
				}
				world3D->remove(o);
				delete o;
				i++;


				float angle = atan2(player->getVel().y, player->getVel().x);

				float addon = random(1.0f);
				addon *= addon*0.75f;

				angle += random(PI)*(addon+0.25f);


				vec3 pos = player->getCameraPosition();

				float distance = DETAIL_OBJECT_CREATION_DISTANCE - abs(random(DETAIL_OBJECT_CREATION_DISTANCE_RANDOM));

				DetailObject* o;

				if(rand()%100 > STATIC_OBJECT_PERCENTAGE)
				{
					o = new DetailObject();
					o->setBitmap(detailBitmap[0]);
					o->setPosition(vec3(pos.x + cos(angle)*distance, pos.y + sin(angle)*distance, 2.0f));

					for(int j = 1; j < 6; j++) {
						if(rand()%6-j == 0) {
							o->setBitmap(detailBitmap[j]);
							break;
						}
					}
				}
				else
				{
					o = new StaticObject();

					if(rand()%3 == 0) {
						for(int i = 0; i < 16; i++) {
							o->pushImage(crystalA[i]);
							o->setPrescale(30.0f);
							o->setPosition(vec3(pos.x + cos(angle)*distance, pos.y + sin(angle)*distance, -2.4f));
						}
					}
					else if(rand()%2 == 0) {
						for(int i = 0; i < 16; i++) {
							o->pushImage(crystalB[i]);
							o->setPrescale(30.0f);
							o->setPosition(vec3(pos.x + cos(angle)*distance, pos.y + sin(angle)*distance, -2.4f));
						}
					}
					else {
						for(int i = 0; i < 16; i++) {
							o->pushImage(crystalC[i]);
							o->setPrescale(30.0f);
							o->setPosition(vec3(pos.x + cos(angle)*distance, pos.y + sin(angle)*distance, -1.0f));
							o->setJumpable();
						}
					}

					stat.push_back(o);
				}

				world3D->push(o);
				detail.push_back(o);
			}
		}

		for(unsigned int i = 0; i < stat.size(); i++) {

			stat[i]->update();

			vec2 oPos = stat[i]->getPos();
			vec2 pPos = player->getPos();

			vec2 dPos = pPos-oPos;

			float l = length(dPos);
			float r = stat[i]->getRadius();


			if(l < r) {
				if(stat[i]->isJumpable()) {
					if(!player->isJumping()) {
						player->setVelocity(player->getVel()*0.90);
					}
				}
				else {
					player->setVelocity(dPos*length(player->getVel())*0.3);
					player->setPosition(oPos+normalize(dPos)*r);
				}
			}
		}

		for (unsigned int i = 0; i < pickup.size(); i++) {
			Pickup* p = pickup[i];
			if(distanceBetween(pickup[i]->getPos(), player->getPos()) < 3.0f) {
				p->remove();
				player->pickup(p);

				if (p->hasNewWeapon)
				{
					pickupsSinceLastWeapon = 0;
				}
				else
				{
					pickupsSinceLastWeapon+= 1;
				}
				lastEventTime = player->getTime();

				pickup.erase(pickup.begin()+i);
				delete p;

				enemySpawnCount += rules[currentRules]->crateEnemyCountStep;
				pickupTimer = rules[currentRules]->crateTime;
				pickups++;
				break;
			}

			if (p->destroy) {
				pickup.erase(pickup.begin()+i);
				lastEventTime = player->getTime();
				delete p;
				i++;

			}
		}

		world3D->setCamera(player->getCameraPosition(), player->getCameraAngle(), player->getCameraTilt());
		floor->setCamera(player->getCameraPosition(), player->getCameraAngle(), player->getCameraTilt());

		if (player->gameOver)
		{
			gameEnded = true;
			showScreen(screenDeath);
			doPause(true);
		}
	}
}

void Game::registerEventSource(ALLEGRO_EVENT_SOURCE *source)
{
	if (source != NULL && event_queue != NULL)
	{
		al_register_event_source(event_queue, source);
	}
}

void Game::unregisterEventSource(ALLEGRO_EVENT_SOURCE *source)
{
	if (source != NULL && event_queue != NULL)
	{
		al_unregister_event_source(event_queue, source);
	}
}


void Game::setUpdateTimer(unsigned int f)
{
	if (updateTimer != NULL)
	{
		double targetFPS = 1.0 / f;
		if (targetFPS != al_get_timer_speed(updateTimer))
		{
			Print("Setting target FPS to", targetFPS);
			al_set_timer_speed(updateTimer, targetFPS);
		}
	}
}

int Game::runMainLoop() {

	updateTimer = al_create_timer(1.0 / settingsMan->getFPS());
	if(!updateTimer) {
		Print("Error setting up update timer.");
		return EXIT_FAILURE;
	}

	fpsCounter = al_create_timer(1.0);
	if(!fpsCounter) {
		Print("Error setting up fps counter.");
		return EXIT_FAILURE;
	}

	hintCounter = al_create_timer(0.5);
	if(!hintCounter) {
		Print("Error setting up hint counter.");
		return EXIT_FAILURE;
	}

	event_queue = al_create_event_queue();
	if(!event_queue) {
		Print("Error creating event_queue.");
		return EXIT_FAILURE;
	}

	ALLEGRO_TIMER* doStuffTimer = al_create_timer(1.0);

	//TODO: Unregister some timers when we're in the menu?
	registerEventSource(al_get_display_event_source(graphicsMan->getDisplay()));
	registerEventSource(al_get_timer_event_source(updateTimer));
	registerEventSource(al_get_timer_event_source(fpsCounter));
	registerEventSource(al_get_timer_event_source(hintCounter));
	registerEventSource(al_get_timer_event_source(doStuffTimer));

	registerEventSource(al_get_keyboard_event_source());
	registerEventSource(al_get_mouse_event_source());
	registerEventSource(al_get_joystick_event_source());

	al_start_timer(updateTimer);
	al_start_timer(fpsCounter);
	al_start_timer(hintCounter);
	al_start_timer(doStuffTimer);

	frame = 0;

	//Main game loop
	while(1)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		bool redraw = false;

		if (inputMan->listenBinding)
		{
			if (ev.type == ALLEGRO_EVENT_KEY_UP || ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP || ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN || ev.type == ALLEGRO_EVENT_MOUSE_AXES || ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
			{
				inputMan->updateBinding(ev);
			}
		}
		else if (!quit)
		{
			if (ev.type == ALLEGRO_EVENT_KEY_UP || ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				inputMan->accumulateKeyboardEvent(&ev);
			}
			else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP || ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
			{
				inputMan->accumulateMouseButtonEvent(&ev);
			}
			else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
			{
				inputMan->accumulateMouseAxisEvent(&ev);
			}
			else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP || ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
			{
				inputMan->accumulateJoystickButtonEvent(&ev);
			}
			else if (ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
			{
				inputMan->accumulateJoystickAxisEvent(&ev);
			}
		}

		if(ev.timer.source == fpsCounter)
		{
			if (settingsMan->getShowFPS())
			{
				char integer_string[4];

				sprintf(integer_string, "%d", frame);

				char text[64] = "FPS: ";

				strcat(text, integer_string);

				fpsText->setText(text);
				frame = 0;
			}
		}
		if(ev.timer.source == hintCounter)
		{
			if (settingsMan->getShowHints() && !rules[currentRules]->suppressHints)
			{
				processHintState();
			}
			else
			{
				setHintVisibility(false);
			}
		}

		if(ev.timer.source == doStuffTimer)
		{
			doStuff();
		}


		if(ev.timer.source == updateTimer)
		{
			if(!quit)
			{
				//FIXME: New input state caching doesn't know if a button was pressed and released within the last frame. Caching should be done on events.
				inputMan->processInput();
			}

			gameTick();
			redraw = true;
		}
		else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			break;
		}

		if((redraw && al_is_event_queue_empty(event_queue)) || quit)
		{
			redraw = false;
			if(currentScreen != NULL)
			{
				if (currentScreen->isClosing())
				{
					int focusedWidget = currentScreen->getFocusedWidgetIndex();

					//TODO: Give screen a pointer to its own setup function so that we can call it without having to do checks?
					if (currentScreen == screenMenu)
					{
						setupScreenMenu();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenNew)
					{
						setupScreenNewGame();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenHowToPlay)
					{
						setupScreenHowToPlay();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenStats)
					{
						setupScreenStats();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenSettings)
					{
						setupScreenSettings();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenBindings)
					{
						setupScreenBindings(INPUT_TYPE_KEY1);
						showScreen(screenBindings);
					}
					else if (currentScreen == screenPause)
					{
						setupScreenPause();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenDeath)
					{
						setupScreenDeath();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenEnd)
					{
						setupScreenEnd();
						showScreen(screenSettings);
					}
					else if (currentScreen == screenCredits)
					{
						setupScreenCredits();
						showScreen(screenSettings);
					}
					currentScreen->setFocusedWidget(focusedWidget);
				}
				graphicsMan->drawScreen(currentScreen);
			}
			else
			{
				graphicsMan->draw();
			}

			if(quit)
			{
				break;
			}
			frame++;
		}
	}

	//Program exited normally.
	return EXIT_SUCCESS;
}

GraphicsManager* Game::getGraphicsMan()
{
	return graphicsMan;
}

AudioManager* Game::getAudioMan()
{
	return audioMan;
}

SettingsManager* Game::getSettingsMan()
{
	return settingsMan;
}

InputManager* Game::getInputMan()
{
	return inputMan;
}

void Game::loadBitmaps()
{

	projectileBitmapStarburst = DrawableBitmap("gfx/starburst.png");
	projectileBitmapHalo = DrawableBitmap("gfx/halo.png");
	projectileBitmapStarburst2 = DrawableBitmap("gfx/starburst2.png");
	projectileBitmapHalo2 = DrawableBitmap("gfx/halo2.png");
	projectileBitmapStarburst.setFloatingHeight(138.0f);
	projectileBitmapHalo.setFloatingHeight(138.0f);


	detailBitmap[0] = DrawableBitmap("gfx/scuff1.png");
	detailBitmap[1] = DrawableBitmap("gfx/scuff2.png");
	detailBitmap[2] = DrawableBitmap("gfx/scuff3.png");
	detailBitmap[3] = DrawableBitmap("gfx/scuff4.png");
	detailBitmap[4] = DrawableBitmap("gfx/scuff5.png");
	detailBitmap[5] = DrawableBitmap("gfx/scuff6.png");
	detailBitmap[6] = DrawableBitmap("gfx/scuff1.png");
	detailBitmap[7] = DrawableBitmap("gfx/scuff2.png");
	detailBitmap[8] = DrawableBitmap("gfx/scuff3.png");
	detailBitmap[9] = DrawableBitmap("gfx/scuff4.png");
	detailBitmap[10] = DrawableBitmap("gfx/scuff5.png");
	detailBitmap[11] = DrawableBitmap("gfx/scuff6.png");
	detailBitmap[12] = DrawableBitmap("gfx/scuff1.png");
	detailBitmap[13] = DrawableBitmap("gfx/scuff2.png");
	detailBitmap[14] = DrawableBitmap("gfx/scuff3.png");
	detailBitmap[15] = DrawableBitmap("gfx/scuff4.png");
	detailBitmap[16] = DrawableBitmap("gfx/scuff5.png");
	detailBitmap[17] = DrawableBitmap("gfx/scuff6.png");

	for(int i = 0; i < 18; i++) {
		detailBitmap[i].setReflection(false);
	}



	crystalA[0] = DrawableBitmap("gfx/crystal00.png");
	crystalA[1] = DrawableBitmap("gfx/crystal01.png");
	crystalA[2] = DrawableBitmap("gfx/crystal02.png");
	crystalA[3] = DrawableBitmap("gfx/crystal03.png");
	crystalA[4] = DrawableBitmap("gfx/crystal04.png");
	crystalA[5] = DrawableBitmap("gfx/crystal05.png");
	crystalA[6] = DrawableBitmap("gfx/crystal06.png");
	crystalA[7] = DrawableBitmap("gfx/crystal07.png");
	crystalA[8] = DrawableBitmap("gfx/crystal08.png");
	crystalA[9] = DrawableBitmap("gfx/crystal09.png");
	crystalA[10] = DrawableBitmap("gfx/crystal10.png");
	crystalA[11] = DrawableBitmap("gfx/crystal11.png");
	crystalA[12] = DrawableBitmap("gfx/crystal12.png");
	crystalA[13] = DrawableBitmap("gfx/crystal13.png");
	crystalA[14] = DrawableBitmap("gfx/crystal14.png");
	crystalA[15] = DrawableBitmap("gfx/crystal15.png");

	for(int i = 0; i < 16; i++) {
		crystalA[i].setReflection(true);
	}



	crystalB[0] = DrawableBitmap("gfx/crystalb00.png");
	crystalB[1] = DrawableBitmap("gfx/crystalb01.png");
	crystalB[2] = DrawableBitmap("gfx/crystalb02.png");
	crystalB[3] = DrawableBitmap("gfx/crystalb03.png");
	crystalB[4] = DrawableBitmap("gfx/crystalb04.png");
	crystalB[5] = DrawableBitmap("gfx/crystalb05.png");
	crystalB[6] = DrawableBitmap("gfx/crystalb06.png");
	crystalB[7] = DrawableBitmap("gfx/crystalb07.png");
	crystalB[8] = DrawableBitmap("gfx/crystalb08.png");
	crystalB[9] = DrawableBitmap("gfx/crystalb09.png");
	crystalB[10] = DrawableBitmap("gfx/crystalb10.png");
	crystalB[11] = DrawableBitmap("gfx/crystalb11.png");
	crystalB[12] = DrawableBitmap("gfx/crystalb12.png");
	crystalB[13] = DrawableBitmap("gfx/crystalb13.png");
	crystalB[14] = DrawableBitmap("gfx/crystalb14.png");
	crystalB[15] = DrawableBitmap("gfx/crystalb15.png");

	for(int i = 0; i < 16; i++) {
		crystalB[i].setReflection(true);
	}


	crystalC[0] = DrawableBitmap("gfx/crystalc00.png");
	crystalC[1] = DrawableBitmap("gfx/crystalc01.png");
	crystalC[2] = DrawableBitmap("gfx/crystalc02.png");
	crystalC[3] = DrawableBitmap("gfx/crystalc03.png");
	crystalC[4] = DrawableBitmap("gfx/crystalc04.png");
	crystalC[5] = DrawableBitmap("gfx/crystalc05.png");
	crystalC[6] = DrawableBitmap("gfx/crystalc06.png");
	crystalC[7] = DrawableBitmap("gfx/crystalc07.png");
	crystalC[8] = DrawableBitmap("gfx/crystalc08.png");
	crystalC[9] = DrawableBitmap("gfx/crystalc09.png");
	crystalC[10] = DrawableBitmap("gfx/crystalc10.png");
	crystalC[11] = DrawableBitmap("gfx/crystalc11.png");
	crystalC[12] = DrawableBitmap("gfx/crystalc12.png");
	crystalC[13] = DrawableBitmap("gfx/crystalc13.png");
	crystalC[14] = DrawableBitmap("gfx/crystalc14.png");
	crystalC[15] = DrawableBitmap("gfx/crystalc15.png");

	for(int i = 0; i < 16; i++) {
		crystalC[i].setReflection(true);
	}


/*	splatBitmap[0] = DrawableBitmap("gfx/splat0.png");
	splatBitmap[1] = DrawableBitmap("gfx/splat1.png");
	splatBitmap[2] = DrawableBitmap("gfx/splat2.png");
	splatBitmap[3] = DrawableBitmap("gfx/splat3.png");
	sparkBitmap[0] = DrawableBitmap("gfx/spark0.png");
	sparkBitmap[1] = DrawableBitmap("gfx/spark1.png");
	sparkBitmap[2] = DrawableBitmap("gfx/spark2.png");
*/
	splatBitmap.clear();
	splatBitmap.push_back(DrawableBitmap("gfx/splat0.png"));
	splatBitmap.push_back(DrawableBitmap("gfx/splat1.png"));
	splatBitmap.push_back(DrawableBitmap("gfx/splat2.png"));
	splatBitmap.push_back(DrawableBitmap("gfx/splat3.png"));

	sparkBitmap.clear();
	sparkBitmap.push_back(DrawableBitmap("gfx/spark0.png"));
	sparkBitmap.push_back(DrawableBitmap("gfx/spark1.png"));
	sparkBitmap.push_back(DrawableBitmap("gfx/spark2.png"));
	sparkBitmap.push_back(DrawableBitmap("gfx/spark2.png"));
	sparkBitmap.push_back(DrawableBitmap("gfx/spark3.png"));
	sparkBitmap.push_back(DrawableBitmap("gfx/spark3.png"));

	grunt[0][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_00.png");
	grunt[1][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_01.png");
	grunt[2][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_02.png");
	grunt[3][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_03.png");
	grunt[4][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_04.png");
	grunt[5][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_05.png");
	grunt[6][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_06.png");
	grunt[7][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_07.png");
	grunt[8][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_08.png");
	grunt[9][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_09.png");
	grunt[10][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_10.png");
	grunt[11][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_11.png");
	grunt[12][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_12.png");
	grunt[13][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_13.png");
	grunt[14][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_14.png");
	grunt[15][0] = DrawableBitmap("gfx/skate_anim_yellow/skate_f1_15.png");

	grunt[0][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_00.png");
	grunt[1][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_01.png");
	grunt[2][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_02.png");
	grunt[3][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_03.png");
	grunt[4][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_04.png");
	grunt[5][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_05.png");
	grunt[6][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_06.png");
	grunt[7][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_07.png");
	grunt[8][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_08.png");
	grunt[9][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_09.png");
	grunt[10][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_10.png");
	grunt[11][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_11.png");
	grunt[12][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_12.png");
	grunt[13][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_13.png");
	grunt[14][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_14.png");
	grunt[15][1] = DrawableBitmap("gfx/skate_anim_yellow/skate_f2_15.png");

	grunt[0][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_00.png");
	grunt[1][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_01.png");
	grunt[2][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_02.png");
	grunt[3][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_03.png");
	grunt[4][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_04.png");
	grunt[5][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_05.png");
	grunt[6][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_06.png");
	grunt[7][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_07.png");
	grunt[8][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_08.png");
	grunt[9][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_09.png");
	grunt[10][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_10.png");
	grunt[11][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_11.png");
	grunt[12][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_12.png");
	grunt[13][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_13.png");
	grunt[14][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_14.png");
	grunt[15][2] = DrawableBitmap("gfx/skate_anim_yellow/skate_f3_15.png");

	grunt[0][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_00.png");
	grunt[1][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_01.png");
	grunt[2][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_02.png");
	grunt[3][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_03.png");
	grunt[4][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_04.png");
	grunt[5][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_05.png");
	grunt[6][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_06.png");
	grunt[7][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_07.png");
	grunt[8][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_08.png");
	grunt[9][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_09.png");
	grunt[10][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_10.png");
	grunt[11][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_11.png");
	grunt[12][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_12.png");
	grunt[13][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_13.png");
	grunt[14][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_14.png");
	grunt[15][3] = DrawableBitmap("gfx/skate_anim_yellow/skate_f4_15.png");

	grunt[0][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_00.png");
	grunt[1][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_01.png");
	grunt[2][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_02.png");
	grunt[3][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_03.png");
	grunt[4][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_04.png");
	grunt[5][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_05.png");
	grunt[6][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_06.png");
	grunt[7][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_07.png");
	grunt[8][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_08.png");
	grunt[9][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_09.png");
	grunt[10][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_10.png");
	grunt[11][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_11.png");
	grunt[12][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_12.png");
	grunt[13][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_13.png");
	grunt[14][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_14.png");
	grunt[15][4] = DrawableBitmap("gfx/skate_anim_yellow/skate_f5_15.png");

	grunt[0][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_00.png");
	grunt[1][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_01.png");
	grunt[2][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_02.png");
	grunt[3][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_03.png");
	grunt[4][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_04.png");
	grunt[5][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_05.png");
	grunt[6][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_06.png");
	grunt[7][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_07.png");
	grunt[8][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_08.png");
	grunt[9][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_09.png");
	grunt[10][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_10.png");
	grunt[11][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_11.png");
	grunt[12][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_12.png");
	grunt[13][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_13.png");
	grunt[14][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_14.png");
	grunt[15][5] = DrawableBitmap("gfx/skate_anim_yellow/skate_f6_15.png");

	grunt[0][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_00.png");
	grunt[1][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_01.png");
	grunt[2][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_02.png");
	grunt[3][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_03.png");
	grunt[4][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_04.png");
	grunt[5][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_05.png");
	grunt[6][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_06.png");
	grunt[7][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_07.png");
	grunt[8][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_08.png");
	grunt[9][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_09.png");
	grunt[10][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_10.png");
	grunt[11][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_11.png");
	grunt[12][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_12.png");
	grunt[13][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_13.png");
	grunt[14][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_14.png");
	grunt[15][6] = DrawableBitmap("gfx/skate_anim_yellow/skate_f7_15.png");

	for(int i = 0; i < 16; i++) {
		for(int j = 0; j < 7; j++) {
			grunt[i][j].setReflection(true);
		}
	}
}

FrameSuperContainer Game::generateCharacterSuperContainer() {
	FrameSuperContainer superC = FrameSuperContainer();

	for(int i = 0; i < 16; i++) {
		FrameContainer c = FrameContainer();

		for(int j = 0; j < 7; j++) {
			c.push(grunt[i][j]);
		}

		for(int j = 5; j >= 1; j--) {
			c.push(grunt[i][j]);
		}

		superC.push(c);
	}

	return superC;
}

void Game::showScreen(GUIScreen* s)
{
	if (s != NULL)
	{
		s->show();
		if (s == screenMenu)
		{
			audioMan->setGameState(GAME_STATE_NORMAL, true);
		}
		if (s == screenStats)
		{
			if (player != NULL)
			{
				std::stringstream s;
				for (unsigned int i = 0; i < MAX_ENEMY_TYPE; i++)
				{
					if (player->getKillStats()[i] > 0)
					{
						s.str("");
						s << player->getKillStats()[i];
						enemyStatKills[i]->setText(s.str());
						enemyStatImages[i]->setVisibility(true);
					}
					else
					{
						enemyStatKills[i]->setText("");
						enemyStatImages[i]->setVisibility(false);
					}
				}
				for (unsigned int i = 0; i < 3; i++)
				{
					if ((int)i <= player->weaponPickup)
					{
						s.str("");
						s << player->weaponList[i]->getShotsFired() << " shots";
						weaponStatShots[i]->setText(s.str());
						s.str("");
						s << "(" << std::fixed << std::setprecision(0) << player->weaponList[i]->getHitPercentage() << "%)";
						Print("Hits", player->weaponList[i]->getHitPercentage());
						weaponStatHits[i]->setText(s.str());
						weaponStatImages[i]->setVisibility(true);
					}
					else
					{
						weaponStatShots[i]->setText("");
						weaponStatHits[i]->setText("");
						weaponStatImages[i]->setVisibility(false);
					}
				}
				s.str("");
				s << std::fixed << std::setfill('0') << std::setw(2) << (int)(player->getTime() / 60) << ":" << std::setw(5) << std::setprecision(2) << std::setfill('0') <<fmod(player->getTime(), 60);
				timeStat->setText(s.str());
			}
		}
	}

	if (currentScreen == screenSettings)
	{
		settingsMan->saveSettings();
	}
	else if (currentScreen == screenBindings)
	{
		settingsMan->saveBindings();
	}
	currentScreen = s;
}

void Game::doPause(bool state)
{
	if (state)
	{
		Print("Pausing");
		paused = true;
		inputMan->mouseReady = true;
		inputMan->setGrabMouse(false);
		audioMan->duckMusicVolume(true);
	}
	else
	{
		Print("Unpausing");
		inputMan->setGrabMouse(true);
		paused = false;
		unpausing = true;
		inputMan->mouseReady = false;
		showScreen(NULL);
		audioMan->duckMusicVolume(false);
	}
}
