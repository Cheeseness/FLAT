/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIBUTTON_H
#define GUIBUTTON_H

#include "GUIWidget.h"

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>

class GUIButton : public GUIWidget
{
	public:
		GUIButton();
		GUIButton(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, const char* _text, ACTION a);
		virtual ~GUIButton();

		void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void setText(const char* _text);
		void setFont(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color);
		void printDetails();

	protected:
		ALLEGRO_FONT* font;
		ALLEGRO_COLOR color;
		char* text;

	private:
};

#endif //GUIBUTTON_H
