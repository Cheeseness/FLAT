/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIToggle.h"
#include "Game.h"


GUIToggle::GUIToggle()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = NULL;
	color = al_map_rgb(255, 255, 255);
	text = (char*)"";
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	isPressed = false;
	visible = true;
	dirty = true;
	borderWidth = 2;
	toggleMargin = 2;
	toggleMargin = 5;
	updateSizePlus();
}

GUIToggle::GUIToggle(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, const char* _text, ACTIONBOOL a)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = _font;
	color = _color;
	setText(_text);
	hasFocus = false;
	action = a;
	leftAlign = true;
	isPressed = false;
	visible = true;
	dirty = true;
	borderWidth = 2;
	toggleMargin = 2;
	toggleGrowth = 5;
}

GUIToggle::~GUIToggle()
{

}

void GUIToggle::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible || !dirty)
	{
		return;
	}

	int boxX = positionPlus.x;
	int boxXEnd = positionPlus.x;
	int textX = positionPlus.x + padding.x;
	int textY = positionPlus.y + padding.y;
	int boxYEnd = positionPlus.y + al_get_font_ascent(font) + padding.y;

	if (leftAlign)
	{
		boxXEnd += al_get_font_ascent(font);
		textX += al_get_font_ascent(font) + padding.y * 2;
	}
	else
	{
		boxX += sizePlus.x + padding.x - al_get_font_ascent(font);
		boxXEnd += sizePlus.x + padding.x;
		textX += -al_get_font_ascent(font) - padding.y * 2;
	}

	int boxY = textY;

	if (!hasFocus && !isMouseOver)
	{
		boxX += toggleGrowth;
		boxY += toggleGrowth;
		boxXEnd -= toggleGrowth;
		boxYEnd -= toggleGrowth;
	}

	al_draw_rectangle(boxX, boxY, boxXEnd, boxYEnd, color, borderWidth);

	if (isPressed)
	{
		al_draw_filled_rectangle(boxX + toggleMargin, boxY + toggleMargin, boxXEnd - toggleMargin, boxYEnd - toggleMargin, color);
	}
	al_draw_text(font, color, textX, textY, 0, text);
}

void GUIToggle::setValue(bool p)
{
	isPressed = p;
	dirty = true;
}

bool GUIToggle::setValue()
{
	return isPressed;
}

void GUIToggle::updateSizePlus()
{
	if (size.y < 20)
	{
		toggleGrowth = 2;
	}
	else
	{
		toggleGrowth = 5;
	}

	GUIWidget::updateSizePlus();
	dirty = true;
}

void GUIToggle::doAction()
{
	Game::getGame()->getAudioMan()->playUISound(SOUND_UI_SELECT, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
	isPressed = !isPressed;
	action(isPressed);
}
