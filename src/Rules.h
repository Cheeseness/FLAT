/*
Copyright 2022-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RULES_H
#define RULES_H

	class Rules {
		public:
			//ENEMY
			int enemyFreq;
			float enemySpeed;
			float enemySpeedChase;
			float enemySpeedCircle;
			float enemySpeedBrake;
			float enemyHealth;
			float enemyFireTime;
			float enemyFireRange;
			float enemyProjectileDamage;
			float enemyProjectileSpeed;
			unsigned int enemyMax;
			unsigned int enemyMaxBoss;

			//BOSS
			int dragonTailLength;
			float dragonAwareDist;
			float dragonTerminalVelocity1;
			float dragonAccelBase1;
			float dragonAccelExtra1;
			float dragonChargeDist;
			float dragonAttackDamage;

			float dragonTerminalVelocity2;
			float dragonAccelBase2;
			float dragonFireTime2;
			float dragonProjectileDamage;
			float dragonProjectileSpeed;

			float dragonKBForce;
			float dragonKBAir;
			float dragonKBRange;
			float dragonKBAirtime;

			float dragonHealth1;
			float dragonHealth2;
			float dragonHealthTail;

			//CRATES
			int crateFreq;
			int crateTimeLimit;
			int crateTime;
			//int crateEnemyFreq;
			int crateEnemyCountStep;
			int crateWeaponFreq;
			int crateHealthCount;
			int crateAmmoCount;

			//PLAYER
			int playerAmmo;
			float playerHealth;
			float playerSpeed;
			float playerSpeedCarving;
			float playerSpeedBackMultiplier;


			//General
			float projectileDamage;
			unsigned int startingEnemies;
			unsigned int startingCrates;
			unsigned int startingDragons;
			bool suppressHints;

			//TODO: Weapons?
		Rules()
		{
			//TODO: Boss spawn behaviour/count

			dragonTailLength = 40;
			dragonAwareDist = 100.0f;
			dragonTerminalVelocity1 = 0.8f;
			dragonAccelBase1 = 0.02f;
			dragonAccelExtra1 = 0.03f;
			dragonChargeDist = 30.0f;

			dragonTerminalVelocity2 = 0.1f;
			dragonAccelBase2 = 0.003f; //TODO: Default value for this feels way too [way too what, Past Cheese???]
			dragonFireTime2 = 1.5f;
			dragonProjectileDamage = 0.2f;
			dragonProjectileSpeed = 1.0f;

			dragonAttackDamage = 0.25;

			dragonKBForce = 1.0f;
			dragonKBAir = 7.0f;
			dragonKBRange = 2.0f;
			dragonKBAirtime = 0.1f;

			dragonHealth1 = 40.0f;
			dragonHealth2 = 10.0f;
			dragonHealthTail = 1.0f;

			enemyFreq = 7;
			enemySpeed = 0.005f;
			enemySpeedChase = 0.004f;
			enemySpeedCircle = 0.004f;
			enemySpeedBrake = 0.01f;
			enemyHealth = 1.5f;
			enemyFireTime = 4.0f;
			enemyFireRange = 60.0f;
			enemyProjectileDamage = 0.2f;
			enemyProjectileSpeed = 0.8f;


			enemyMax = 12;
			enemyMaxBoss = 2;

			crateFreq = 10;
			crateTime = 15;
			crateTimeLimit = -15;
			crateWeaponFreq = 100;
			crateHealthCount = 2;
			crateEnemyCountStep = 3;
			crateAmmoCount = 100;

			playerAmmo = -1; //TODO: Implement limited ammo
			playerHealth = 1.0f;
			playerSpeed = 0.10f;
			playerSpeedCarving = 0.018f;
			playerSpeedBackMultiplier = 1.0f;

			projectileDamage = 0.2f;
			startingEnemies = 0;
			startingDragons = 0;
			startingCrates = 1;
			suppressHints = false; //TODO: Implement per-ruleset hints?

		}
		~Rules()
		{
		
		}


	};

#endif
