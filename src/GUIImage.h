/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIIMAGE_H
#define GUIIMAGE_H

#include "GUIWidget.h"

class GUIImage : public GUIWidget
{
	public:
		GUIImage(ALLEGRO_BITMAP* i);
		virtual ~GUIImage();

		void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void setImage(ALLEGRO_BITMAP* i);
		void setReflection(bool r);

	protected:
		void updateSizePlus();

	private:
		ALLEGRO_BITMAP* image;
		bool reflection;
		vec2 aspectMultiplier;
};

#endif //GUITEXT_H
