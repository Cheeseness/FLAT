/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIWIDGET_H
#define GUIWIDGET_H

#include <allegro5/allegro.h>

#include <vector>

#include "vec2.h"
#include "utilities.h"

#define ANCHOR_TL 0
#define ANCHOR_TR 1
#define ANCHOR_BR 2
#define ANCHOR_BL 3

typedef void (*ACTION)();
typedef void (*ACTIONBOOL)(bool b);
typedef void (*ACTIONFLOAT)(float v);

class GUIWidget
{
	public:
		GUIWidget();
		virtual ~GUIWidget();

		virtual void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void setPosition(vec2 pos);
		void setPosition(vec2 pos, int anchorMode, vec2 target);
		void setPosition(vec2 pos, int anchorMode, GUIWidget* anchor, int targetMode);
		vec2 getPosition();
		vec2 getPositionPlus();
		void setSize(vec2 s);
		vec2 getSize();
		vec2 getSizePlus();
		void setMargin(vec2 m);
		void setPadding(vec2 p);
		bool getFocusState();
		virtual void setFocusState(bool focus, bool silent = false);
		void setHoverState(bool hover);
		virtual GUIWidget* getFocusedWidget();
		virtual void setVisibility(bool v);
		virtual bool checkCoords(vec2 coords, bool takeFocus, bool doClick);
		virtual bool doLeft();
		virtual bool doRight();
		virtual bool isDirty();
		void setAction(ACTION a);
		virtual void doAction();
		void unclick();
		virtual void printDetails();

	protected:
		virtual void updateSizePlus();
		virtual void updatePositionPlus();
		virtual void checkClickExtra(vec2 coords);

		vec2 position;
		vec2 positionPlus;
		vec2 size;
		vec2 sizePlus;
		vec2 padding;
		vec2 margin;
		bool hasFocus;
		bool isClicked;
		bool isMouseOver;
		bool leftAlign;
		bool visible;
		bool dirty;
		ACTION action;
	private:
};

#endif //GUIWIDGET_H
