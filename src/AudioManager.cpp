/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2012-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cmath>

#include "AudioManager.h"


AudioManager::AudioManager()
{
	gameState = GAME_STATE_NONE;

	Print("Loading sound effects");
	sounds[SOUND_ENEMY_FIRE] = al_load_sample("sfx/zap_2.ogg");
	sounds[SOUND_ENEMY_SKATE] = al_load_sample("sfx/enemy_skate.ogg");
	sounds[SOUND_ENEMY_PAIN] = al_load_sample("sfx/enemy_pain.ogg");
	sounds[SOUND_ENEMY_DEATH] = al_load_sample("sfx/enemy_death.ogg");
	sounds[SOUND_DRAGON_ROAR1] = al_load_sample("sfx/dragon_roar_2.ogg");
	sounds[SOUND_DRAGON_ROAR2] = al_load_sample("sfx/dragon_roar_1.ogg");
	sounds[SOUND_DRAGON_PAIN] = al_load_sample("sfx/dragon_pain.ogg");
	sounds[SOUND_DRAGON_DEATH] = al_load_sample("sfx/dragon_death.ogg");
	sounds[SOUND_DRAGON_AMBIENCE] = al_load_sample("sfx/dragon_ambience.ogg");
	sounds[SOUND_PICKUP_HUM] = al_load_sample("sfx/pickup_spawn.ogg");

	sounds[SOUND_PISTOL_FIRE] = al_load_sample("sfx/gun.ogg");
	sounds[SOUND_SHOTGUN_FIRE] = al_load_sample("sfx/shotgun.ogg");
	sounds[SOUND_SMG_FIRE] = al_load_sample("sfx/smg.ogg");

	sounds[SOUND_UI_FOCUS] = al_load_sample("sfx/gui_hover2.ogg");
	sounds[SOUND_UI_HOVER] = al_load_sample("sfx/gui_hover2.ogg");
	sounds[SOUND_UI_HOVER_SHORT] = al_load_sample("sfx/gui_hover_short.ogg");
	sounds[SOUND_UI_SELECT] = al_load_sample("sfx/gui_select2.ogg");

	normalMusic = al_load_sample("music/normal.ogg");
	combatMusic = al_load_sample("music/combat.ogg");
	dragonMusic = al_load_sample("music/dragon.ogg");
	first = true;

	globalMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);
	musicMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);
	uiMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);
	effectsMixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2);

	normalInstance = al_create_sample_instance(normalMusic);
	al_set_sample_instance_playmode(normalInstance, ALLEGRO_PLAYMODE_LOOP);
	al_set_sample_instance_gain(normalInstance, 0.0f);
	al_attach_sample_instance_to_mixer(normalInstance, musicMixer);

	combatInstance = al_create_sample_instance(combatMusic);
	al_set_sample_instance_playmode(combatInstance, ALLEGRO_PLAYMODE_LOOP);
	al_set_sample_instance_gain(combatInstance, 0.0f);
	al_attach_sample_instance_to_mixer(combatInstance, musicMixer);

	dragonInstance = al_create_sample_instance(dragonMusic);
	al_set_sample_instance_playmode(dragonInstance, ALLEGRO_PLAYMODE_LOOP);
	al_set_sample_instance_gain(dragonInstance, 0.0f);
	al_attach_sample_instance_to_mixer(dragonInstance, musicMixer);

	if (!al_attach_mixer_to_mixer(musicMixer, globalMixer))
	{
		Alert("Unable to set up music mixer!");
	}


	if (!al_attach_mixer_to_mixer(uiMixer, globalMixer))
	{
		Alert("Unable to set up ui mixer!");
	}


	if (!al_attach_mixer_to_mixer(effectsMixer, globalMixer))
	{
		Alert("Unable to set up effects mixer!");

	}


	voice = al_create_voice(44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2);
	if (!al_attach_mixer_to_voice(globalMixer, voice))
	{
		Alert("Unable to set up global mixer!");
	}

	effectsSampleID = 0;
	for (int i = 0; i < MAX_EFFECT_SAMPLES; i++)
	{
		effectsSamplePool[i] = al_create_sample_instance(NULL);
		al_attach_sample_instance_to_mixer(effectsSamplePool[i], effectsMixer);
	}
	uiSampleID = 0;
	for (int i = 0; i < MAX_UI_SAMPLES; i++)
	{
		uiSamplePool[i] = al_create_sample_instance(NULL);
		al_attach_sample_instance_to_mixer(uiSamplePool[i], uiMixer);
	}

	for (int i = 0; i < MAX_EFFECT_UNINTERRUPTABLE_SAMPLES; i++)
	{
		effectsUnintPool[i] = al_create_sample_instance(NULL);
		al_attach_sample_instance_to_mixer(effectsUnintPool[i], effectsMixer);
	}

	for (int i = 0; i < MAX_UI_UNINTERRUPTABLE_SAMPLES; i++)
	{
		uiUnintPool[i] = al_create_sample_instance(NULL);
		al_attach_sample_instance_to_mixer(uiUnintPool[i], uiMixer);
	}

	musicVolume = 0.7f;
	effectsVolume = 0.7f;
	uiVolume = 0.7f;
	globalVolume = 1.0f;

	isDucked = false;
	//ctor
}

void AudioManager::setMusicVolume(float volume)
{
	//FIXME: Since music is ducked whenever we're adjusting the settings, this isn't accurate :(
	musicVolume = volume;
	al_set_mixer_gain(musicMixer, musicVolume);
	if (!first)
	{
		updateStateAudio(gameState);
	}
}

void AudioManager::setEffectsVolume(float volume, bool silent)
{
	effectsVolume = volume;
	al_set_mixer_gain(effectsMixer, effectsVolume);
	if (!silent)
	{
		playEffectUnintSound(sounds[SOUND_UI_HOVER_SHORT], 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, NULL);
	}
}

void AudioManager::setUIVolume(float volume, bool silent)
{
	uiVolume = volume;
	al_set_mixer_gain(uiMixer, uiVolume);
	if (!silent)
	{
		playUIUnintSound(sounds[SOUND_UI_HOVER_SHORT], 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, NULL);
	}
}

void AudioManager::setGlobalVolume(float volume)
{
	globalVolume = volume;
	al_set_mixer_gain(globalMixer, globalVolume);
}

void AudioManager::duckMusicVolume(bool duck)
{
	isDucked = duck;
	if (!first)
	{
		updateStateAudio(gameState);
	}
}

bool AudioManager::playEffectUnintSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id)
{
	return playUnintSound(effectsUnintPool, MAX_EFFECT_UNINTERRUPTABLE_SAMPLES, sound, gain, pan, speed, loop, id);
}

bool AudioManager::playUIUnintSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id)
{
	return playUnintSound(uiUnintPool, MAX_UI_UNINTERRUPTABLE_SAMPLES, sound, gain, pan, speed, loop, id);
}

bool AudioManager::playUnintSound(ALLEGRO_SAMPLE_INSTANCE* pool[], int poolSize, ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id)
{
	for (int i = 0; i < poolSize; i++)
	{
		if (!al_get_sample_instance_playing(pool[i]))
		{
			playSound(pool[i], sound, gain, pan, speed, loop);
			if (id != NULL)
			{
				*id = i;
			}
			return true;
		}
	}
	if (id != NULL)
	{
		*id = -1;
	}
	return false;
}

bool AudioManager::stopEffectUnintSound(ALLEGRO_SAMPLE* sound, int id)
{
	return stopUnintSound(effectsUnintPool, MAX_EFFECT_UNINTERRUPTABLE_SAMPLES, sound, &id);
}

bool AudioManager::stopUIUnintSound(ALLEGRO_SAMPLE* sound, int id)
{
	return stopUnintSound(uiUnintPool, MAX_UI_UNINTERRUPTABLE_SAMPLES, sound, &id);
}

bool AudioManager::stopUnintSound(ALLEGRO_SAMPLE_INSTANCE* pool[], int poolSize, ALLEGRO_SAMPLE* sound, int* id)
{
	if (id == NULL || *id < 0)
	{
		for (int i = 0; i < poolSize; i++)
		{
			if (al_get_sample_instance_playing(pool[i]))
			{
				ALLEGRO_SAMPLE* s = al_get_sample(pool[i]);
				if (al_get_sample_data(s) == sound)
				{
					al_stop_sample_instance(pool[i]);
					return true;
				}
			}
		}
	}
	else
	{
		if (al_get_sample_instance_playing(pool[*id]))
		{
			ALLEGRO_SAMPLE* s = al_get_sample(pool[*id]);
			//FIXME: contrary to docs, al_get_sample_data() doesn't return the right value :/
			if (al_get_sample_data(s) == sound || true)
			{
				al_stop_sample_instance(pool[*id]);
				return true;
			}

		}
		else
		{
			Print("Can't stop unint sample - ID wasn't playing? ", *id);
		}
	}
	return false;
}

void AudioManager::playEffectSound(int sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
{
	playEffectSound(sounds[sound], gain, pan, speed, loop);
}

void AudioManager::playEffectSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
{
	playSound(effectsSamplePool[effectsSampleID], sound, gain, pan, speed, loop);
	effectsSampleID += 1;
	if (effectsSampleID >= MAX_EFFECT_SAMPLES)
	{
		effectsSampleID = 0;
	}
}

void AudioManager::playUISound(int sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
{
	playUISound(sounds[sound], gain, pan, speed, loop);
}

void AudioManager::playUISound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
{
	playSound(uiSamplePool[uiSampleID], sound, gain, pan, speed, loop);
	uiSampleID += 1;
	if (uiSampleID >= MAX_UI_SAMPLES)
	{
		uiSampleID = 0;
	}
}


void AudioManager::playSound(ALLEGRO_SAMPLE_INSTANCE* sample, ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
{
	al_set_sample(sample, sound);
	al_set_sample_instance_gain(sample, gain);
	al_set_sample_instance_pan(sample, pan);
	al_set_sample_instance_speed(sample, speed);
	al_set_sample_instance_playmode(sample, loop);
	al_play_sample_instance(sample);
}


int AudioManager::getGameState()
{
	return gameState;
}

void AudioManager::setGameState(int newState, bool force)
{
	if (newState != gameState && (gameState != GAME_STATE_DRAGON || force))
	{
		gameState = newState;
		updateStateAudio(gameState);
	}
}

void AudioManager::setGameState(int newState)
{
	setGameState(newState, false);
}

void AudioManager::updateStateAudio(int newState)
{
	switch (newState)
	{
		case GAME_STATE_NONE:
			stopAudio();
			first = true;
			break;

		case GAME_STATE_COMBAT:
			if (newState != gameState)
			{
				Print("Setting combat state.");
			}
			setAudioCue(combatInstance);
			break;

		case GAME_STATE_DRAGON:
			if (newState != gameState)
			{
				Print("Setting dragon state.");
			}
			setAudioCue(dragonInstance);
			break;

		case GAME_STATE_NORMAL:
		default:
			if (newState != gameState)
			{
				Print("Setting normal state.");
			}
			setAudioCue(normalInstance);
			break;
	}
}

void AudioManager::stopAudio()
{
	al_stop_sample_instance(normalInstance);
	al_stop_sample_instance(combatInstance);
	al_stop_sample_instance(dragonInstance);
}

void AudioManager::setAudioCue(ALLEGRO_SAMPLE_INSTANCE* newCue)
{
	if (first)
	{
		al_play_sample_instance(normalInstance);
		al_play_sample_instance(combatInstance);
		al_play_sample_instance(dragonInstance);

		first = false;
	}

	float volume = isDucked ? max(0.0f, musicVolume - 0.5f * musicVolume) : musicVolume;
	al_set_mixer_gain(musicMixer, volume);

	al_set_sample_instance_gain(normalInstance, normalInstance == newCue ? 1.0f : 0.0f);
	al_set_sample_instance_gain(combatInstance, combatInstance == newCue ? 1.0f : 0.0f);
	al_set_sample_instance_gain(dragonInstance, dragonInstance == newCue ? 1.0f : 0.0f);
}

AudioManager::~AudioManager()
{
	stopAudio();

	for (int i = 0; i < MAX_EFFECT_SAMPLES; i++)
	{
		al_detach_sample_instance(effectsSamplePool[i]);
	}
	for (int i = 0; i < MAX_UI_SAMPLES; i++)
	{
		al_detach_sample_instance(effectsSamplePool[i]);
	}

	al_detach_sample_instance(normalInstance);
	al_detach_sample_instance(combatInstance);
	al_detach_sample_instance(dragonInstance);

	al_destroy_mixer(musicMixer);
	al_destroy_mixer(uiMixer);
	al_destroy_mixer(effectsMixer);
	al_destroy_mixer(globalMixer);

	al_destroy_voice(voice);

	//dtor
}
