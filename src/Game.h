/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2012-2023 Josh "Cheeseness" Bush
Copyright 2013 ptitSeb

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GAME_H
#define GAME_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <vector>
#include "ActionManager.h"
#include "GraphicsManager.h"
#include "AudioManager.h"
#include "InputManager.h"
#include "SettingsManager.h"
#include "utilities.h"
#include "DrawableLine.h"
#include "DrawableBitmap.h"
#include "DrawStack3D.h"
#include "DrawableText.h"
#include "Character.h"
#include "Player.h"
#include "Floor.h"
#include "DetailObject.h"
#include "Projectile.h"
#include "StaticObject.h"
#include "DragonHead.h"
#include "DragonSpine.h"
#include "Pickup.h"
#include "Splat.h"
#include "Rules.h"

#define RULES_EASY 0
#define RULES_NORMAL 1
#define RULES_CHALLENGING 2
#define RULES_FREE_SKATE 3
#define RULES_DRAGON 4
#define RULES_DEBUG 5
#define RULES_COUNT 6

#define DETAIL_OBJECT_CREATION_DISTANCE 390.0f
#define DETAIL_OBJECT_CREATION_DISTANCE_RANDOM 100.0f
#define DETAIL_OBJECT_NUMBER 800

#define STATIC_OBJECT_PERCENTAGE 25

#define SAFE_RADIUS 200.0f

#define HINT_NONE 0
#define HINT_HEAL 1
#define HINT_MOVE 2
#define HINT_PICKUP 3
#define HINT_SPEED 5
#define HINT_ENEMY 6
#define HINT_PICKUP2 7
#define HINT_DRAGON 8

#define BOSS_SPAWN_DISTANCE 450.0f
#define BOSS_SPAWN_CHANCE 3
#define MIN_BOSS_SPAWN_PICKUPS 3
#define MAX_BOSS_SPAWN_PICKUPS 4

#define ENEMY_SPAWN_DISTANCE 300.0f
#define ENEMY_SPAWN_DISTANCE_RANDOM 90.0f

#define PICKUP_TIME_STEP 1.0f

#ifndef VERSION_NUMBER
	#define VERSION_NUMBER "UNKNOWN VERSION"
#endif
#define WEBSITE_URL "http://flat.jbushproductions.com"

#ifndef PI
	#define PI 3.14159265
#endif

class Game
{
	public:
		Game();
		virtual ~Game();

		static Game* getGame();

		int initialize(int argc, char *argv[]);
		int runMainLoop();

		void doPause(bool state);
		void newGame();
		void endGame();
		void showScreen(GUIScreen* s);
		Player* getPlayer();
		void setQuit(bool q);
		void setUpdateTimer(unsigned int f);
		void setFPSVisibility(bool v);
		void setHintVisibility(bool v);
		void processHintState();

		GraphicsManager* getGraphicsMan();
		AudioManager* getAudioMan();
		SettingsManager* getSettingsMan();
		InputManager* getInputMan();

		void setRules(int d);
		void setupRules();

		void setupScreenMenu();
		void setupScreenNewGame();
		void setupScreenHowToPlay();
		void setupScreenStats();
		void setupScreenSettings();
		void setupScreenBindings(int mode);
		void setupScreenPause();
		void setupScreenDeath();
		void setupScreenEnd();
		void setupScreenCredits();

		void setupScreens();

		void registerEventSource(ALLEGRO_EVENT_SOURCE *source);
		void unregisterEventSource(ALLEGRO_EVENT_SOURCE *source);

		void openURI(std::string);

		GUIScreen* currentScreen;
		GUIScreen* screenMenu;
		GUIScreen* screenNew;
		GUIScreen* screenHowToPlay;
		GUIScreen* screenStats;
		GUIScreen* screenSettings;
		GUIScreen* screenBindings;
		GUIScreen* screenPause;
		GUIScreen* screenDeath;
		GUIScreen* screenEnd;
		GUIScreen* screenCredits;

		bool quit;
		bool paused;
		bool unpausing;
		bool gameEnded;

		Floor* floor;
		ALLEGRO_COLOR uiColour;
		ALLEGRO_COLOR uiColourBad;
		ALLEGRO_COLOR uiColourActive;


	private:

		static Game *instance;

		int fps;

		int frame;

		bool safe;

		int currentRules;
		Rules* rules[RULES_COUNT];

		ALLEGRO_EVENT_QUEUE *event_queue;
		ALLEGRO_TIMER *updateTimer;
		ALLEGRO_TIMER *fpsCounter;
		ALLEGRO_TIMER *hintCounter;
		ALLEGRO_FONT* hintFont;
		ALLEGRO_FONT* buttonFont;
		ALLEGRO_FONT* settingFont;
		ALLEGRO_FONT* bindingFont;
		ALLEGRO_FONT* titleFont;
		ALLEGRO_FONT* titleSmallFont;
		ALLEGRO_FONT* headingFont;
		ALLEGRO_FONT* textFont;

		DrawableBitmap projectileBitmapStarburst;
		DrawableBitmap projectileBitmapHalo;
		DrawableBitmap projectileBitmapStarburst2;
		DrawableBitmap projectileBitmapHalo2;
		DrawableBitmap detailBitmap[18];

		DrawableBitmap crystalA[16];
		DrawableBitmap crystalB[16];
		DrawableBitmap crystalC[16];
		DrawableBitmap grunt[16][7];
		vector <DrawableBitmap> splatBitmap;
		vector <DrawableBitmap> sparkBitmap;

		DragonHead* dragonHead;

		vector <Character*> npc;
		vector <Projectile*> projectile;
		vector <DetailObject*> detail;
		vector <DetailObject*> stat;
		vector <Pickup*> pickup;
		vector <Splat*> splat;

		GraphicsManager* graphicsMan;
		AudioManager* audioMan;
		SettingsManager* settingsMan;
		InputManager* inputMan;

		DrawStack3D* world3D;

		DrawableText* fpsText;
		DrawableText* hintText;
		int hintState;

		Player* player;
		GUIText* weaponStatShots[3];
		GUIText* weaponStatHits[3];
		GUIImage* weaponStatImages[3];
		GUIImage* enemyStatImages[3];
		GUIText* enemyStatKills[3];
		GUIText* timeStat;

		float lastEventTime;

		void gameTick();
		void draw();

		bool testcase;

		float pickupTimer;

		void loadBitmaps();

		void doStuff();

		vec2 lastPosition;

		bool bossSpawned;

		Character* spawnCharacter(vec3 position, float variance);
		DragonHead* spawnDragon(vec3 position, float variance);
		Splat* spawnSplat(vector<DrawableBitmap> frameList, vec3 pos);

		float pickups;
		int pickupsSinceLastWeapon;

		unsigned int enemySpawnCount;

		FrameSuperContainer generateCharacterSuperContainer();
};

#endif // GAME_H
