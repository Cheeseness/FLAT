/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GraphicsManager.h"

GraphicsManager::GraphicsManager() {
	Alert("Initialising graphics manager without a specified resolution (this should never happen). Defaulting to 1280x720.");
	GraphicsManager(1280, 720);
}

GraphicsManager::GraphicsManager(int _resx, int _resy)
{
	initialize(_resx, _resy);
}

GraphicsManager::~GraphicsManager()
{
	Print("Shutting down Allegro...");
	al_uninstall_keyboard();
	al_uninstall_mouse();
	al_shutdown_primitives_addon();
	al_destroy_display(display);
	al_uninstall_system();
	Print("GraphicsManager was cleaned up.");
}

void GraphicsManager::setFullscreen(bool value)
{
	al_set_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, value);
}

void GraphicsManager::pushDrawableObject(DrawableObject* o)
{
	stack.push(o);
}

void GraphicsManager::removeDrawableObject(DrawableObject* o)
{
	stack.remove(o);
}

//Draw from the manager's stack, which should contain all objects to be drawn.
void GraphicsManager::draw() {
	al_clear_to_color(clearColorGame);

	stack.draw(aspectMultiplier);

	al_flip_display();
}


void GraphicsManager::drawScreen(GUIScreen* screen)
{
	if (screen->isDirty())
	{
		al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);

		al_clear_to_color(clearColorMenu);
		screen->draw(minAspectMultiplier);
		al_flip_display();

		al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
	}
}

//Initialize allegro and the display.
int GraphicsManager::initialize(int resx, int resy) {

	resolution_x = resx;
	resolution_y = resy;

	updateAspectMultiplier(false);

	display = NULL;

	Print("Initialising Allegro primitives addon...");
	if(!al_init_primitives_addon()) {
		Print("Failed to initialize primitives addon!");
		return 0;
	}

	Print("Initialising Allegro image addon...");
	if(!al_init_image_addon()) {
		Print("Failed to initialize image addon!");
		return 0;
	}

	Print("Initialising Allegro font addon...");
	al_init_font_addon();

	Print("Initialising Allegro TTF addon...");
	if(!al_init_ttf_addon()) {
		Print("Failed to initialize ttf addon!");
		return 0;
	}

	Print("Initialising Allegro audio addon...");
	if(!al_install_audio()) {
		Print("Failed to initialize audio addon!");
		return 0;
	}


	Print("Initialising Allegro audio codecs addon...");
	if(!al_init_acodec_addon()) {
		Print("Failed to initialize audio codec addon!");
		return 0;
	}


	Print("Reserving audio samples addon...");
	if(!al_reserve_samples(16)){
		Print("Failed to reserve samples!");
		return -1;
	}


	Print("Initialising mouse...");
	if(!al_install_mouse()) {
		Print("Failed to initialize mouse");
		return 0;
	}


	Print("Initialising keyboard...");
	if(!al_install_keyboard()) {
		Print("Failed to initialize keyboard");
		return 0;
	}

	Print("Allegro initialized!");


	//Initialize the window
	display = al_create_display(resolution_x, resolution_y);
	if(!display) {
		Print("Failed to create display!", vec2(resolution_x, resolution_y));
		return -1;
	}

	Print("Display created successfully.");

	clearColorGame = al_map_rgb(100,100,200);
	clearColorMenu = al_map_rgb(0, 0, 0);

	windowIcons[0] = al_load_bitmap("gfx/icon_16.png");
	windowIcons[1] = al_load_bitmap("gfx/icon_32.png");
	windowIcons[2] = al_load_bitmap("gfx/icon_48.png");
	windowIcons[3] = al_load_bitmap("gfx/icon_64.png");
	windowIcons[4] = al_load_bitmap("gfx/icon_128.png");
	windowIcons[5] = al_load_bitmap("gfx/icon_256.png");
	windowIcons[6] = al_load_bitmap("gfx/icon_512.png");

	al_set_display_icons(display, 7, windowIcons);

	return 1;
}

ALLEGRO_DISPLAY* GraphicsManager::getDisplay() {
	return display;
}

vec2 GraphicsManager::getAspectMultiplier()
{
	return aspectMultiplier;
}

vec2 GraphicsManager::getMinAspectMultiplier()
{
	return minAspectMultiplier;
}

vec2 GraphicsManager::getMaxAspectMultiplier()
{
	return maxAspectMultiplier;
}

void GraphicsManager::updateAspectMultiplier(bool allowStretch)
{
	aspectMultiplier.x = resolution_x == NATIVE_RES_X ? 1 : float(resolution_x) / NATIVE_RES_X;
	aspectMultiplier.y = resolution_y == NATIVE_RES_Y ? 1 : float(resolution_y) / NATIVE_RES_Y;

	minAspectMultiplier.x = min(aspectMultiplier.x, aspectMultiplier.y);
	minAspectMultiplier.y = minAspectMultiplier.x;
	maxAspectMultiplier.x = max(aspectMultiplier.x, aspectMultiplier.y);
	maxAspectMultiplier.y = maxAspectMultiplier.x;

	if (!allowStretch)
	{
		aspectMultiplier = maxAspectMultiplier;
	}
	Print("Using resolution ", vec2(resolution_x, resolution_y));
	Print("Using aspect multiplier", aspectMultiplier);
}

void GraphicsManager::targetBackbuffer() {
	al_set_target_bitmap(al_get_backbuffer(display));
}
