/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUISCROLLER_H
#define GUISCROLLER_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "GUIWidget.h"

class GUIScroller : public GUIWidget
{
	public:
		GUIScroller();
		GUIScroller(ALLEGRO_COLOR _color);
		virtual ~GUIScroller();
		void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void addWidget(GUIWidget* w);
		GUIWidget* getFocusedWidget();
		int getFocusedWidgetIndex();
		void addFocusableWidget(GUIWidget* w);
		void clearFocusedWidget();
		void setFocusedWidget(int i);
		void setFocusedWidget(GUIWidget* w);
		bool focusNextWidget(int direction);
		void setDefaultFocusWidget(int i);
		void setDefaultFocusWidget(GUIWidget* w);
		bool checkCoords(vec2 coords, bool takeFocus, bool doClick);
		void processMouse(vec2 pos, bool click);
		void doAction();
		void doFocusedAction();
		void checkClickExtra(vec2 coords);
		void scrollToWidget(GUIWidget *w);
		void scrollToRegion(vec2 startCoords, vec2 endCoords);
		void scrollToPoint(vec2 coords);

	protected:
		ALLEGRO_COLOR colour;
		float scrollPos;
		int internalHeight;
		std::vector <GUIWidget*> widgets;
		std::vector <GUIWidget*> focusableWidgets;
		int focusedWidget;
		int defaultFocusWidget;
		int clickedWidget;
		bool closing;
		int handleSize;
		int handleGrowth;
	private:
};

#endif //GUISCROLLER_H
