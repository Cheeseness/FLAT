/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "utilities.h"

using namespace std;

void Print(string output) {
	cout<<output<<"\n";
}

void Print(string output, string s) {
	cout<<output<<" "<<s<<"\n";
}

void Print(string output, float n) {
	cout<<output<<" "<<n<<"\n";
}

void Print(string output, vec2 v) {
	cout<<output<<" ("<<v.x<<", "<<v.y<<")"<<"\n";
}

void Print(string output, void* v) {
	cout<<output<<" "<<v<<"\n";
}

void Alert(const char* msg) {
	//TODO: Implement alert prompts
	cerr<<msg<<"\n";
}

void Error(const char* msg) {
	//TODO: Implement error prompts
	cerr<<msg<<"\n";
}

string toUpper(string s)
{
	string u(s);
	for (unsigned int i = 0; i < u.size(); i++)
	{
		u.at(i) = toupper(u.at(i));
	}
	return u;
}

string removeSpaces(string s)
{
	string output;
	for (unsigned int i = 0; i < s.length(); i++)
	{
		if (s[i] != ' ')
		{
			output.push_back(s[i]);
		}
		else
		{
			output.push_back('_');
		}
	}
	return output;
}

float random(float scale) {
	return (((float)(rand()%20000-10000))/10000.0f)*scale;
}

bool chance(float chance) {
	return rand()%30000<chance*30000.0f;
}

float mod(float a, float b) {
	int n = (int)(a/b);
	return a-b*n;
}
