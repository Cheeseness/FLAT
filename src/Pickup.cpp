/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2012-2022 Josh "Cheeseness" Bush
Copyright 2013 ptitSeb

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Pickup.h"
#include "Game.h"

Pickup::Pickup()
{
	time = 0.0f;
	healthKitCount = 3;
	hasNewWeapon = true;
}

Pickup::Pickup(DrawStack3D* _world, vec3 _position) {

	DrawableBitmap b[8];

	b[0] = DrawableBitmap("gfx/capsule04.png");
	b[1] = DrawableBitmap("gfx/capsule05.png");
	b[2] = DrawableBitmap("gfx/capsule06.png");
	b[3] = DrawableBitmap("gfx/capsule07.png");
	b[4] = DrawableBitmap("gfx/capsule00.png");
	b[5] = DrawableBitmap("gfx/capsule01.png");
	b[6] = DrawableBitmap("gfx/capsule02.png");
	b[7] = DrawableBitmap("gfx/capsule03.png");

	skyBeam = al_load_bitmap("gfx/skyBeam.png");

	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 8; j++) {
			b[j].setReflection(true);
			pushImage(b[j]);
		}
	}

	_position.z = -1.0f;
	setPosition(_position);

	world = _world;
	world->push(this);

	entityType = 1;
	healthKitCount = 3;
	hasNewWeapon = true;

	setPrescale(5.0f);
}

Pickup::~Pickup()
{
	//dtor
	world->remove(this);
}

void Pickup::setHumSample(ALLEGRO_SAMPLE* sample)
{
	//Start playing the hum silently. We'll increase its volume as we get closer.
	humSample = sample;
	Game::getGame()->getAudioMan()->playEffectSound(humSample, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
}

void Pickup::draw(vec2 aspectMultiplier) {
	time += TIME_STEP;

	if(!visible) {
		return;
	}
	frames.draw(aspectMultiplier);

	float dAngle = rotation;

	while(dAngle > PI) {
		dAngle -= 2*PI;
	}

	while(dAngle < -PI) {
		dAngle += 2*PI;
	}


	float dx = (dAngle*900.0f*(0.9f+0.1f*abs(sin(dAngle))));

	//TODO: Someone cleverer than me is going to need to clear up the perceived vertical bounce - Cheese
	al_draw_tinted_scaled_rotated_bitmap(skyBeam, al_map_rgba(255, 255, 255, 255), al_get_bitmap_width(skyBeam)/2, 600+(cscale*5000), cposition.x * aspectMultiplier.x, (360.0f + sin(rotation)*dx) * aspectMultiplier.y, 50.0f*cscale * aspectMultiplier.x, 1.0f * aspectMultiplier.y, rotation*0.95f, 0);
}

void Pickup::setImageParameters(vec2 _position, float _scale, float _rotation) {
	update();

	int len = frames.getNumber();
	float angle = playerAngle;
	rotation = _rotation;

	//Clamp angle between 0 and 2*PI
	angle += PI;

	frames.setIndex((int)(angle/(2*PI)*((float)len)));

	frames.setImageParameters(_position, _scale*scale, _rotation);
	cposition = _position;
	cscale = _scale;
}

void Pickup::remove() {

	destroy = true;
}
