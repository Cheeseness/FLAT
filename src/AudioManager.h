/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2012-2022 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "utilities.h"


#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/mouse.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#define GAME_STATE_NONE -1
#define GAME_STATE_NORMAL 0
#define GAME_STATE_COMBAT 1
#define GAME_STATE_DRAGON 2

#define MUSIC_DUCK_AMOUNT 0.3f

/*ENEMY SOUNDS*/
#define SOUND_ENEMY_FIRE 0
#define SOUND_ENEMY_SKATE 1
#define SOUND_ENEMY_PAIN 2
#define SOUND_ENEMY_DEATH 3
#define SOUND_DRAGON_ROAR1 4
#define SOUND_DRAGON_ROAR2 5
#define SOUND_DRAGON_PAIN 6
#define SOUND_DRAGON_DEATH 7
#define SOUND_DRAGON_AMBIENCE 8
#define SOUND_PICKUP_HUM 9

/*PLAYER SOUNDS*/
#define SOUND_PISTOL_FIRE 10
#define SOUND_SHOTGUN_FIRE 11
#define SOUND_SMG_FIRE 12


/*WORLD SOUNDS*/

/*UI SOUNDS*/
#define SOUND_UI_FOCUS 13
#define SOUND_UI_HOVER 14
#define SOUND_UI_HOVER_SHORT 15
#define SOUND_UI_SELECT 16

#define MAX_SOUND_COUNT 17


#define MAX_UI_SAMPLES 3
#define MAX_UI_UNINTERRUPTABLE_SAMPLES 1
#define MAX_EFFECT_SAMPLES 10
#define MAX_EFFECT_UNINTERRUPTABLE_SAMPLES 1

class AudioManager
{
	public:
		AudioManager();
		virtual ~AudioManager();
		int getGameState();
		void duckMusicVolume(bool duck);
		void setGameState(int newState, bool force);
		void setGameState(int newState);
		void updateStateAudio(int newState);
		void setMusicVolume(float volume);
		void setEffectsVolume(float volume, bool silent = false);
		void setUIVolume(float volume, bool silent = false);
		void setGlobalVolume(float volume);
		void stopAudio();
		void setAudioCue(ALLEGRO_SAMPLE_INSTANCE* newCue);
		void playEffectSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop);
		void playUISound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop);
		void playEffectSound(int sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop);
		void playUISound(int sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop);
		void playSound(ALLEGRO_SAMPLE_INSTANCE* sample, ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop);
		bool playEffectUnintSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id);
		bool playUIUnintSound(ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id);
		bool playUnintSound(ALLEGRO_SAMPLE_INSTANCE* pool[], int poolSize, ALLEGRO_SAMPLE* sound, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, int* id);
		bool stopEffectUnintSound(ALLEGRO_SAMPLE* sound, int id);
		bool stopUIUnintSound(ALLEGRO_SAMPLE* sound, int id);
		bool stopUnintSound(ALLEGRO_SAMPLE_INSTANCE* pool[], int poolSize, ALLEGRO_SAMPLE* sound, int* id);
		ALLEGRO_SAMPLE *sounds[MAX_SOUND_COUNT];

	protected:
	private:
		int gameState;
		ALLEGRO_SAMPLE* normalMusic;
		ALLEGRO_SAMPLE* combatMusic;
		ALLEGRO_SAMPLE* dragonMusic;
		ALLEGRO_SAMPLE_INSTANCE* normalInstance;
		ALLEGRO_SAMPLE_INSTANCE* combatInstance;
		ALLEGRO_SAMPLE_INSTANCE* dragonInstance;
		ALLEGRO_MIXER* globalMixer;
		ALLEGRO_MIXER* musicMixer;
		ALLEGRO_MIXER* uiMixer;
		ALLEGRO_MIXER* effectsMixer;
		ALLEGRO_VOICE* voice;


		ALLEGRO_SAMPLE_INSTANCE* uiSamplePool[MAX_UI_SAMPLES];
		ALLEGRO_SAMPLE_INSTANCE* effectsSamplePool[MAX_EFFECT_SAMPLES];
		ALLEGRO_SAMPLE_INSTANCE* uiUnintPool[MAX_UI_UNINTERRUPTABLE_SAMPLES];
		ALLEGRO_SAMPLE_INSTANCE* effectsUnintPool[MAX_EFFECT_UNINTERRUPTABLE_SAMPLES];

		int uiSampleID;
		int effectsSampleID;

		bool first;
		bool isDucked;
		float musicVolume;
		float effectsVolume;
		float uiVolume;
		float globalVolume;
//		ALLEGRO_MIXER musicMixer;
};

#endif // AUDIOMANAGER_H
