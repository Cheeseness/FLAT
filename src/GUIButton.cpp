/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIButton.h"

GUIButton::GUIButton()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = NULL;
	color = al_map_rgb(255, 255, 255);
	text = (char*)"";
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	visible = true;
	dirty = true;
	updateSizePlus();
}

GUIButton::GUIButton(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, const char* _text, ACTION a)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = _font;
	color = _color;
	setText(_text);
	hasFocus = false;
	action = a;
	leftAlign = true;
	visible = true;
	dirty = true;
}

GUIButton::~GUIButton()
{

}

void GUIButton::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible || !dirty)
	{
		return;
	}
	if (hasFocus || isMouseOver)
	{
		if (leftAlign)
		{
			al_draw_filled_rectangle(positionOffset.x + positionPlus.x, positionOffset.y + positionPlus.y + padding.y, positionOffset.x + positionPlus.x + al_get_font_ascent(font), positionOffset.y + positionPlus.y + al_get_font_ascent(font) + padding.y, color);
			al_draw_text(font, color, positionPlus.x + padding.x + al_get_font_ascent(font) + padding.y * 2, positionPlus.y + padding.y, 0, text);
		}
		else
		{
			al_draw_filled_rectangle(positionOffset.x + positionPlus.x + sizePlus.x + padding.x - al_get_font_ascent(font), positionOffset.y + positionPlus.y + padding.y, positionOffset.x + positionPlus.x + sizePlus.x + padding.x, positionOffset.y + positionPlus.y + al_get_font_ascent(font) + padding.y, color);
			al_draw_text(font, color, positionOffset.x + positionPlus.x + padding.x - al_get_font_ascent(font) - padding.y * 2 + size.x, positionOffset.y + positionPlus.y + padding.y, ALLEGRO_ALIGN_RIGHT, text);
		}
	}
	else
	{
		if (leftAlign)
		{
			al_draw_text(font, color, positionOffset.x + positionPlus.x + padding.x, positionOffset.y + positionPlus.y + padding.y, 0, text);
		}
		else
		{
			al_draw_text(font, color, positionOffset.x + positionPlus.x + padding.x + size.x, positionOffset.y + positionPlus.y + padding.y, ALLEGRO_ALIGN_RIGHT, text);
		}
	}
}

void GUIButton::setText(const char* _text)
{
	text = (char*)_text;
	size = vec2(al_get_text_width(font, text), al_get_font_line_height(font));
	updateSizePlus();
	dirty = true;
}

void GUIButton::setFont(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color)
{
	color = _color;
	dirty = true;
}

void GUIButton::printDetails()
{
	Print(text, this);
	Print("  start", getPosition());
	Print("  end  ", getPosition() + getSize());
}
