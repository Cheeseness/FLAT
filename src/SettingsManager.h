/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#define SAVE_FILE "flat.rc"
#define BINDING_FILE "controls.rc"

#ifdef _WIN32
	#define SETTINGS_PATH_FALLBACK ".\\"
#else
	#define SETTINGS_PATH_FALLBACK "./"
#endif


//TODO: Move this out to a config file
#define DEFAULT_MUSIC_VOLUME 0.7f
#define DEFAULT_EFFECTS_VOLUME 0.7f
#define DEFAULT_UI_VOLUME 0.7f
#define DEFAULT_GLOBAL_VOLUME 1.0f
#define DEFAULT_AUTO_HEAL false
#define DEFAULT_TURN_SENSITIVITY 0.004f
#define DEFAULT_ENABLE_JOYSTICK true
#define DEFAULT_AXIS_DEADZONE 0.05
#define DEFAULT_SHOW_HINTS true
#define DEFAULT_TARGET_FPS 60


#ifdef PANDORA
	#define DEFAULT_RES_X 800
	#define DEFAULT_RES_Y 480
#else
	#define DEFAULT_RES_X 1280
	#define DEFAULT_RES_Y 720
#endif

class SettingsManager
{
	public:
		SettingsManager();
		virtual ~SettingsManager();

		void loadSettingsDefaults(bool save);
		bool loadSettings();
		bool saveSettings();
		void applyAllSettings();

		bool loadBindings();
		bool loadBindings_v1_2();
		bool backupBindings();
		bool saveBindings();

		void setResX(unsigned int i);
		unsigned int getResX();
		void setResY(unsigned int i);
		unsigned int getResY();

		void setMusicVolume(float v);
		float getMusicVolume();
		void applyMusicVolume();

		void setEffectsVolume(float v);
		float getEffectsVolume();
		void applyEffectsVolume(bool silent = false);

		void setUIVolume(float v);
		float getUIVolume();
		void applyUIVolume(bool silent = false);

		void setGlobalVolume(float v);
		float getGlobalVolume();
		void applyGlobalVolume();

		void setAutoHeal(bool b);
		bool getAutoHeal();
		void applyAutoHeal();

		void setTurnSensitivity(float v);
		float getTurnSensitivity();
		void applyTurnSensitivity();

		void setEnableJoystick(bool b);
		bool getEnableJoystick();
		void applyEnableJoystick();

		void setJoystickIndex(unsigned int i);
		unsigned int getJoystickIndex();
		void applyJoystickIndex();

		void setAxisDeadzone(float v);
		float getAxisDeadzone();
		void applyAxisDeadzone();

		void setFullscreen(bool b);
		bool getFullscreen();
		void applyFullscreen();

		void setShowFPS(bool b);
		bool getShowFPS();
		void applyShowFPS();

		void setFPS(unsigned int f);
		unsigned int getFPS();
		void applyFPS();

		void setShowHints(bool b);
		bool getShowHints();
		void applyShowHints();

		std::string settingsPath;

	private:
		bool readSetting(std::string setting, std::stringstream* s, std::string line, bool* v);
		bool readSetting(std::string setting, std::stringstream* s, std::string line, int* v);
		bool readSetting(std::string setting, std::stringstream* s, std::string line, unsigned int* v);
		bool readSetting(std::string setting, std::stringstream* s, std::string line, float* v);

		bool bindingsDirty;
		bool settingsDirty;

		//Graphics
		unsigned int resX;
		unsigned int resY;
		unsigned int fullscreenX;
		unsigned int fullscreenY;
		unsigned int targetFPS;
		bool fullscreen;

		//Audio
		float musicVolume;
		float effectsVolume;
		float uiVolume;
		float globalVolume;

		//Gameplay
		int difficulty;
		bool autoHeal;
		bool showHints;

		//Input
		float turnSensitivity;
		bool enableJoystick;
		unsigned int joystickIndex;
		float axisDeadzone;

		//Debug
		bool showFPS;

};

#endif //SETTINGSMANAGER_H
