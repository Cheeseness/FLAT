/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIText.h"

GUIText::GUIText()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = NULL;
	color = al_map_rgb(255, 255, 255);
	text = (char*)"";
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	visible = true;
	dirty = true;
}

GUIText::GUIText(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, std::string _text)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = _font;
	color = _color;
	setText(_text);
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	visible = true;
	dirty = true;
	updateSizePlus();
}

GUIText::~GUIText()
{

}

void GUIText::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible || !dirty)
	{
		return;
	}
	al_draw_text(font, color, positionOffset.x + positionPlus.x + padding.x, positionOffset.y + positionPlus.y + padding.y, 0, text.c_str());
}

void GUIText::setText(std::string _text)
{
	text = _text;
	size = vec2(al_get_text_width(font, text.c_str()), al_get_font_line_height(font));
	updateSizePlus();
	dirty = true;
}

void GUIText::setFont(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color)
{
	color = _color;
	dirty = true;
}
