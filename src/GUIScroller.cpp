/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

//FIXME: This is duplicated from GUIScreen.h
#define FOCUS_DIRECTION_LEFT 0
#define FOCUS_DIRECTION_RIGHT 1
#define FOCUS_DIRECTION_UP 2
#define FOCUS_DIRECTION_DOWN 3

#include "GUIScroller.h"

GUIScroller::GUIScroller()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	colour = al_map_rgb(255, 255, 255);
	visible = true;
	dirty = true;
	scrollPos = 0;
	internalHeight = 0;
	focusedWidget = -1;
	defaultFocusWidget = -1;
	clickedWidget = - 1;
	handleSize = 20;
	handleGrowth = 5;
	closing = false;
}

GUIScroller::GUIScroller(ALLEGRO_COLOR _colour)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	colour = _colour;
	visible = true;
	dirty = true;

	scrollPos = 0;
	internalHeight = 0;
	focusedWidget = -1;
	defaultFocusWidget = -1;
	clickedWidget = - 1;
	closing = false;
	handleSize = 20;
	handleGrowth = 5;
	updateSizePlus();
}

GUIScroller::~GUIScroller()
{
	closing = true;
	for (unsigned int i = 0; i < widgets.size(); i++)
	{
		delete widgets[i];
	}
	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{
		delete focusableWidgets[i];
	}
	widgets.clear();
	focusableWidgets.clear();
}

void GUIScroller::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible)// || !dirty)
	{
		return;
	}

	al_draw_rectangle(positionOffset.x + positionPlus.x, positionOffset.y + positionPlus.y, positionOffset.x + positionPlus.x + sizePlus.x, positionOffset.y + positionPlus.y + sizePlus.y , colour, 2);

	int scrollOffset = int(floor((internalHeight - size.y) * scrollPos));
	if (scrollOffset < 0)
	{
		scrollOffset = 0;
	}


	int handleX = positionOffset.x + positionPlus.x + size.x - (int)(handleSize * 0.5);
	int handleXEnd = handleX + handleSize;
	int handleY = floor(sizePlus.y * scrollPos) + positionOffset.y + position.y - (int)(handleSize * 0.5);
	int handleYEnd = handleY + handleSize;

	if (!isMouseOver)
	{
		handleX += handleGrowth;
		handleY += handleGrowth;
		handleXEnd -= handleGrowth;
		handleYEnd -= handleGrowth;
	}

	al_draw_filled_rectangle(handleX, handleY, handleXEnd, handleYEnd, colour);



	int clipX = 0;
	int clipY = 0;
	int clipW = 0;
	int clipH = 0;
	al_get_clipping_rectangle(&clipX, &clipY, &clipW, &clipH);
	al_set_clipping_rectangle(positionOffset.x + positionPlus.x, positionOffset.y + positionPlus.y, sizePlus.x, sizePlus.y);

	positionOffset.y -= scrollOffset;

	for (unsigned int i = 0; i < widgets.size(); i++)
	{
		widgets[i]->draw(aspectMultiplier, positionOffset  + positionPlus);
	}
	for (unsigned int i = 0; i < focusableWidgets.size(); i++)
	{
		focusableWidgets[i]->draw(aspectMultiplier, positionOffset + positionPlus);
	}
	al_set_clipping_rectangle(clipX, clipY, clipW, clipH);

	dirty = false;
}

void GUIScroller::addWidget(GUIWidget* w)
{
	widgets.push_back(w);
	if (focusableWidgets.size() == 1)
	{
		defaultFocusWidget = 0;
	}
	internalHeight = int(max(float(internalHeight), w->getPositionPlus().y + w->getSizePlus().y));
	dirty = true;
}

void GUIScroller::addFocusableWidget(GUIWidget* w)
{
	focusableWidgets.push_back(w);
	if (focusableWidgets.size() == 1)
	{
		defaultFocusWidget = 0;
	}
	dirty = true;
}

GUIWidget* GUIScroller::getFocusedWidget()
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		return focusableWidgets[focusedWidget]->getFocusedWidget();
	}
	return NULL;
}

void GUIScroller::setFocusedWidget(int i)
{
	if (i >=0 && i < (int)focusableWidgets.size())
	{
		clearFocusedWidget();
		if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
		{
			focusableWidgets[focusedWidget]->setFocusState(false);
		}
		focusedWidget = i;
		focusableWidgets[focusedWidget]->setFocusState(true);
		scrollToWidget(focusableWidgets[focusedWidget]);
	}
	dirty = true;
}

void GUIScroller::clearFocusedWidget()
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		focusableWidgets[focusedWidget]->setFocusState(false);
	}
	focusedWidget = -1;
}

bool GUIScroller::focusNextWidget(int d)
{
	//Print("Seeking within scroller");
	if (focusableWidgets.size() == 0)
	{
		Print("Seeking with no focusable widgets available");
		return false;
	}

	bool shouldWrap = false;
	if (focusedWidget == -1)
	{
		//Select widget closest to entering direction	
		Print("Seeking with no focused widget");
		focusedWidget = 0;
		shouldWrap = true;
		//dirty = true;
		//return true;
	}
	//else
	{
		//TODO: This is duplicated from GUIScreen - bust this out into some kind of helper, maybe?
		int widgetClose = focusedWidget;
		int widgetWrap = focusedWidget;
		int buffer = 200;

		//Loop through all focusable widgets, and track the closest next and the farthest first widget in the seek direction that align within a buffer distance of perpendicular to the seek direction

		bool forwards = (d == FOCUS_DIRECTION_LEFT || d == FOCUS_DIRECTION_UP) ? false : true;

		int wDir = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[focusedWidget]->getPosition().x : focusableWidgets[focusedWidget]->getPosition().y;
		int wPer = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[focusedWidget]->getPosition().y : focusableWidgets[focusedWidget]->getPosition().x;

		int closeDir = wDir;
		int closePer = wPer;

		int wrapDir = wDir;
		int wrapPer = wPer;

		int iPer;
		int iDir;

		for (unsigned int i = 0; i < focusableWidgets.size(); i++)
		{

			if (focusedWidget == (int) i)
			{
				continue;
			}

			iDir = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[i]->getPosition().x : focusableWidgets[i]->getPosition().y;
			iPer = (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT) ? focusableWidgets[i]->getPosition().y : focusableWidgets[i]->getPosition().x;

			if (iPer < wPer + buffer && iPer > wPer - buffer && iDir != wDir)
			{
				if ((iDir > wDir) == forwards)
				{
					if (iDir == closeDir)
					{
						if (abs(wPer - iPer) < abs(wPer - closePer))
						{
							closeDir = iDir;
							closePer = iPer;
							widgetClose = i;
						}
					}
					else if ((iDir < closeDir) == forwards || widgetClose == focusedWidget)
					{
						closeDir = iDir;
						closePer = iPer;
						widgetClose = i;
					}
				}
				else
				{
					if (shouldWrap)
					{
						if ((iDir < wrapDir) == forwards)
						{					
							wrapDir = iDir;
							widgetWrap = i;
						}
					}
					else
					{
						if (abs(iPer - wPer) < abs(wrapPer - wPer) || widgetWrap == focusedWidget)
						{
							Print("Setting wrap widget", i);
							wrapDir = iDir;
							wrapPer = iPer;
							widgetWrap = i;
						}
					}
				}
			}
		}

		if (shouldWrap)
		{
			setFocusedWidget(widgetWrap);
			dirty = true;
			return true;
		}
		else if (widgetClose != focusedWidget)
		{
			setFocusedWidget(widgetClose);
			dirty = true;
			return true;
		}
		else
		{
			if (d == FOCUS_DIRECTION_RIGHT || d == FOCUS_DIRECTION_LEFT)
			{
				setFocusedWidget(widgetWrap);
				dirty = true;
				return true;
			}
			setFocusedWidget(-1);
			Print("Can't seek anywhere?");
		}
		dirty = true;
	}
	Print("Failing to seek in scroller");
	return false;
}

bool GUIScroller::checkCoords(vec2 coords, bool takeFocus, bool doClick)
{
	//TODO: Calculte position offset from parent? Don't assume we're positioned relative to 0,0
	int handleX = positionPlus.x + size.x - (int)(handleSize * 0.5);
	int handleXEnd = handleX + handleSize;
	int handleY = floor(sizePlus.y * scrollPos) + position.y - (int)(handleSize * 0.5);
	int handleYEnd = handleY + handleSize;

	isMouseOver = false;
	if (doClick && coords.y >= position.y && coords.y <= position.y + sizePlus.y)
	{
		if (coords.x >= handleX - handleSize * 2)
		{
			checkClickExtra(coords);
		}
	}
	else if (coords.x >= handleX && coords.x <= handleXEnd)
	{
		if (coords.y >= handleY && coords.y <= handleYEnd)
		{
			isMouseOver = true;
			dirty = true;
			return false;
		}
	}

	if (coords.x >= positionPlus.x && coords.x <= positionPlus.x + sizePlus.x)
	{
		if (coords.y >= positionPlus.y && coords.y <= positionPlus.y + sizePlus.y)
		{
			processMouse(coords - positionPlus, doClick);
			return focusedWidget != -1;
		}
	}
	processMouse(vec2(-1, -1), false);
	return false;
}

void GUIScroller::processMouse(vec2 pos, bool click)
{
	if (closing)
	{
		return;
	}

	if (!click)
	{
		clickedWidget = -1;
	}

	int scrollOffset = int(floor((internalHeight - size.y) * scrollPos));
	if (scrollOffset < 0)
	{
		scrollOffset = 0;
	}
	pos.y += scrollOffset;


	int oldFocusedWidget = focusedWidget;

	if (clickedWidget >= 0)
	{
		vec2 tempMin = focusableWidgets[clickedWidget]->getPositionPlus();
		vec2 tempMax = tempMin + focusableWidgets[clickedWidget]->getSize();
		pos.x = max(pos.x, tempMin.x);
		pos.x = min(pos.x, tempMax.x);
		pos.y = max(pos.y, tempMin.y);
		pos.y = min(pos.y, tempMax.y);

		focusableWidgets[clickedWidget]->checkCoords(pos, true, true);
		dirty = true;
	}
	else
	{
		for (unsigned int i = 0; i < focusableWidgets.size(); i++)
		{
			if (focusableWidgets[i]->checkCoords(pos, click, click))
			{
				if (click)//clickReady)
				{
					setFocusedWidget(focusedWidget);
					focusableWidgets[i]->doAction();
/*					if (Game::getGame()->currentScreen != this)
					{
						return;
					}
*/					clickedWidget = i;
					focusedWidget = clickedWidget;
				}
			}
			//HACK: This is a quick and dirty way to prevent crashes when hitting the reset button
			if (closing || i > focusableWidgets.size())
			{
				Alert("Current GUIWidget does not exist. GUIScreen has likely been destroyed.");
				return;
			}
			if (focusableWidgets[i]->isDirty())
			{
				dirty = true;
			}
		}
	}

	if (focusedWidget != oldFocusedWidget)
	{
		if (oldFocusedWidget >=0 && oldFocusedWidget < (int)focusableWidgets.size())
		{
			focusableWidgets[oldFocusedWidget]->setFocusState(false);
		}
	}
}

void GUIScroller::checkClickExtra(vec2 coords)
{
	float oldValue = scrollPos;
	scrollPos = (coords.y - positionPlus.y) / sizePlus.y;
	if (scrollPos != oldValue)
	{
		isMouseOver = true;
		dirty = true;
	}
}

void GUIScroller::doAction()
{
	if (focusedWidget >=0 && focusedWidget < (int)focusableWidgets.size())
	{
		Print("Doing action for scroller child", focusableWidgets[focusedWidget]);
		return focusableWidgets[focusedWidget]->doAction();
	}
}

void GUIScroller::scrollToWidget(GUIWidget *w)
{
	scrollToRegion(w->getPosition(), w->getPositionPlus() + w->getSizePlus());
}

void GUIScroller::scrollToRegion(vec2 startCoords, vec2 endCoords)
{
	float scrollBegin = (internalHeight - sizePlus.y) * scrollPos;
	startCoords.y -= padding.y;
	endCoords.y -= sizePlus.y - padding.y;

	if (startCoords.y <= scrollBegin)
	{
		scrollToPoint(startCoords);
	}
	else if (endCoords.y > scrollBegin)
	{
		scrollToPoint(endCoords);
	}
}

void GUIScroller::scrollToPoint(vec2 coords)
{
	scrollPos = ((coords.y) / (internalHeight + padding.y * 2 - sizePlus.y));
	if (scrollPos < 0)
	{
		scrollPos = 0;
	}
	if (scrollPos > 1.0)
	{
		scrollPos = 1.0;
	}
	dirty = true;
}
