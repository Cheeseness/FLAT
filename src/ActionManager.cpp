/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ActionManager.h"
#include "Game.h"


ActionManager* ActionManager::a = NULL;

ActionManager* ActionManager::getActionManager()
{
	if (a == NULL)
	{
		a = new ActionManager();
	}
	return a;
}

ActionManager::ActionManager()
{

}

ActionManager::~ActionManager()
{

}

void ActionManager::doQuit()
{
	Print("Doing quit");
	Game::getGame()->setQuit(true);
}

void ActionManager::doNewGame()
{
	Print("Doing new game");
	Game::getGame()->showScreen(Game::getGame()->screenNew);
}

void ActionManager::doNewEasy()
{
	Print("Doing easy new game");
	Game::getGame()->setRules(RULES_EASY);
	Game::getGame()->newGame();
	Game::getGame()->doPause(false);
}

void ActionManager::doNewNormal()
{
	Print("Doing normal new game");
	Game::getGame()->setRules(RULES_NORMAL);
	Game::getGame()->newGame();
	Game::getGame()->doPause(false);
}

void ActionManager::doNewChallenging()
{
	Print("Doing challenging new game");
	Game::getGame()->setRules(RULES_CHALLENGING);
	Game::getGame()->newGame();
	Game::getGame()->doPause(false);
}

void ActionManager::doNewFreeSkate()
{
	Print("Doing free skate new game");
	Game::getGame()->setRules(RULES_FREE_SKATE);
	Game::getGame()->newGame();
	Game::getGame()->doPause(false);
}


void ActionManager::doNewDebug()
{
	Print("Doing debug new game");
	Game::getGame()->setRules(RULES_DEBUG);
	Game::getGame()->newGame();
	Game::getGame()->doPause(false);
}

void ActionManager::doEndGame()
{
	Print("Returning to menu");
	Game::getGame()->showScreen(Game::getGame()->screenMenu);
	Game::getGame()->endGame();
	Game::getGame()->doPause(true);
}

void ActionManager::doResume()
{
	Print("Doing resume");
	Game::getGame()->doPause(false);
}

void ActionManager::doWebsite()
{
	Print("Opening website:", WEBSITE_URL);
	Game::getGame()->openURI(WEBSITE_URL);
}

void ActionManager::doSettingsFolder()
{
	Print("Opening settings folder:", Game::getGame()->getSettingsMan()->settingsPath);
	Game::getGame()->openURI(Game::getGame()->getSettingsMan()->settingsPath);
}

void ActionManager::doHowToPlay()
{
	Print("Showing instructions");
	Game::getGame()->showScreen(Game::getGame()->screenHowToPlay);
}

void ActionManager::doStats()
{
	Print("Showing stats");
	Game::getGame()->showScreen(Game::getGame()->screenStats);
}

void ActionManager::doBindings()
{
	Print("Showing bindings");
	Game::getGame()->currentScreen->close();
	Game::getGame()->setupScreenBindings(INPUT_TYPE_KEY1);
	Game::getGame()->showScreen(Game::getGame()->screenBindings);
}

void ActionManager::doSettings()
{
	Print("Showing settings");
	Game::getGame()->showScreen(Game::getGame()->screenSettings);
}

void ActionManager::doCredits()
{
	Print("Showing credits");
	Game::getGame()->showScreen(Game::getGame()->screenCredits);
}

void ActionManager::doBack()
{
	Print("Returning");
	if (Game::getGame()->getPlayer() == NULL)
	{
		Game::getGame()->showScreen(Game::getGame()->screenMenu);
	}
	else
	{
		//TODO: This should probably be handled/managed elsewhere
		if (Game::getGame()->gameEnded && Game::getGame()->currentScreen == Game::getGame()->screenStats)
		{
			if (Game::getGame()->getPlayer()->gameOver)
			{
				Game::getGame()->showScreen(Game::getGame()->screenDeath);
			}
			else
			{
				Game::getGame()->showScreen(Game::getGame()->screenEnd);
			}
		}
		else
		{
			Game::getGame()->showScreen(Game::getGame()->screenPause);
		}
	}
}


void ActionManager::doKeyboardBindings()
{
	Print("Showing keyboard bindings!");
	Game::getGame()->currentScreen->close();
	Game::getGame()->setupScreenBindings(INPUT_TYPE_KEY1);
	Game::getGame()->showScreen(Game::getGame()->screenBindings);
}

void ActionManager::doGamepadBindings()
{
	Print("Showing gamepad bindings!");
	Game::getGame()->currentScreen->close();
	Game::getGame()->setupScreenBindings(INPUT_TYPE_GAMEPAD1);
	Game::getGame()->showScreen(Game::getGame()->screenBindings);
}


void ActionManager::doMouseBindings()
{
	Print("Showing mouse bindings!");
	Game::getGame()->currentScreen->close();
	Game::getGame()->setupScreenBindings(INPUT_TYPE_MOUSE1);
	Game::getGame()->showScreen(Game::getGame()->screenBindings);
}

void ActionManager::doResetBindings()
{
	Print("Resetting bindings!");
	Game::getGame()->currentScreen->close();
	Game::getGame()->getInputMan()->loadBindingsDefaults(true);
}

void ActionManager::doResetSettings()
{
	Print("Resetting settings!");
	Game::getGame()->currentScreen->close();
	Game::getGame()->getSettingsMan()->loadSettingsDefaults(true);
}


void ActionManager::doNothing()
{
	Print("Doing nothing!");
}

void ActionManager::updateFullscreen(bool b)
{
	Game::getGame()->getSettingsMan()->setFullscreen(b);

}

void ActionManager::updateAutoHeal(bool b)
{
	Game::getGame()->getSettingsMan()->setAutoHeal(b);
}

void ActionManager::updateEnableJoystick(bool b)
{
	Game::getGame()->getSettingsMan()->setEnableJoystick(b);
}

void ActionManager::updateAxisDeadzone(float v)
{
	Game::getGame()->getSettingsMan()->setAxisDeadzone(v);
}

void ActionManager::updateShowFPS(bool b)
{
	Game::getGame()->getSettingsMan()->setShowFPS(b);
}

void ActionManager::updateShowHints(bool b)
{
	Game::getGame()->getSettingsMan()->setShowHints(b);
}

void ActionManager::updateGlobalVolume(float v)
{
	Game::getGame()->getSettingsMan()->setGlobalVolume(v);
}

void ActionManager::updateMusicVolume(float v)
{
	Game::getGame()->getSettingsMan()->setMusicVolume(v);
}

void ActionManager::updateEffectsVolume(float v)
{
	Game::getGame()->getSettingsMan()->setEffectsVolume(v);
}


void ActionManager::updateUIVolume(float v)
{
	Game::getGame()->getSettingsMan()->setUIVolume(v);
}

void ActionManager::updateTurnSensitivity(float v)
{
	Game::getGame()->getSettingsMan()->setTurnSensitivity(v * 0.01f);
}
