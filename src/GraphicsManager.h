/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2021-2022 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GRAPHICSMANAGER_H
#define GRAPHICSMANAGER_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/mouse.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "utilities.h"
#include "DrawableObject.h"
#include "DrawStack.h"
#include "GUIScreen.h"

#define NATIVE_RES_X 1280
#define NATIVE_RES_Y 720

class GraphicsManager
{
	public:
		GraphicsManager();
		GraphicsManager(int _resx, int _resy);
		virtual ~GraphicsManager();

		void draw();
		void drawScreen(ALLEGRO_BITMAP* screen);
		void drawScreen(GUIScreen* screen);

		void pushDrawableObject(DrawableObject* o);
		void removeDrawableObject(DrawableObject* o);

		void targetBackbuffer();

		unsigned int resolution_x;
		unsigned int resolution_y;
		void setFullscreen(bool value);
		vec2 getAspectMultiplier();
		vec2 getMinAspectMultiplier();
		vec2 getMaxAspectMultiplier();
		void updateAspectMultiplier(bool allowStretch);

		ALLEGRO_DISPLAY* getDisplay();
	private:
		ALLEGRO_COLOR clearColorGame;
		ALLEGRO_COLOR clearColorMenu;
		ALLEGRO_DISPLAY *display;

		vec2 aspectMultiplier;
		vec2 minAspectMultiplier;
		vec2 maxAspectMultiplier;

		int initialize(int resx, int resy);


		ALLEGRO_BITMAP* windowIcons[7];

		DrawStack stack;
};

#endif // GRAPHICSMANAGER_H
