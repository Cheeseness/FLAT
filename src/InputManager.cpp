/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <fstream>
#include <iostream>

#include <allegro5/allegro.h>
#include "InputManager.h"
#include "Game.h"

InputAction::InputAction()
{
	key1 = -1;
	key2 = -1;

	gamepad1 = -1;
	gamepad1Stick = -1;
	gamepad1Invert = false;
	gamepad2 = -1;
	gamepad2Stick = -1;
	gamepad2Invert = false;

	mouse1 = -1;
	mouse1Axis = false;
	mouse1Invert = false;
	mouse2 = -1;
	mouse2Axis = false;
	mouse2Invert = false;

	name = "unknown";
	value = 0;
	mouseAxisValue = 0;
	axisInput = false;
	pressed = false;
	changed = false;
}

InputAction::InputAction(std::string n, int k1, int k2, int g1, int g1S, bool g1I, int g2, int g2S, bool g2I, int m1, bool m1A, bool m1I, int m2, bool m2A, bool m2I)
{
	key1 = k1;
	key2 = k2;

	gamepad1 = g1;
	gamepad1Stick = g1S;
	gamepad1Invert = g1I;

	gamepad2 = g2;
	gamepad2Stick = g2S;
	gamepad2Invert = g2I;

	mouse1 = m1;
	mouse1Axis = m1A;
	mouse1Invert = m1I;

	mouse2 = m2;
	mouse2Axis = m2A;
	mouse2Invert = m2I;

	name = n;
	value = 0;
	mouseAxisValue = 0;
	axisInput = false;
	pressed = false;
	changed = false;
}

void InputAction::clearStates()
{
	value = 0;
	axisInput = false;
	changed = false;
	pressed = false;
}


//TODO: There's a bunch of duplicaed code here in the process*Event() functions that could be refactored into separate functions that take pointers to button, axis, and invert
void InputAction::processKeyboardEvent(ALLEGRO_EVENT* ev)
{
	bool newState = false;
	if (ev->keyboard.keycode == key1 || ev->keyboard.keycode == key2)
	{
		newState = newState || ev->type == ALLEGRO_EVENT_KEY_DOWN;

		changed = changed || newState != pressed;
		pressed = newState;
	}
}

void InputAction::processMouseButtonEvent(ALLEGRO_EVENT* ev)
{
	bool newState = false;
	int button = (int)ev->mouse.button;
	if ((button == mouse1 && !mouse1Axis) || (button == mouse2 && !mouse2Axis))
	{
		newState = newState || ev->type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN;

		changed = changed || newState != pressed;
		pressed = newState;
	}
}

void InputAction::processMouseAxisEvent(ALLEGRO_EVENT* ev)
{
	//Mouse axis events are momentary/instantaneous, and so don't need to modifiy pressed - if we're not pressed though, we can set changed to true so that isJustReleased() returns true
	if (mouse1 >= 0 && mouse1Axis)
	{
		float movement = 0;
		switch (mouse1)
		{
			case MOUSE_X:
				movement = ev->mouse.x - MOUSE_WARP_POS;
				movement *= MOUSE_AXIS_SENSITIVITY;
				break;
			case MOUSE_Y:
				movement = ev->mouse.y - MOUSE_WARP_POS;
				movement *= MOUSE_AXIS_SENSITIVITY;
				break;
			case MOUSE_WHEEL_1:
				movement = ev->mouse.z;
				movement /= AXIS_MOVE_THRESHOLD;
				break;
			case MOUSE_WHEEL_2:
				movement = ev->mouse.w;
				movement /= AXIS_MOVE_THRESHOLD;
				break;
		}

		if (abs(movement) >= 0)
		{
			if (!mouse1Invert)
			{
				if (movement > 0)
				{
					if (!pressed)
					{
						changed = true;
					}
					//axisInput = true;
					mouseAxisValue += movement;
				}
			}
			else
			{
				if (movement < 0)
				{
					if (!pressed)
					{
						changed = true;
					}
					//axisInput = true;
					mouseAxisValue += -movement;
				}
			}
		}
	}
	if (mouse2 >= 0 && mouse2Axis)
	{
		float movement = 0;
		switch (mouse2)
		{
			case MOUSE_X:
				movement = ev->mouse.x - MOUSE_WARP_POS;
				movement *= MOUSE_AXIS_SENSITIVITY;
				break;
			case MOUSE_Y:
				movement = ev->mouse.y - MOUSE_WARP_POS;
				movement *= MOUSE_AXIS_SENSITIVITY;
				break;
			case MOUSE_WHEEL_1:
				movement = ev->mouse.z;
				movement /= AXIS_MOVE_THRESHOLD;
				break;
			case MOUSE_WHEEL_2:
				movement = ev->mouse.w;
				movement /= AXIS_MOVE_THRESHOLD;
				break;
		}

		if (abs(movement) >= 0)
		{
			if (!mouse2Invert)
			{
				if (movement > 0)
				{
					if (!pressed)
					{
						changed = true;
					}
					//axisInput = true;
					mouseAxisValue += movement;
				}
			}
			else
			{
				if (movement < 0)
				{
					if (!pressed)
					{
						changed = true;
					}
					//axisInput = true;
					mouseAxisValue += -movement;
				}
			}
		}
	}
}

void InputAction::processJoystickButtonEvent(ALLEGRO_EVENT* ev)
{
	if (ev->joystick.id == al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex()))
	{
		bool newState = false;
		if ((ev->joystick.button == gamepad1 && gamepad1Stick < 0) || (ev->joystick.button == gamepad2 && gamepad2Stick < 0))
		{
			newState = newState || ev->type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN;

			changed = changed || newState != pressed;
			pressed = newState;
		}
	}
}

void InputAction::processJoystickAxisEvent(ALLEGRO_EVENT* ev)
{
	if (ev->joystick.id == al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex()))
	{
		if ((ev->joystick.axis == gamepad1 && gamepad1Stick == ev->joystick.stick) || (ev->joystick.axis == gamepad2 && gamepad2Stick == ev->joystick.stick))
		{
			bool newState = false;
			float deadzone = Game::getGame()->getSettingsMan()->getAxisDeadzone();
			if (gamepad1Invert)
			{
				if (ev->joystick.pos <= -deadzone)
				{
					newState = true;
					axisInput = true;
					value = max(value, -ev->joystick.pos * GAMEPAD_AXIS_SENSITIVITY);
				}
				else if (ev->joystick.pos < deadzone)
				{
					axisInput = false;
				}
			}
			else
			{
				if (ev->joystick.pos >= deadzone)
				{
					newState = true;
					axisInput = true;
					value = max(value, ev->joystick.pos * GAMEPAD_AXIS_SENSITIVITY);
				}
				else if (ev->joystick.pos > -deadzone)
				{
					axisInput = false;
				}
			}

			changed = changed || newState != pressed;
			pressed = newState;
		}
	}
}

void InputAction::updateValue()
{
	if (pressed)
	{
		if (value == 0)
		{
			value = 1;
		}
	}
	else
	{
		value = 0;
	}
}

float InputAction::getValue()
{
	return value;
}


float InputAction::getMouseAxisValue()
{
	return mouseAxisValue;
}


float InputAction::isAxisInput()
{
	return axisInput;
}

bool InputAction::isPressed()
{
	return pressed;
}

bool InputAction::isJustPressed()
{
	return pressed && changed;
}

bool InputAction::isJustReleased()
{
	return !pressed && changed;
}

std::string InputAction::getBoundName(int type)
{
	int value = -1;
	int axis = -1;
	bool invert = false;
	switch (type)
	{
		case INPUT_TYPE_MOUSE2:
			value = mouse2;
			axis = mouse2Axis;
			invert = mouse2Invert;
			break;
		case INPUT_TYPE_MOUSE1:
			value = mouse1;
			axis = mouse1Axis;
			invert = mouse1Invert;
			break;
		case INPUT_TYPE_GAMEPAD1:
			value = gamepad1;
			axis = gamepad1Stick;
			invert = gamepad1Invert;
			break;
		case INPUT_TYPE_GAMEPAD2:
			value = gamepad2;
			axis = gamepad2Stick;
			invert = gamepad2Invert;
			break;
		case INPUT_TYPE_KEY2:
			value = key2;
			break;
		case INPUT_TYPE_KEY1:
			value = key1;
			break;
	}
	if (value >= 0)
	{
		switch (type)
		{
			case INPUT_TYPE_KEY1:
			case INPUT_TYPE_KEY2:
				return al_keycode_to_name(value);
			case INPUT_TYPE_GAMEPAD1:
			case INPUT_TYPE_GAMEPAD2:
				if (Game::getGame()->getSettingsMan()->getEnableJoystick())
				{
					if (axis < 0)
					{
						return al_get_joystick_button_name(al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex()), value);
					}
					else
					{
						std::string boundName = al_get_joystick_stick_name(al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex()), axis);
						boundName += + " ";
						boundName += al_get_joystick_axis_name(al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex()), axis, value);
						if (invert)
						{
							boundName += "-";
						}
						else
						{
							boundName += "+";
						}
						return boundName;
					}
				}
				else
				{
					return "Disabled";
				}
				break;
			case INPUT_TYPE_MOUSE1:
			case INPUT_TYPE_MOUSE2:
				{
					std::stringstream boundName;
					if (axis != true)
					{
						if (value == 1)
						{
							return "Left Button";
						}
						else if (value == 2)
						{
							return "Right Button";
						}
						else
						{
							boundName << "Button " << value;
						}
					}
					else
					{
						switch (value)
						{
							case 0:
								boundName << "X";
								break;
							case 1:
								boundName << "Y";
								break;
							case 2:
								boundName << "Wheel";
								break;
							case 3:
								boundName << "Wheel";
								break;
							default:
								boundName << "Unknown";
						}
						if (invert == true)
						{
							boundName << "-";
						}
						else
						{
							boundName << "+";
						}
					}
					return boundName.str();
				}
		}
	}
	return "";
}



InputManager::InputManager()
{
	al_set_mouse_wheel_precision(AXIS_MOVE_THRESHOLD);
	grabMouse = false;
	mouseReady = true;
	lastMousePos = vec2(0, 0);
	lastMouseClickState = false;
	digitalTurn = 0.0f;
	listenBinding = false;
	listenBindingType = -1;

	currentJoystick = NULL;

	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i] = NULL;
	}
	loadBindingsDefaults(false);
}

InputManager::~InputManager()
{
	freeBindings();
}


void InputManager::loadBindingsDefaults(bool save)
{
	freeBindings();
	//												Name				Key1				Key2			Gamepad1			Gamepad1Stick				Gamepad1Invert	Gamepad2			Gamepad2Stick	Gamepad1Invert	Mouse1				Mouse1Axis		Mouse1Invert	Mouse2				Mouse2Axis		Mouse2Invert
	input[INPUT_FORWARD]		= new InputAction("FORWARD",			ALLEGRO_KEY_W,		-1,				AXIS_FORWARD,		STICK_FORWARD_BACKWARD,		true,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_BACKWARD]		= new InputAction("BACKWARD",			ALLEGRO_KEY_S,		-1,				AXIS_BACKWARD,		STICK_FORWARD_BACKWARD,		false,			-1,					-1,				true,			-1,					false,			true,			-1,					false,			true);
	input[INPUT_RIGHT]			= new InputAction("RIGHT",				ALLEGRO_KEY_D,		-1,				AXIS_RIGHT,			STICK_LEFT_RIGHT,			false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_LEFT]			= new InputAction("LEFT",				ALLEGRO_KEY_A,		-1,				AXIS_LEFT,			STICK_LEFT_RIGHT,			true,			-1,					-1,				true,			-1,					false,			true,			-1,					false,			true);
	input[INPUT_JUMP]			= new InputAction("JUMP",				ALLEGRO_KEY_SPACE,	-1,				10,					-1,							false,			 1,					-1,				false,			 2,					false,			false,			-1,					false,			false);
	input[INPUT_HEAL]			= new InputAction("HEAL",				ALLEGRO_KEY_Q,		ALLEGRO_KEY_H,	 2,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_FIRE]			= new InputAction("FIRE",				ALLEGRO_KEY_ENTER,	ALLEGRO_KEY_UP,	 1,					 2,							false,			 0,					-1,				false,			 1,					false,			false,			-1,					false,			false);
	input[INPUT_LOOKRIGHT]		= new InputAction("LOOK RIGHT",			ALLEGRO_KEY_RIGHT,	-1,				AXIS_LOOKRIGHT,		STICK_LOOK,					false,			-1,					-1,				false,			 0,					true,			false,			-1,					false,			false);
	input[INPUT_LOOKLEFT]		= new InputAction("LOOK LEFT",			ALLEGRO_KEY_LEFT,	-1,				AXIS_LOOKLEFT,		STICK_LOOK,					true,			-1,					-1,				true,			 0,					true,			true,			-1,					false,			true);
	input[INPUT_WEAPON1]		= new InputAction("WEAPON 1",			ALLEGRO_KEY_1,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_WEAPON2]		= new InputAction("WEAPON 2",			ALLEGRO_KEY_2,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_WEAPON3]		= new InputAction("WEAPON 3",			ALLEGRO_KEY_3,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_WEAPON4]		= new InputAction("WEAPON 4",			ALLEGRO_KEY_4,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_WEAPON_NEXT]	= new InputAction("NEXT WEAPON",		ALLEGRO_KEY_R,		-1,				 4,					-1,							false,			-1,					-1,				false,			 2,					true,			false,			-1,					false,			false);
	input[INPUT_WEAPON_PREV]	= new InputAction("PREVIOUS WEAPON",	ALLEGRO_KEY_F,		-1,				 5,					-1,							true,			-1,					-1,				true,			 2,					true,			true,			-1,					false,			true);
	input[INPUT_PAUSE]			= new InputAction("PAUSE",				ALLEGRO_KEY_ESCAPE,	-1,				 6,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_UI_UP]			= new InputAction("UI UP",				ALLEGRO_KEY_UP,		-1,				 1,					 3,							true,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_UI_DOWN]		= new InputAction("UI DOWN",			ALLEGRO_KEY_DOWN,	-1,				 1,					 3,							false,			-1,					-1,				true,			-1,					false,			true,			-1,					false,			true);
	input[INPUT_UI_RIGHT]		= new InputAction("UI RIGHT",			ALLEGRO_KEY_RIGHT,	-1,				 0,					 3,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_UI_LEFT]		= new InputAction("UI LEFT",			ALLEGRO_KEY_LEFT,	-1,				 0,					 3,							true,			-1,					-1,				true,			-1,					false,			true,			-1,					false,			true);
	input[INPUT_UI_SELECT]		= new InputAction("UI SELECT",			ALLEGRO_KEY_ENTER,	ALLEGRO_KEY_X,	 0,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_GRAB_MOUSE]		= new InputAction("RELEASE MOUSE",		ALLEGRO_KEY_F1,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);
	input[INPUT_SWAP_SCENERY]	= new InputAction("DEBUG SWAP SCENE",	ALLEGRO_KEY_F2,		-1,				-1,					-1,							false,			-1,					-1,				false,			-1,					false,			false,			-1,					false,			false);

	//bindingsDirty = true;
	if (save)
	{
		Game::getGame()->getSettingsMan()->saveBindings();
	}
}

void InputManager::freeBindings()
{
	for (int i = 0; i < INPUT_MAX; i++)
	{
		if (input[i] != NULL)
		{
			delete input[i];
		}
	}
}

void InputManager::updateBinding(ALLEGRO_EVENT ev)
{
	if (ev.any.source == al_get_keyboard_event_source() && ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
	{
		//Keep escape from being assigned to anything - that way madness lies
		((GUIBindingButton*)Game::getGame()->currentScreen->getFocusedWidget())->cancelAction();
		Game::getGame()->currentScreen->setDirty(true);
		return;
	}

	//TODO: Add a way of clearing the current binding

	int value = -1;
	int axis = -1;
	bool invert = false;
	switch (listenBindingType)
	{
		case INPUT_TYPE_KEY1:
		case INPUT_TYPE_KEY2:
			{
				//TODO: Make these source checks nicer?
				if (ev.any.source != al_get_keyboard_event_source())
				{
					return;
				}

				if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
				{
					return;
				}
				value = ev.keyboard.keycode;
				break;
			}
		case INPUT_TYPE_GAMEPAD1:
		case INPUT_TYPE_GAMEPAD2:
			{
				if (ev.any.source != al_get_joystick_event_source())
				{
					return;
				}

				if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP) //TODO: Should this be button_down?
				{
					value = ev.joystick.button;
				}
				else if (ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS && abs(ev.joystick.pos) > AXIS_PRESSED_THRESHOLD)
				{
					value = ev.joystick.axis;
					axis = ev.joystick.stick;
					invert = ev.joystick.pos < 0;
				}
				break;
			}
		case INPUT_TYPE_MOUSE1:
		case INPUT_TYPE_MOUSE2:
			{
				if (ev.any.source != al_get_mouse_event_source())
				{
					return;
				}

				if (ev.type == ALLEGRO_EVENT_MOUSE_AXES)
				{
					vec2 motion = vec2(ev.mouse.x, ev.mouse.y) - lastMousePos;
					vec2 motionWheel = vec2(ev.mouse.dz, ev.mouse.dw);
					if (abs(motion.x) >= AXIS_MOVE_THRESHOLD)
					{
						value = MOUSE_X;
						axis = true;
						invert = motion.x < 0;
					}
					else if (abs(motion.y) >= AXIS_MOVE_THRESHOLD)
					{
						value = MOUSE_Y;
						axis = true;
						invert = motion.y < 0;
					}
					else if (abs(motionWheel.x) >= AXIS_MOVE_THRESHOLD)
					{
						value = MOUSE_WHEEL_1;
						axis = true;
						invert = motionWheel.x < 0;
					}
					else if (abs(motionWheel.y) >= AXIS_MOVE_THRESHOLD)
					{
						value = MOUSE_WHEEL_2;
						axis = true;
						invert = motionWheel.y < 0;
					}
				}
				else
				{
					value = ev.mouse.button;
				}
				break;
			}
	}

	if (value != -1)
	{
		Print("Valid binding value detected...");

		GUIBindingButton* widget = (GUIBindingButton*)Game::getGame()->currentScreen->getFocusedWidget();

		switch (listenBindingType)
		{
			case INPUT_TYPE_KEY1:
				widget->inputAction->key1 = value;
				break;
			case INPUT_TYPE_KEY2:
				widget->inputAction->key2 = value;
				break;
			case INPUT_TYPE_MOUSE1:
				widget->inputAction->mouse1 = value;
				widget->inputAction->mouse1Axis = axis == 1;
				widget->inputAction->mouse1Invert = invert;
				break;
			case INPUT_TYPE_MOUSE2:
				widget->inputAction->mouse2 = value;
				widget->inputAction->mouse2Axis = axis == 1;
				widget->inputAction->mouse2Invert = invert;
				break;
			case INPUT_TYPE_GAMEPAD1:
				widget->inputAction->gamepad1 = value;
				widget->inputAction->gamepad1Stick = axis;
				widget->inputAction->gamepad1Invert = invert;
				break;
			case INPUT_TYPE_GAMEPAD2:
				widget->inputAction->gamepad2 = value;
				widget->inputAction->gamepad2Stick = axis;
				widget->inputAction->gamepad2Invert = invert;
				break;
		}
		for (int i = 0; i < INPUT_MAX; i++)
		{
			input[i]->clearStates();
		}
		widget->applyAction();
		Game::getGame()->currentScreen->setDirty(true);
		if (Game::getGame()->getSettingsMan()->getEnableJoystick())
		{
			if (al_get_num_joysticks() > 0)
			{
				currentJoystick = al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex());
			}
		}
	}
}

void InputManager::initialiseJoystick()
{
	if (Game::getGame()->getSettingsMan()->getEnableJoystick())
	{
		Print("Initialising joystick...");
		if(!al_install_joystick()) {
			Print("Failed to initialize joystick");
			Game::getGame()->getSettingsMan()->setEnableJoystick(false);
		}
	}
	if (Game::getGame()->getSettingsMan()->getEnableJoystick())
	{
		if (al_get_num_joysticks() > 0)
		{
			currentJoystick = al_get_joystick(Game::getGame()->getSettingsMan()->getJoystickIndex());
			Print("Using joystick", al_get_joystick_name(currentJoystick));
			Game::getGame()->registerEventSource(al_get_joystick_event_source());
		}
		else
		{
			Print("No joysticks found");
			Game::getGame()->getSettingsMan()->setEnableJoystick(false);
		}
	}
	else
	{
		Print("Disabling joystick...");
		al_uninstall_joystick();
	}
}

std::string InputManager::getDefaultBoundName(int action)
{
	//TODO: Return prompts last used input device? (keyboard, gamepad, mouse)
	if (input[action]->key1 >= 0)
	{
		return toUpper(input[action]->getBoundName(INPUT_TYPE_KEY1));
	}
	else
	{
		return "!!!UNBOUND!!!";
	}
}

void InputManager::clearInputs()
{
	digitalTurn = 0.0f;
	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->clearStates();
	}
}

void InputManager::accumulateKeyboardEvent(ALLEGRO_EVENT* ev)
{
	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->processKeyboardEvent(ev);
	}
}

void InputManager::accumulateMouseButtonEvent(ALLEGRO_EVENT* ev)
{
	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->processMouseButtonEvent(ev);
	}
}

void InputManager::accumulateMouseAxisEvent(ALLEGRO_EVENT* ev)
{
	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->processMouseAxisEvent(ev);
	}

	ALLEGRO_MOUSE_STATE* mState = new ALLEGRO_MOUSE_STATE();
	al_get_mouse_state(mState);
	al_set_mouse_z(0);
	al_set_mouse_w(0);
	if (grabMouse)// && (mState->x != MOUSE_WARP_POS || mState->y != MOUSE_WARP_POS))
	{
		al_set_mouse_xy(Game::getGame()->getGraphicsMan()->getDisplay(), MOUSE_WARP_POS, MOUSE_WARP_POS);
	}
}

void InputManager::accumulateJoystickButtonEvent(ALLEGRO_EVENT* ev)
{
	if (Game::getGame()->getSettingsMan()->getEnableJoystick())
	{
		for (int i = 0; i < INPUT_MAX; i++)
		{
			input[i]->processJoystickButtonEvent(ev);
		}
	}
}

void InputManager::accumulateJoystickAxisEvent(ALLEGRO_EVENT* ev)
{
	if (Game::getGame()->getSettingsMan()->getEnableJoystick())
	{
		for (int i = 0; i < INPUT_MAX; i++)
		{
			input[i]->processJoystickAxisEvent(ev);
		}
	}
}

void InputManager::processInput()
{
	ALLEGRO_MOUSE_STATE *mState = NULL;
	if (grabMouse && Game::getGame()->currentScreen == NULL)
	{
		mState = new ALLEGRO_MOUSE_STATE();
		al_get_mouse_state(mState);
	}

	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->updateValue();
	}

	if (Game::getGame()->currentScreen != NULL)
	{
		{
			processMenuInputMouse();
			processMenuInput();
		}
	}
	else
	{
		//TODO: Move paused into InputManager?
		if (!Game::getGame()->paused)
		{
			//TODO: Tidy this up
			if(!grabMouse)
			{
				if (mState == NULL)
				{
					mState = new ALLEGRO_MOUSE_STATE();
					al_get_mouse_state(mState);
				}
				if (al_mouse_button_down(mState, 1))
				{
					setGrabMouse(true);
					Game::getGame()->getAudioMan()->duckMusicVolume(false);
					delete mState;
					return;
				}
			}
			else
			{
				if (!al_mouse_button_down(mState, 1))
				{
					//Allegro doesn't let us programmatically release the mouse button, so we need to wait for the player to let go before we pay attention to input
					mouseReady = true;
				}

				//HACK: Wait for mouse state.x to settle after warping when unpausing
				if (Game::getGame()->unpausing)
				{
					if (mState->x == MOUSE_WARP_POS)
					{
						Game::getGame()->unpausing = false;
					}
					else
					{
						return;
					}
				}
			}
		}
		if (mouseReady)
		{
			processGameInput();
		}
	}

	for (int i = 0; i < INPUT_MAX; i++)
	{
		input[i]->changed = false;
		input[i]->mouseAxisValue = 0;
	}


	if (mState != NULL)
	{
		delete mState;
	}
}

void InputManager::setListenBinding(int inputType)
{
	//FIXME: These register/unregister event source calls are redundant now, I think
	if (inputType >= 0)
	{
		listenBinding = true;
		listenBindingType = inputType;

		switch (listenBindingType)
		{
			case INPUT_TYPE_KEY1:
			case INPUT_TYPE_KEY2:
				{
					Game::getGame()->registerEventSource(al_get_keyboard_event_source());
					break;
				}
			case INPUT_TYPE_GAMEPAD1:
			case INPUT_TYPE_GAMEPAD2:
				{
					if (Game::getGame()->getSettingsMan()->getEnableJoystick())
					{
						Game::getGame()->registerEventSource(al_get_joystick_event_source());
					}
					break;
				}
			case INPUT_TYPE_MOUSE1:
			case INPUT_TYPE_MOUSE2:
				{
					Game::getGame()->registerEventSource(al_get_mouse_event_source());
					break;
				}
		}
	}
	else
	{
		listenBinding = false;
		listenBindingType = -1;
		Game::getGame()->registerEventSource(al_get_keyboard_event_source());
		Game::getGame()->registerEventSource(al_get_mouse_event_source());
		Game::getGame()->registerEventSource(al_get_joystick_event_source());

	}
}

bool InputManager::isInputJustPressed(int action)
{
	return input[action]->isJustPressed();
}

bool InputManager::isInputJustReleased(int action)
{
	return input[action]->isJustReleased();
}

bool InputManager::isInputPressed(int action)
{
	return input[action]->isPressed();
}

float InputManager::getInputValue(int action)
{
	return input[action]->getValue();
}

float InputManager::getInputMouseAxisValue(int action)
{
	return input[action]->getMouseAxisValue();
}

bool InputManager::isInputAxis(int action)
{
	return input[action]->isAxisInput();
}

void InputManager::setGrabMouse(bool grab)
{
	if (grab)
	{
		al_set_mouse_xy(Game::getGame()->getGraphicsMan()->getDisplay(), MOUSE_WARP_POS, MOUSE_WARP_POS);
		al_hide_mouse_cursor(Game::getGame()->getGraphicsMan()->getDisplay());
		grabMouse = true;
	}
	else
	{
		al_show_mouse_cursor(Game::getGame()->getGraphicsMan()->getDisplay());
		grabMouse = false;
	}
}

void InputManager::processMenuInputMouse()
{
	ALLEGRO_MOUSE_STATE state;
	al_get_mouse_state(&state);
	if (lastMousePos.x != state.x || lastMousePos.y != state.y || al_mouse_button_down(&state, 1) != lastMouseClickState)
	{
		lastMousePos.x = state.x;
		lastMousePos.y = state.y;
		lastMouseClickState = al_mouse_button_down(&state, 1);
		Game::getGame()->currentScreen->processMouse(lastMousePos, lastMouseClickState);
	}
}


void InputManager::processMenuInput()
{
	bool doAction = isInputJustReleased(INPUT_UI_SELECT);
	bool doLeft = isInputJustReleased(INPUT_LEFT) || isInputJustReleased(INPUT_UI_LEFT);
	bool doRight = isInputJustReleased(INPUT_RIGHT) || isInputJustReleased(INPUT_UI_RIGHT);
	bool doUp = isInputJustReleased(INPUT_FORWARD) || isInputJustReleased(INPUT_UI_UP);
	bool doDown = isInputJustReleased(INPUT_BACKWARD) || isInputJustReleased(INPUT_UI_DOWN);
	bool doBackPress = isInputJustReleased(INPUT_PAUSE);

	bool skip = false;
	if (doAction)
	{
		Game::getGame()->currentScreen->doFocusedAction();
		skip = true;
	}
	if (!skip && doBackPress)
	{
		if (Game::getGame()->currentScreen == Game::getGame()->screenPause)
		{
			ActionManager::getActionManager()->doResume();
		}
		else
		{
			if (Game::getGame()->currentScreen == Game::getGame()->screenBindings)
			{
				ActionManager::getActionManager()->doSettings();
			}
			else
			{
				ActionManager::getActionManager()->doBack();
			}
		}
		Game::getGame()->getAudioMan()->playUISound(SOUND_UI_SELECT, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
		skip = true;
	}
	if (doLeft && !skip)
	{
		Game::getGame()->currentScreen->focusNextWidget(FOCUS_DIRECTION_LEFT);
		skip = true;
	}
	if (doRight && !skip)
	{
		Game::getGame()->currentScreen->focusNextWidget(FOCUS_DIRECTION_RIGHT);
		skip = true;
	}
	if (doUp && !skip)
	{
		Game::getGame()->currentScreen->focusNextWidget(FOCUS_DIRECTION_UP);
		skip = true;
	}
	if (doDown && !skip)
	{
		Game::getGame()->currentScreen->focusNextWidget(FOCUS_DIRECTION_DOWN);
		skip = true;
	}
}


void InputManager::processGameInput()
{
	bool doFire = isInputPressed(INPUT_FIRE);
	bool doPausePress = isInputJustReleased(INPUT_PAUSE);

	digitalDrag = DIGITAL_DRAG_RELEASE;

	if(doFire)
	{
		if (!grabMouse)
		{
			setGrabMouse(true);
		}

		if (Game::getGame()->paused)
		{
			Game::getGame()->doPause(false);
		}
		else
		{
			Game::getGame()->getPlayer()->fire();
		}
	}

	if(doPausePress)
	{
		if (Game::getGame()->currentScreen == NULL)
		{
			Game::getGame()->showScreen(Game::getGame()->screenPause);
			Game::getGame()->doPause(true);
		}
		else
		{
			if (Game::getGame()->currentScreen == Game::getGame()->screenPause)
			{
				Game::getGame()->doPause(false);
			}
			else
			{
				ActionManager::getActionManager()->doBack();
			}
			Game::getGame()->getAudioMan()->playUISound(SOUND_UI_SELECT, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
		}
	}

	if (!Game::getGame()->paused && !Game::getGame()->quit && Game::getGame()->currentScreen == NULL)
	{
		bool doJump = isInputPressed(INPUT_JUMP);
		bool doHeal = isInputPressed(INPUT_HEAL);

		float lookLeftRight = getInputValue(INPUT_LOOKRIGHT) - getInputValue(INPUT_LOOKLEFT);
		float lookLeftRightMouse = getInputMouseAxisValue(INPUT_LOOKRIGHT) - getInputMouseAxisValue(INPUT_LOOKLEFT);

		bool doForward = isInputPressed(INPUT_FORWARD);
		bool doBackward = isInputPressed(INPUT_BACKWARD);
		bool doLeft = isInputPressed(INPUT_LEFT);
		bool doRight = isInputPressed(INPUT_RIGHT);

		bool doWeapon1 = isInputPressed(INPUT_WEAPON1);
		bool doWeapon2 = isInputPressed(INPUT_WEAPON2);
		bool doWeapon3 = isInputPressed(INPUT_WEAPON3);

		bool doWeaponNext = isInputPressed(INPUT_WEAPON_NEXT);
		bool doWeaponPrev = isInputPressed(INPUT_WEAPON_PREV);

		if(doForward)
		{
			//world3D->incrementCamera(vec3(-0.05f, 0.0f, 0.0f));
			Game::getGame()->getPlayer()->holdForward();
		}
		if(doBackward)
		{
			//world3D->incrementCamera(vec3(-0.05f, 0.0f, 0.0f));
			Game::getGame()->getPlayer()->holdBackward();
		}
		if(doLeft)
		{
			//world3D->incrementCamera(vec3(0.0f, 0.05f, 0.0f));
			Game::getGame()->getPlayer()->holdLeft();
		}
		if(doRight)
		{
			//world3D->incrementCamera(vec3(0.0f, -0.05f, 0.0f));
			Game::getGame()->getPlayer()->holdRight();
		}
		if(doJump)
		{
			Game::getGame()->getPlayer()->jump();
		} else {
			Game::getGame()->getPlayer()->jumpRelease();
		}

		if(doHeal)
		{
			Game::getGame()->getPlayer()->heal();
		}

		if(doWeapon1)
		{
			Game::getGame()->getPlayer()->switchWeapon(0);
		}

		if(doWeapon2)
		{
			Game::getGame()->getPlayer()->switchWeapon(1);
		}

		if(doWeapon3)
		{
			Game::getGame()->getPlayer()->switchWeapon(2);
		}

		if (doWeaponNext)
		{
			Game::getGame()->getPlayer()->switchWeaponNext();
		}

		if (doWeaponPrev)
		{
			Game::getGame()->getPlayer()->switchWeaponPrevious();
		}

		if (isInputJustReleased(INPUT_SWAP_SCENERY))
		{
			Game::getGame()->floor->swapScenery();
		}

		if (isInputJustReleased(INPUT_GRAB_MOUSE))
		{
			setGrabMouse(!grabMouse);
		}


		if (grabMouse && lookLeftRightMouse != 0)
		{
				Game::getGame()->getPlayer()->turn(lookLeftRightMouse);
				digitalTurn = 0;
				digitalDrag = 0;
		}

		if (abs(lookLeftRight) > 0.1f)
		{
			if (isInputAxis(INPUT_LOOKRIGHT) || isInputAxis(INPUT_LOOKLEFT))
			{
				Game::getGame()->getPlayer()->turn(lookLeftRight);
				digitalTurn = 0;
				digitalDrag = 0;
			}
			else
			{
				//TODO: DIGITAL_ACCELERATION needs to be multiplied by tick speed to preserve behaviour at lower FPS
				if (lookLeftRight > 0)
				{
					digitalTurn += DIGITAL_ACCELERATION;
				}
				else
				{
					digitalTurn -= DIGITAL_ACCELERATION;
				}
				digitalDrag = DIGITAL_DRAG_HOLD;
			}
		}

		if (abs(digitalTurn) > 0.01f)
		{
			Game::getGame()->getPlayer()->turn(digitalTurn);
			digitalTurn *= digitalDrag;
		}
	}
}

