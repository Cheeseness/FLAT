/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include "utilities.h"


#define DIGITAL_DRAG_RELEASE 0.60
#define DIGITAL_DRAG_HOLD 0.985
#define DIGITAL_ACCELERATION 0.2f

#define AXIS_PRESSED_THRESHOLD 0.5f
#define AXIS_MOVE_THRESHOLD 50

#define GAMEPAD_AXIS_SENSITIVITY 8.0f
#define MOUSE_AXIS_SENSITIVITY 0.75f


#define INPUT_TYPE_KEY1		0
#define INPUT_TYPE_KEY2		1
#define INPUT_TYPE_GAMEPAD1	2
#define INPUT_TYPE_GAMEPAD2	3
#define INPUT_TYPE_MOUSE1	4
#define INPUT_TYPE_MOUSE2	5

#define INPUT_FORWARD		 0
#define INPUT_BACKWARD		 1
#define INPUT_RIGHT			 2
#define INPUT_LEFT			 3
#define INPUT_JUMP			 4
#define INPUT_HEAL			 5
#define INPUT_FIRE			 6
#define INPUT_LOOKRIGHT		 7
#define INPUT_LOOKLEFT		 8
#define INPUT_WEAPON1		 9
#define INPUT_WEAPON2		10
#define INPUT_WEAPON3		11
#define INPUT_WEAPON4		12
#define INPUT_WEAPON_NEXT	13
#define INPUT_WEAPON_PREV	14
#define INPUT_PAUSE			15
#define INPUT_UI_UP			16
#define INPUT_UI_DOWN		17
#define INPUT_UI_RIGHT		18
#define INPUT_UI_LEFT		19
#define INPUT_UI_SELECT		20
#define INPUT_GRAB_MOUSE	21
#define INPUT_SWAP_SCENERY	22
#define INPUT_MAX			23

#define STICK_FORWARD_BACKWARD 0
#define STICK_LEFT_RIGHT 0
#define STICK_LOOK 1

#define MOUSE_X 0
#define MOUSE_Y 1
#define MOUSE_WHEEL_1 2
#define MOUSE_WHEEL_2 3

#define MOUSE_WARP_POS 200

#define AXIS_FORWARD 1
#define AXIS_BACKWARD 1
#define AXIS_RIGHT 0
#define AXIS_LEFT 0
#define AXIS_LOOKRIGHT 1
#define AXIS_LOOKLEFT 1

class InputAction
{
	public:
		InputAction();
		InputAction(std::string n, int k1, int k2, int g1, int g1S, bool g1I, int g2, int g2S, bool g2I, int m1, bool m1A, bool m1I, int m2, bool m2A, bool m2I);

		int key1;
		int key2;

		int gamepad1;
		int gamepad1Stick;
		bool gamepad1Invert;
		int gamepad2;
		int gamepad2Stick;
		bool gamepad2Invert;

		int mouse1;
		bool mouse1Axis;
		bool mouse1Invert;
		int mouse2;
		bool mouse2Axis;
		bool mouse2Invert;

		std::string name;
		bool pressed;
		bool axisInput;
		float value;
		float mouseAxisValue;
		bool changed;
		void clearStates();
		void processKeyboardEvent(ALLEGRO_EVENT* ev);
		void processMouseButtonEvent(ALLEGRO_EVENT* ev);
		void processMouseAxisEvent(ALLEGRO_EVENT* ev);
		void processJoystickButtonEvent(ALLEGRO_EVENT* ev);
		void processJoystickAxisEvent(ALLEGRO_EVENT* ev);
		void updateValue();
		float getValue();
		float getMouseAxisValue();
		float isAxisInput();
		bool isPressed();
		bool isJustPressed();
		bool isJustReleased();
		std::string getBoundName(int type);
};

class InputManager
{
	public:
		InputManager();
		virtual ~InputManager();
		void initialiseJoystick();
		void loadBindingsDefaults(bool save);
		void freeBindings();
		void clearInputs();
		void accumulateKeyboardEvent(ALLEGRO_EVENT* ev);
		void accumulateMouseButtonEvent(ALLEGRO_EVENT* ev);
		void accumulateMouseAxisEvent(ALLEGRO_EVENT* ev);
		void accumulateJoystickButtonEvent(ALLEGRO_EVENT* ev);
		void accumulateJoystickAxisEvent(ALLEGRO_EVENT* ev);
		void processInput();
		void setListenBinding(int inputType);
		bool isInputJustPressed(int action);
		bool isInputJustReleased(int action);
		bool isInputPressed(int action);
		bool isInputAxis(int action);
		float getInputValue(int action);
		float getInputMouseAxisValue(int action);
		std::string getDefaultBoundName(int action);
		InputAction* input[INPUT_MAX];


		bool grabMouse;
		bool mouseReady;
		vec2 lastMousePos;
		bool lastMouseClickState;
		bool listenBinding;
		int listenBindingType;

		float digitalTurn;
		float digitalDrag;

		void setGrabMouse(bool grab);
		void processGameInput();
		void processMenuInputMouse();
		void processMenuInput();
		void updateBinding(ALLEGRO_EVENT ev);

	private:
		ALLEGRO_JOYSTICK* currentJoystick;

};

#endif //INPUTMANAGER_H
