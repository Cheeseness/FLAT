/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUITOGGLE_H
#define GUITOGGLE_H

#include "GUIButton.h"

class GUIToggle : public GUIButton
{
	public:
		GUIToggle();
		GUIToggle(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, const char* _text, ACTIONBOOL a);
		virtual ~GUIToggle();

		void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void doAction();
		void setValue(bool p);
		bool setValue();
		void updateSizePlus();

	protected:

	private:
		bool isPressed;
		ACTIONBOOL action;
		int toggleMargin;
		int toggleGrowth;
		int borderWidth;
};

#endif //GUITOGGLE_H
