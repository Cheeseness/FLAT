/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIBINDINGBUTTON_H
#define GUIBINDINGBUTTON_H

#include "GUIWidget.h"
#include "InputManager.h"

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>

class GUIBindingButton : public GUIWidget
{
	public:
		GUIBindingButton();
		GUIBindingButton(ALLEGRO_FONT* _font, ALLEGRO_COLOR _colour, ALLEGRO_COLOR _colourActive, ALLEGRO_COLOR _colourBad, InputAction* _inputAction, int _inputType, ACTION a);
		virtual ~GUIBindingButton();

		void draw(vec2 aspectMultiplier, vec2 positionOffset = vec2(0,0));
		void updateText();
		void setFont(ALLEGRO_FONT* _font, ALLEGRO_COLOR _colour);
		void setFocusState(bool focus, bool silent);
		void setConflict(bool c);
		void doAction();
		void cancelAction();
		void applyAction();
		InputAction* inputAction;

	protected:
		ALLEGRO_FONT* font;
		ALLEGRO_COLOR colour;
		ALLEGRO_COLOR colourActive;
		ALLEGRO_COLOR colourBad;
		std::string text;
		int inputType;

		int borderWidth;
		bool active;
		bool conflict;


	private:
};

#endif //GUIBINDINGBUTTON_H
