/*
Copyright 2012 Johan "SteelRaven7" Hassel
Copyright 2013 ptitSeb
Copyright 2021-2022 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "DrawableText.h"

DrawableText::DrawableText()
{
	text = "Nothing";
	position = vec2(0.0f, 0.0f);
	visible = true;
	align = 0;
}

DrawableText::DrawableText(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color)
{
	font = _font;
	position = vec2(0.0f, 0.0f);
	color = _color;
	visible = true;
	align = 0;
}

DrawableText::~DrawableText()
{
	//dtor
}

void DrawableText::draw(vec2 aspectMultiplier) {
	if (visible)
	{
		al_draw_text(font, color, position.x * aspectMultiplier.x, position.y * aspectMultiplier.y, align, text.c_str());
	}
}

void DrawableText::setFont(ALLEGRO_FONT* _font)
{
	font = _font;
}

void DrawableText::setPosition(vec2 _position) {
	position = _position;
}

void DrawableText::setText(std::string _text) {
	text = _text;
	align = ALLEGRO_ALIGN_LEFT;
}

void DrawableText::setTextCentered(std::string _text) {
	text = _text;
	align = ALLEGRO_ALIGN_CENTER;
}

void DrawableText::setVisibility(bool b)
{
	visible = b;
}
