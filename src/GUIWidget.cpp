/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIWidget.h"
#include "Game.h"

GUIWidget::GUIWidget()
{
	position = vec2(-1, -1);
	size = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	hasFocus = false;
	isClicked = false;
	action = NULL;
	isMouseOver = false;
	leftAlign = true;
	visible = true;
	dirty = true;
}

GUIWidget::~GUIWidget()
{

}

void GUIWidget::draw(vec2 aspectMultiplier, vec2 positionOffset) {
	Print("Non-overloaded GUI draw() call!");
}

void GUIWidget::setPosition(vec2 pos)
{
	position = pos;
	updatePositionPlus();
	dirty = true;
}

void GUIWidget::setPosition(vec2 pos, int anchorMode, vec2 target)
{
	if (anchorMode == ANCHOR_BL || anchorMode == ANCHOR_BR)
	{
		pos.y -= sizePlus.y;
	}
	if (anchorMode == ANCHOR_TR || anchorMode == ANCHOR_BR)
	{
		pos.x -= sizePlus.x;
		leftAlign = false;
	}
	pos += target;
	position = pos;
	updatePositionPlus();
	dirty = true;
}

void GUIWidget::setPosition(vec2 pos, int anchorMode, GUIWidget* target, int targetMode)
{
	vec2 offsetTL = vec2(0, 0);
	vec2 offsetBR = vec2(0, 0);

	if (target != NULL)
	{
		offsetTL.x = target->getPosition().x;
		offsetTL.y = target->getPosition().y;
		offsetBR.x = target->getPosition().x + target->getSizePlus().x;
		offsetBR.y = target->getPosition().y + target->getSizePlus().y;
	}

	if (anchorMode == ANCHOR_BL || anchorMode == ANCHOR_BR)
	{
		pos.y -= sizePlus.y;
	}
	if (anchorMode == ANCHOR_TR || anchorMode == ANCHOR_BR)
	{
		pos.x -= sizePlus.x;
		leftAlign = false;
	}

	if (targetMode == ANCHOR_TL || targetMode == ANCHOR_TR)
	{
		pos.y += offsetTL.y;
	}
	else
	{
		pos.y += offsetBR.y;
	}
	if (targetMode == ANCHOR_TR || targetMode == ANCHOR_BR)
	{
		pos.x += offsetBR.x;
	}
	else
	{
		pos.x += offsetTL.x;
	}

	position = pos;
	updatePositionPlus();
	dirty = true;
}

vec2 GUIWidget::getPosition()
{
	return position;
}

vec2 GUIWidget::getPositionPlus()
{
	return positionPlus;
}

void GUIWidget::setSize(vec2 s)
{
	if (s.x >= 0)
	{
		size.x = s.x;
	}
	if (s.y >= 0)
	{
		size.y = s.y;
	}
	updateSizePlus();
	dirty = true;
}

vec2 GUIWidget::getSize()
{
	return size;
}

vec2 GUIWidget::getSizePlus()
{
	return sizePlus;
}

void GUIWidget::setMargin(vec2 m)
{
	margin = m;
	updatePositionPlus();
	dirty = true;
}

void GUIWidget::setPadding(vec2 p)
{
	padding = p;
	updateSizePlus();
	dirty = true;
}

void GUIWidget::updateSizePlus()
{
	sizePlus.x = size.x + padding.x * 2;
	sizePlus.y = size.y + padding.y * 2;
}

void GUIWidget::updatePositionPlus()
{
	positionPlus.x = position.x + margin.x;
	positionPlus.y = position.y + margin.y;
}

bool GUIWidget::getFocusState()
{
	return hasFocus;
}

void GUIWidget::setFocusState(bool focus, bool silent)
{
	hasFocus = focus;
	if (focus && !silent)
	{
		Game::getGame()->getAudioMan()->playUISound(SOUND_UI_FOCUS, 0.3f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
	}
	dirty = true;
}

void GUIWidget::setHoverState(bool hover)
{
	isMouseOver = hover;
	if (hover)
	{
		Game::getGame()->getAudioMan()->playUISound(SOUND_UI_HOVER, 0.3f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
	}
	dirty = true;
}

void GUIWidget::setVisibility(bool v)
{
	visible = v;
	dirty = true;
}

GUIWidget* GUIWidget::getFocusedWidget()
{
	return this;
}

void GUIWidget::checkClickExtra(vec2 coords)
{
}

bool GUIWidget::checkCoords(vec2 coords, bool takeFocus, bool doClick)
{
	if (isClicked)
	{
		//return false;
	}

	bool doMouseOut = false;

	if (coords.x >= positionPlus.x && coords.x <= positionPlus.x + sizePlus.x)
	{
		if (coords.y >= positionPlus.y && coords.y <= positionPlus.y + sizePlus.y)
		{
			if (takeFocus)
			{
				if (!hasFocus)
				{
					setFocusState(true, true);
				}
			}
			if (doClick)
			{
				checkClickExtra(coords);
				if (!isClicked)
				{
					isClicked = true;
					dirty = true;
					return true;
				}
			}
			else
			{
				if (isClicked)
				{
					unclick();
				}
				if (!hasFocus)
				{
					if (!isMouseOver)
					{
						//do hover
						setHoverState(true);
					}
					isMouseOver = true;
					dirty = true;
				}
			}
		}
		else
		{
			doMouseOut = true;
		}
	}
	else
	{
		doMouseOut = true;
	}

	if (doMouseOut)
	{
		if (isMouseOver)
		{
			isMouseOver = false;
			unclick();
			dirty = true;
		}
		if (takeFocus)
		{
			setFocusState(false);
			dirty = true;
		}
	}

	return false;
}

bool GUIWidget::doLeft()
{
	return false;
}

bool GUIWidget::doRight()
{
	return false;
}

bool GUIWidget::isDirty()
{
	return dirty;
}

void GUIWidget::unclick()
{
	isClicked = false;
	dirty = true;
}

void GUIWidget::setAction(ACTION a)
{
	action = a;
}

void GUIWidget::doAction()
{
	if (action != NULL)
	{
		Game::getGame()->getAudioMan()->playUISound(SOUND_UI_SELECT, 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE);
		action();
	}
}

void GUIWidget::printDetails()
{
	Print("Widget\t", this);
	Print("  start", getPosition());
	Print("  end  ", getPosition() + getSize());
}
