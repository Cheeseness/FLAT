/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIImage.h"

GUIImage::GUIImage(ALLEGRO_BITMAP* i)
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	reflection = false;
	visible = true;
	dirty = true;
	aspectMultiplier = vec2(1, 1);
	setImage(i);
}

GUIImage::~GUIImage()
{

}

void GUIImage::draw(vec2 am, vec2 positionOffset)
{
	if (am != aspectMultiplier)
	{
		aspectMultiplier.x = am.x;
		aspectMultiplier.y = am.y;
		updateSizePlus();
	}

	if (!visible || !dirty)
	{
		return;
	}

	if (reflection)
	{
		al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
		al_draw_tinted_scaled_bitmap(image, al_map_rgba(255, 255, 255, 50), 0, 0, size.x, size.y, positionOffset.x + positionPlus.x + padding.x, positionOffset.y + positionPlus.y + sizePlus.y, size.x * aspectMultiplier.x, size.y * aspectMultiplier.y, ALLEGRO_FLIP_VERTICAL);
		al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
	}
	al_draw_scaled_bitmap(image, 0, 0, size.x, size.y, positionOffset.x + positionPlus.x + padding.x, positionOffset.y + positionPlus.y + padding.y, size.x * aspectMultiplier.x, size.y * aspectMultiplier.y, 0);
}

void GUIImage::setImage(ALLEGRO_BITMAP* i)
{
	image = i;
	size = vec2(al_get_bitmap_width(image), al_get_bitmap_height(image));
	updateSizePlus();
	dirty = true;
}

void GUIImage::setReflection(bool r)
{
	reflection = r;
	dirty = true;
}

void GUIImage::updateSizePlus()
{
	sizePlus.x = size.x * aspectMultiplier.x + padding.x * 2;
	sizePlus.y = size.y * aspectMultiplier.y + padding.y * 2;
	dirty = true;
}
