/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIBindingButton.h"
#include "Game.h"

GUIBindingButton::GUIBindingButton()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = NULL;
	colour = al_map_rgb(255, 255, 255);
	colourActive = al_map_rgb(0, 255, 0);
	colourBad = al_map_rgb(255, 0, 0);
	text = "";
	inputType = -1;
	hasFocus = false;
	inputAction = NULL;
	action = NULL;
	leftAlign = true;
	active = false;
	conflict = false;
	visible = true;
	dirty = true;
	updateSizePlus();
}

GUIBindingButton::GUIBindingButton(ALLEGRO_FONT* _font, ALLEGRO_COLOR _colour, ALLEGRO_COLOR _colourActive, ALLEGRO_COLOR _colourBad, InputAction* _inputAction, int _inputType, ACTION a)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = _font;
	colour = _colour;
	colourActive = _colourActive;
	colourBad = _colourBad;
	inputAction = _inputAction;
	inputType = _inputType;
	updateText();
	hasFocus = false;
	action = a;
	leftAlign = true;
	active = false;
	conflict = false;
	visible = true;
	dirty = true;
}

GUIBindingButton::~GUIBindingButton()
{

}

void GUIBindingButton::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	//FIXME: The focus border shouldn't be outside the widget bounds, the filled rectangle should be smaller instead, and sizePlus should be calculated with the extra padding
	if (!visible || !dirty)
	{
		return;
	}
	if (conflict)
	{
		//FIXME: This ends up being redundant in some cases
		al_draw_filled_rectangle(positionOffset.x + positionPlus.x - 2, positionOffset.y + positionPlus.y + 2, positionOffset.x + positionPlus.x + sizePlus.x - 2, positionOffset.y + positionPlus.y + sizePlus.y - 2, colourBad);
	}
	if (hasFocus || isMouseOver || active)
	{
		al_draw_rectangle(positionOffset.x + positionPlus.x - 4, positionOffset.y + positionPlus.y, positionOffset.x + positionPlus.x + sizePlus.x, positionOffset.y + positionPlus.y + sizePlus.y , colour, 2);
	}

	if (active)
	{
		if (conflict)
		{
			al_draw_filled_rectangle(positionOffset.x + positionPlus.x - 2, positionOffset.y + positionPlus.y + 2, positionOffset.x + positionPlus.x + sizePlus.x - 2, positionOffset.y + positionPlus.y + sizePlus.y - 2, colourBad);
		}
		else
		{
			al_draw_filled_rectangle(positionOffset.x + positionPlus.x - 2, positionOffset.y + positionPlus.y + 2, positionOffset.x + positionPlus.x + sizePlus.x - 2, positionOffset.y + positionPlus.y + sizePlus.y - 2, colourActive);
		}
	}

	al_draw_text(font, colour, floor(positionOffset.x + positionPlus.x + padding.x + sizePlus.x * 0.5f - 2), positionOffset.y + positionPlus.y + padding.y, ALLEGRO_ALIGN_CENTRE, text.c_str());
}

void GUIBindingButton::updateText()
{
	text = inputAction->getBoundName(inputType);
	size = vec2(max(size.x, float(al_get_text_width(font, text.c_str()))), al_get_font_line_height(font));
	updateSizePlus();
	dirty = true;
}

void GUIBindingButton::setFont(ALLEGRO_FONT* _font, ALLEGRO_COLOR _colour)
{
	colour = _colour;
	dirty = true;
}

void GUIBindingButton::setFocusState(bool focus, bool silent)
{
	active = false;
	GUIWidget::setFocusState(focus, silent);
}

void GUIBindingButton::setConflict(bool c)
{
	conflict = c;
	dirty = true;
}

void GUIBindingButton::doAction()
{
	Game::getGame()->getInputMan()->setListenBinding(inputType);
	active = true;
	dirty = true;
}

void GUIBindingButton::cancelAction()
{
	Game::getGame()->getInputMan()->setListenBinding(-1);
	active = false;
	dirty = true;
}

void GUIBindingButton::applyAction()
{

	Game::getGame()->getInputMan()->setListenBinding(-1);
	active = false;
	updateText();
}
