/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUISlider.h"

GUISlider::GUISlider()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = NULL;
	color = al_map_rgb(255, 255, 255);
	text = (char*)"";
	hasFocus = false;
	action = NULL;
	leftAlign = true;
	visible = true;
	dirty = true;
	value = 0.0f;
	step = 0.1;
	toggleGrowth = 5;
	updateSizePlus();
}

GUISlider::GUISlider(ALLEGRO_FONT* _font, ALLEGRO_COLOR _color, const char* _text, ACTIONFLOAT a)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	font = _font;
	color = _color;
	setText(_text);
	hasFocus = false;
	action = a;
	leftAlign = true;
	visible = true;
	dirty = true;
	value = 0.0f;
	step = 0.1;
	toggleGrowth = 5;
}

GUISlider::~GUISlider()
{

}

void GUISlider::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible || !dirty)
	{
		return;
	}
	int lineY = positionOffset.y + positionPlus.y + size.y + padding.y + al_get_font_ascent(font);
	int boxX = positionOffset.x + positionPlus.x + (int)(size.x * value) - (int)(al_get_font_ascent(font) * 0.5);
	int boxXEnd = positionOffset.x + boxX + al_get_font_ascent(font);
	int textX = positionOffset.x + positionPlus.x;
	int textY = positionOffset.y + positionPlus.y + padding.y;
	int boxY = positionOffset.y + positionPlus.y + size.y + padding.y + (int)(al_get_font_ascent(font) * 0.5);
	int boxYEnd = positionOffset.y + boxY + al_get_font_ascent(font);

	if (!hasFocus && !isMouseOver)
	{
		boxX += toggleGrowth;
		boxY += toggleGrowth;
		boxXEnd -= toggleGrowth;
		boxYEnd -= toggleGrowth;
	}

	al_draw_filled_rectangle(boxX, boxY, boxXEnd, boxYEnd, color);
	al_draw_line(positionPlus.x, lineY, positionPlus.x + size.x, lineY, color, 2);

	al_draw_text(font, color, textX, textY, 0, text);
}

void GUISlider::setValue(float v)
{
	value = v;
	dirty = true;
}

float GUISlider::getValue()
{
	return value;
}

void GUISlider::doAction()
{
	action(value);
}

void GUISlider::updateSizePlus()
{
	if (size.y < 20)
	{
		toggleGrowth = 2;
	}
	else
	{
		toggleGrowth = 5;
	}

	sizePlus.x = size.x + padding.x * 2;
	sizePlus.y = size.y + padding.y * 3 + al_get_font_ascent(font);
	dirty = true;
}

bool GUISlider::doLeft()
{
	value -= step;
	if (value < 0)
	{
		value = 0;
	}
	doAction();
	dirty = true;
	return true;
}

bool GUISlider::doRight()
{
	value += step;
	if (value > 1)
	{
		value = 1;
	}
	doAction();
	dirty = true;
	return true;
}

void GUISlider::checkClickExtra(vec2 coords)
{
	float oldValue = value;
	value = (coords.x - positionPlus.x) / size.x;
	if (value != oldValue)
	{
		doAction();
		dirty = true;
	}
}
