/*
Copyright 2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUILine.h"

GUILine::GUILine()
{
	size = vec2(-1, -1);
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	colour = al_map_rgb(255, 255, 255);
	width = 2;
	visible = true;
	dirty = true;
}

GUILine::GUILine(ALLEGRO_COLOR _colour, int _width)
{
	position = vec2(-1, -1);
	sizePlus = vec2(-1, -1);
	positionPlus = vec2(-1, -1);
	width = _width;
	colour = _colour;
	visible = true;
	dirty = true;
	updateSizePlus();
}

GUILine::~GUILine()
{

}

void GUILine::draw(vec2 aspectMultiplier, vec2 positionOffset)
{
	if (!visible || !dirty)
	{
		return;
	}
	al_draw_line(positionOffset.x + positionPlus.x + padding.x, positionOffset.y + positionPlus.y + padding.y, positionOffset.x + positionPlus.x + sizePlus.x, positionOffset.y + positionPlus.y + sizePlus.y, colour, width);
}

