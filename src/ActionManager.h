/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

class ActionManager
{
	public:
		static ActionManager* getActionManager();

		//Menu funcs
		static void doQuit();
		static void doNewGame();
		static void doNewEasy();
		static void doNewNormal();
		static void doNewChallenging();
		static void doNewFreeSkate();
		static void doNewDebug();
		static void doResume();
		static void doWebsite();
		static void doSettingsFolder();
		static void doSettings();
		static void doBindings();
		static void doCredits();
		static void doBack();
		static void doEndGame();
		static void doHowToPlay();
		static void doStats();
		static void doResetSettings();
		static void doResetBindings();
		static void doKeyboardBindings();
		static void doGamepadBindings();
		static void doMouseBindings();
		static void doNothing();

		//Settings funcs
		static void updateAutoHeal(bool b);
		static void updateEnableJoystick(bool b);
		static void updateAxisDeadzone(float v);
		static void updateShowFPS(bool b);
		static void updateShowHints(bool b);
		static void updateFullscreen(bool b);

		static void updateMusicVolume(float v);
		static void updateEffectsVolume(float v);
		static void updateUIVolume(float v);
		static void updateGlobalVolume(float v);
		static void updateTurnSensitivity(float v);

	private:
		ActionManager();
		virtual ~ActionManager();
		static ActionManager* a;
};


#endif //ACTIONMANAGER
