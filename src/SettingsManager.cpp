/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <fstream>
#include <iostream>

#include "SettingsManager.h"
#include "Game.h"

SettingsManager::SettingsManager()
{
	ALLEGRO_PATH *savePath;
	savePath = al_get_standard_path(ALLEGRO_USER_SETTINGS_PATH);
	bool fallback = false;
	if (!savePath)
	{
		fallback = true;
	}
	else
	{
		if (!al_make_directory(al_path_cstr(savePath, ALLEGRO_NATIVE_PATH_SEP)))
		{
			fallback = true;
		}
	}

	if (fallback)
	{
		Alert(strerror(errno));
		settingsPath = SETTINGS_PATH_FALLBACK;
	}
	else
	{
		settingsPath = al_path_cstr(savePath, ALLEGRO_NATIVE_PATH_SEP);
	}
	Print("Using settings path: ", settingsPath);

	al_destroy_path(savePath);


	resX = DEFAULT_RES_X;
	resY = DEFAULT_RES_Y;
	fullscreenX = resX;
	fullscreenY = resY;

	loadSettingsDefaults(false);
	settingsDirty = false;
	bindingsDirty = false;
}

SettingsManager::~SettingsManager()
{
}

void SettingsManager::loadSettingsDefaults(bool save)
{
	//Graphics
	resX = DEFAULT_RES_X;
	resY = DEFAULT_RES_Y;
	fullscreenX = resX;
	fullscreenY = resY;
	fullscreen = false;
	targetFPS = DEFAULT_TARGET_FPS;

	//Audio
	//TODO: Maybe this should just use AudioManager's variables instead of doubling up?
	musicVolume = DEFAULT_MUSIC_VOLUME;
	effectsVolume = DEFAULT_EFFECTS_VOLUME;
	uiVolume = DEFAULT_UI_VOLUME;
	globalVolume = DEFAULT_GLOBAL_VOLUME;

	//Gameplay
	difficulty = 0;
	autoHeal = DEFAULT_AUTO_HEAL;
	showHints = DEFAULT_SHOW_HINTS;

	//Input
	turnSensitivity = DEFAULT_TURN_SENSITIVITY;
	enableJoystick = DEFAULT_ENABLE_JOYSTICK;
	joystickIndex = 0;
	axisDeadzone = DEFAULT_AXIS_DEADZONE;

	//Debug
	showFPS = false;

	settingsDirty = true;
	if (save)
	{
		applyAllSettings(); //FIXME: This is a slightly dodgy workaround for defaults being pre-loaded at startup as part of instiation. Applying settings before the settings manager has been instantiated is obviously a bad idea
		saveSettings();
	}
}

void SettingsManager::applyAllSettings()
{
	applyGlobalVolume();
	applyMusicVolume();
	applyEffectsVolume(true);
	applyUIVolume(true);
	applyFullscreen();
	applyFPS();
	applyAutoHeal();
	applyTurnSensitivity();
	applyEnableJoystick();
	applyJoystickIndex();
	applyAxisDeadzone();
	applyShowFPS();
	applyShowHints();
}

bool SettingsManager::readSetting(std::string setting, std::stringstream* s, std::string line, bool* v)
{
	setting = setting + " ";
	std::string temp;
	if (line.find(setting) == 0)
	{
		bool value;
		if ((*s) >> temp >> value)
		{
			*v = value;
			return true;
		}
		else
		{
			Print(" Got bad value for", line);
		}
	}
	return false;
}

bool SettingsManager::readSetting(std::string setting, std::stringstream* s, std::string line, int* v)
{
	setting = setting + " ";
	std::string temp;
	if (line.find(setting) == 0)
	{
		int value;
		if ((*s) >> temp >> value)
		{
			*v = value;
			return true;
		}
		else
		{
			Print(" Got bad value for", line);
		}
	}
	return false;
}

bool SettingsManager::readSetting(std::string setting, std::stringstream* s, std::string line, unsigned int* v)
{
	setting = setting + " ";
	std::string temp;
	if (line.find(setting) == 0)
	{
		int value;
		if ((*s) >> temp >> value)
		{
			*v = value;
			return true;
		}
		else
		{
			Print(" Got bad value for", line);
		}
	}
	return false;
}

bool SettingsManager::readSetting(std::string setting, std::stringstream* s, std::string line, float* v)
{
	setting = setting + " ";
	std::string temp;
	if (line.find(setting) == 0)
	{
		float value;
		if ((*s) >> temp >> value)
		{
			*v = value;
			return true;
		}
		else
		{
			Print(" Got bad value for", line);
		}
	}
	return false;
}


bool SettingsManager::loadSettings()
{
	Print("Loading settings from ", settingsPath + SAVE_FILE);
	ifstream settings;
	settings.open((settingsPath + SAVE_FILE).c_str());
	if (!settings.is_open())
	{
		Alert("Unable to read settings file. Trying fallback.");

		settings.open(((std::string)SETTINGS_PATH_FALLBACK + SAVE_FILE).c_str());
		if (!settings.is_open())
		{
			Error("Unable to read fallback settings file.");
			return false;
		}
	}
	std::string line;
	std::string garbage;

	if (std::getline(settings, line))
	{
		if (line.find("gameVersion ") != 0)
		{
			Print("Loading legacy settings file.");
		}
	}
	else
	{
		Print("Unable to check bindings file for version header.");
	}
	settings.clear();
	settings.seekg(0);

	while (std::getline(settings, line))
	{
		if (line.find("gameVersion ") == 0)
		{
			continue;
		}

		std::stringstream s(line);
		if (readSetting("resX", &s, line, &resX))
		{
			continue;
		}
		else if (readSetting("resY", &s, line, &resY))
		{
			continue;
		}
		else if (readSetting("fullscreen", &s, line, &fullscreen))
		{
			continue;
		}
		else if (readSetting("targetFPS", &s, line, &targetFPS))
		{
			continue;
		}

		else if (readSetting("globalVolume", &s, line, &globalVolume))
		{
			continue;
		}
		else if (readSetting("musicVolume", &s, line, &musicVolume))
		{
			continue;
		}
		else if (readSetting("effectsVolume", &s, line, &effectsVolume))
		{
			continue;
		}
		else if (readSetting("uiVolume", &s, line, &uiVolume))
		{
			continue;
		}

		else if (readSetting("autoHeal", &s, line, &autoHeal))
		{
			continue;
		}

		else if (readSetting("turnSensitivity", &s, line, &turnSensitivity))
		{
			continue;
		}

		else if (readSetting("enableJoystick", &s, line, &enableJoystick))
		{
			continue;
		}
		else if (readSetting("joystickIndex", &s, line, &joystickIndex))
		{
			continue;
		}
		else if (readSetting("axisDeadzone", &s, line, &axisDeadzone))
		{
			continue;
		}

		else if (readSetting("showFPS", &s, line, &showFPS))
		{
			continue;
		}

		else if (readSetting("showHints", &s, line, &showHints))
		{
			continue;
		}

		else
		{
			Print(" Unknown binding line:", line);
		}

	}
	settingsDirty = false;
	return true;
}

bool SettingsManager::saveSettings()
{
	if (!settingsDirty)
	{
		Print("Not saving settings. Settings are already current.");
		return false;
	}
	Print("Saving settings to", settingsPath + SAVE_FILE);
	ofstream settings;
	settings.open((settingsPath + SAVE_FILE).c_str(), ios::trunc);
	if (!settings.is_open())
	{
		Error("Unable to write to settings file.");
		return false;
	}

	settings << "gameVersion " << VERSION_NUMBER << std::endl;
	settings << "resX " << resX << std::endl;
	settings << "resY " << resY << std::endl;
	settings << "fullscreen " << fullscreen << std::endl;
	settings << "targetFPS " << targetFPS << std::endl;

	settings << "globalVolume " << globalVolume << std::endl;
	settings << "effectsVolume " << effectsVolume << std::endl;
	settings << "uiVolume " << uiVolume << std::endl;
	settings << "musicVolume " << musicVolume << std::endl;

	settings << "autoHeal " << autoHeal << std::endl;

	settings << "turnSensitivity " << turnSensitivity << std::endl;
	settings << "enableJoystick " << enableJoystick << std::endl;
	settings << "joystickIndex " << joystickIndex << std::endl;
	settings << "axisDeadzone " << axisDeadzone << std::endl;

	settings << "showFPS " << showFPS << std::endl;
	settings << "showHints " << showHints << std::endl;

	settings.flush();
	settings.close();
	settingsDirty = false;
	return true;
}

bool SettingsManager::backupBindings()
{
	std::string backupSuffix = "_bak"; //TODO: Use _YYYMMDD from modified date instead?

	Print("Backing up legacy bindings to ", settingsPath + BINDING_FILE + backupSuffix);
	ifstream bindings;
	bindings.open((settingsPath + BINDING_FILE).c_str());
	if (!bindings.is_open())
	{
		Error("Unable to read bindings file. Trying fallback.");

		bindings.open(((std::string)SETTINGS_PATH_FALLBACK + BINDING_FILE).c_str());
		if (!bindings.is_open())
		{
			Error("Unable to read fallback bindings file.");
			return false;
		}
		return false;
	}
	ofstream backup;
	backup.open((settingsPath + BINDING_FILE + backupSuffix).c_str(), ios::trunc);
	if (!bindings.is_open())
	{
		Error("Unable to write to bindings file.");
		return false;
	}

	std::string line;
	while (std::getline(bindings, line))
	{
		backup << line << std::endl;
	}
	backup.flush();
	backup.close();
	return true;
}

bool SettingsManager::loadBindings()
{

	Print("Loading bindings from ", settingsPath + BINDING_FILE);
	ifstream bindings;
	bindings.open((settingsPath + BINDING_FILE).c_str());
	if (!bindings.is_open())
	{
		Error("Unable to read bindings file. Trying fallback.");

		bindings.open(((std::string)SETTINGS_PATH_FALLBACK + BINDING_FILE).c_str());
		if (!bindings.is_open())
		{
			Error("Unable to read fallback bindings file.");
			return false;
		}
		return false;
	}
	std::string line;
	std::string garbage;
	int v = -1;
	bool found = false;

	if (std::getline(bindings, line))
	{
		if (line.find("GAME_VERSION ") != 0)
		{
			//TODO: Parse and separate version for more granular checks in the future
			bindings.close();
			bool success = loadBindings_v1_2();
			if (success)
			{
				success = backupBindings();
				if (success)
				{
					success = saveBindings();
				}
			}

			if (!success)
			{
				Print("Error: Unable to load or back up legacy bindings. Continuing");
			}
			return success;
		}
	}
	else
	{
		Print("Unable to check bindings file for version header.");
	}
	bindings.clear();
	bindings.seekg(0);

	while (std::getline(bindings, line))
	{
		if (line.find("GAME_VERSION ") == 0)
		{
			continue;
		}

		found = false;
		v = -1;
		std::stringstream s(line);
		for (unsigned int i = 0; i < INPUT_MAX; i++)
		{
			InputAction* input = Game::getGame()->getInputMan()->input[i];
			std::string outputName = removeSpaces(input->name);

			if (line.find(outputName) == 0)
			{
				if (s >> garbage >> v)
				{
					if (line.find("_KEY1 ") == outputName.length())
					{
						input->key1 = v;
						found = true;
					}
					else if (line.find("_KEY2 ") == outputName.length())
					{
						input->key2 = v;
						found = true;
					}
					else if (line.find("_GP1 ") == outputName.length())
					{
						input->gamepad1 = v;
						found = true;
					}
					else if (line.find("_GP1S ") == outputName.length())
					{
						input->gamepad1Stick = v;
						found = true;
					}
					else if (line.find("_GP1I ") == outputName.length())
					{
						input->gamepad1Invert = v;
						found = true;
					}
					else if (line.find("_GP2 ") == outputName.length())
					{
						input->gamepad2 = v;
						found = true;
					}
					else if (line.find("_GP2S ") == outputName.length())
					{
						input->gamepad2Stick = v;
						found = true;
					}
					else if (line.find("_GP2I ") == outputName.length())
					{
						input->gamepad2Invert = v;
						found = true;
					}
					else if (line.find("_M1 ") == outputName.length())
					{
						input->mouse1 = v;
						found = true;
					}
					else if (line.find("_M1A ") == outputName.length())
					{
						input->mouse1Axis = v;
						found = true;
					}
					else if (line.find("_M1I ") == outputName.length())
					{
						input->mouse1Invert = v;
						found = true;
					}
					else if (line.find("_M2 ") == outputName.length())
					{
						input->mouse2 = v;
						found = true;
					}
					else if (line.find("_M2A ") == outputName.length())
					{
						input->mouse2Axis = v;
						found = true;
					}
					else if (line.find("_M2I ") == outputName.length())
					{
						input->mouse2Invert = v;
						found = true;
					}
				}
				continue;
			}
		}
		if (!found)
		{
			Print(" Unknown binding line:", line);
		}
	}
	bindingsDirty = false;
	return true;
}


bool SettingsManager::loadBindings_v1_2()
{

	Print("Loading legacy v1.2 bindings from ", settingsPath + BINDING_FILE);
	ifstream bindings;
	bindings.open((settingsPath + BINDING_FILE).c_str());
	if (!bindings.is_open())
	{
		Error("Unable to read bindings file. Trying fallback.");

		bindings.open(((std::string)SETTINGS_PATH_FALLBACK + BINDING_FILE).c_str());
		if (!bindings.is_open())
		{
			Error("Unable to read fallback bindings file.");
			return false;
		}
		return false;
	}
	std::string line;
	std::string garbage;
	while (std::getline(bindings, line))
	{
		std::stringstream s(line);
		if (line.find("KEY_FORWARD ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_FORWARD]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_BACKWARD ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_BACKWARD]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_LEFT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_LEFT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_RIGHT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_RIGHT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_LOOKLEFT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_LOOKLEFT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_LOOKRIGHT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_LOOKRIGHT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_FIRE ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_FIRE]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_FIRE_ALT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_FIRE]->key2 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_HEAL ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_HEAL]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_HEAL_ALT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_HEAL]->key2 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_JUMP ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_JUMP]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_WEAPON1 ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON1]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_WEAPON2 ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON2]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_WEAPON3 ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON3]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_WEAPON4 ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON4]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_SWAP_SCENERY ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_SWAP_SCENERY]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_PAUSE ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_PAUSE]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_UI_UP ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_UI_UP]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_UI_DOWN ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_UI_DOWN]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_UI_LEFT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_UI_LEFT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_UI_RIGHT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_UI_RIGHT]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("KEY_GRAB_MOUSE ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_GRAB_MOUSE]->key1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}


		else if (line.find("BUTTON_FIRE ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_FIRE]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
//FIXME: Don't want to lose alt gamepad button bindings
/*		else if (line.find("BUTTON_FIRE_ALT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_FIRE]->buttonGamepad2 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
*/		else if (line.find("BUTTON_JUMP ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_JUMP]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("BUTTON_HEAL ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_HEAL]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("BUTTON_PAUSE ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_PAUSE]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("BUTTON_WEAPON_PREV ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON_PREV]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}
		else if (line.find("BUTTON_WEAPON_NEXT ") == 0)
		{
			int v;
			if (s >> garbage >> v)
			{
				Game::getGame()->getInputMan()->input[INPUT_WEAPON_NEXT]->gamepad1 = v;
			}
			else
			{
				Print(" Got bad value for", line);
			}
		}

		else
		{
			Print(" Unknown binding line:", line);
		}

	}
	bindingsDirty = false;
	return true;
}

bool SettingsManager::saveBindings()
{
/*
	//TODO: Maybe implement this?
	if (!bindingsDirty)
	{
		Print("Not saving bindings. Bettings are already current.");
		return false;
	}
*/
	Print("Saving bindings to", settingsPath + BINDING_FILE);
	ofstream bindings;
	bindings.open((settingsPath + BINDING_FILE).c_str(), ios::trunc);
	if (!bindings.is_open())
	{
		Error("Unable to write to bindings file.");
		return false;
	}
	bindings << "GAME_VERSION " << VERSION_NUMBER << std::endl;
	for (unsigned int i = 0; i < INPUT_MAX; i++)
	{
		InputAction* input = Game::getGame()->getInputMan()->input[i];
		std::string outputName = removeSpaces(input->name);

		if (input->key1 >= 0)
		{
			bindings << outputName << "_KEY1 " << input->key1 << std::endl;
		}
		if (input->key2 >= 0)
		{
			bindings << outputName << "_KEY2 " << input->key2 << std::endl;
		}
		if (input->gamepad1 >= 0)
		{
			bindings << outputName << "_GP1 " << input->gamepad1 << std::endl;
			if (input->gamepad1Stick >= 0)
			{
				bindings << outputName << "_GP1S " << input->gamepad1Stick << std::endl;
				bindings << outputName << "_GP1I " << input->gamepad1Invert << std::endl;
			}
		}
		if (input->gamepad2 >= 0)
		{
			bindings << outputName << "_GP2 " << input->gamepad2 << std::endl;
			if (input->gamepad2Stick >= 0)
			{
				bindings << outputName << "_GP2S " << input->gamepad2Stick << std::endl;
				bindings << outputName << "_GP2I " << input->gamepad2Invert << std::endl;
			}
		}
		if (input->mouse1 >= 0)
		{
			bindings << outputName << "_M1 " << input->mouse1 << std::endl;
			if (input->mouse1Axis)
			{
				bindings << outputName << "_M1A " << input->mouse1Axis << std::endl;
				bindings << outputName << "_M1I " << input->mouse1Invert << std::endl;
			}
		}
		if (input->mouse2 >= 0)
		{
			bindings << outputName << "_M2 " << input->mouse2 << std::endl;
			if (input->mouse2Axis)
			{
				bindings << outputName << "_M2A " << input->mouse2Axis << std::endl;
				bindings << outputName << "_M2I " << input->mouse2Invert << std::endl;
			}
		}
	}

	bindings.flush();
	bindings.close();
	bindingsDirty = false;
	return true;
}


void SettingsManager::setResX(unsigned int i)
{
	resX = i;
	settingsDirty = true;
}
unsigned int SettingsManager::getResX()
{
	if (fullscreen)
	{
		return fullscreenX;
	}
	else
	{
		return resX;
	}
}

void SettingsManager::setResY(unsigned int i)
{
	settingsDirty = true;
	resY = i;
}
unsigned int SettingsManager::getResY()
{
	if (fullscreen)
	{
		return fullscreenY;
	}
	else
	{
		return resY;
	}
}

void SettingsManager::setMusicVolume(float v)
{
	settingsDirty = true;
	musicVolume = v;
	applyMusicVolume();
}
float SettingsManager::getMusicVolume()
{
	return musicVolume;
}
void SettingsManager::applyMusicVolume()
{
	if (Game::getGame()->getAudioMan() != NULL)
	{
		Game::getGame()->getAudioMan()->setMusicVolume(musicVolume);
	}
}

void SettingsManager::setEffectsVolume(float v)
{
	settingsDirty = true;
	effectsVolume = v;
	applyEffectsVolume();
}
float SettingsManager::getEffectsVolume()
{
	return effectsVolume;
}
void SettingsManager::applyEffectsVolume(bool silent)
{
	if (Game::getGame()->getAudioMan() != NULL)
	{
		Game::getGame()->getAudioMan()->setEffectsVolume(effectsVolume, silent);
	}
}

void SettingsManager::setUIVolume(float v)
{
	settingsDirty = true;
	uiVolume = v;
	applyUIVolume();
}
float SettingsManager::getUIVolume()
{
	return uiVolume;
}
void SettingsManager::applyUIVolume(bool silent)
{
	if (Game::getGame()->getAudioMan() != NULL)
	{
		Game::getGame()->getAudioMan()->setUIVolume(uiVolume, silent);
	}
}

void SettingsManager::setGlobalVolume(float v)
{
	settingsDirty = true;
	globalVolume = v;
	applyGlobalVolume();
}
float SettingsManager::getGlobalVolume()
{
	return globalVolume;
}
void SettingsManager::applyGlobalVolume()
{
	if (Game::getGame()->getAudioMan() != NULL)
	{
		Game::getGame()->getAudioMan()->setGlobalVolume(globalVolume);
	}
}

void SettingsManager::setAutoHeal(bool b)
{
	settingsDirty = true;
	autoHeal = b;
	applyAutoHeal();
}
bool SettingsManager::getAutoHeal()
{
	return autoHeal;
}
void SettingsManager::applyAutoHeal()
{
	if (Game::getGame()->getPlayer() != NULL)
	{
		Game::getGame()->getPlayer()->setAutoHeal(autoHeal);
	}
}

void SettingsManager::setTurnSensitivity(float v)
{
	settingsDirty = true;
	turnSensitivity = v;
	applyTurnSensitivity();
	Game::getGame()->getAudioMan()->playUIUnintSound(Game::getGame()->getAudioMan()->sounds[SOUND_UI_HOVER_SHORT], 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, NULL);
}
float SettingsManager::getTurnSensitivity()
{
	return turnSensitivity;
}
void SettingsManager::applyTurnSensitivity()
{
	if (Game::getGame()->getPlayer() != NULL)
	{
		Game::getGame()->getPlayer()->setTurnSensitivity(turnSensitivity);
	}
}

void SettingsManager::setEnableJoystick(bool b)
{
	settingsDirty = true;
	enableJoystick = b;
	applyEnableJoystick();
}
bool SettingsManager::getEnableJoystick()
{
	return enableJoystick;
}
void SettingsManager::applyEnableJoystick()
{
	Game::getGame()->getInputMan()->initialiseJoystick();
}

void SettingsManager::setJoystickIndex(unsigned int i)
{
	settingsDirty = true;
	joystickIndex = i;
	applyJoystickIndex();
}
unsigned int SettingsManager::getJoystickIndex()
{
	return joystickIndex;
}
void SettingsManager::applyJoystickIndex()
{
	//Nothing to do \o/
	return;
}

void SettingsManager::setAxisDeadzone(float v)
{
	settingsDirty = true;
	axisDeadzone = v;
	applyAxisDeadzone();
	Game::getGame()->getAudioMan()->playUIUnintSound(Game::getGame()->getAudioMan()->sounds[SOUND_UI_HOVER_SHORT], 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, NULL);
}
float SettingsManager::getAxisDeadzone()
{
	return axisDeadzone;
}
void SettingsManager::applyAxisDeadzone()
{
	//Nothing to do \o/
	return;
}

void SettingsManager::setShowHints(bool b)
{
	settingsDirty = true;
	showHints = b;
	applyShowHints();
}
bool SettingsManager::getShowHints()
{
	return showHints;
}
void SettingsManager::applyShowHints()
{
	if (!showHints)
	{
		Game::getGame()->setHintVisibility(false);
	}
}

void SettingsManager::setFullscreen(bool b)
{
	settingsDirty = true;
	fullscreen = b;
	applyFullscreen();
}
bool SettingsManager::getFullscreen()
{
	return fullscreen;
}
void SettingsManager::applyFullscreen()
{
	Game::getGame()->getGraphicsMan()->setFullscreen(fullscreen);
	ALLEGRO_DISPLAY* display = Game::getGame()->getGraphicsMan()->getDisplay();
	if (fullscreen)
	{
		fullscreenX = al_get_display_width(display);
		fullscreenY = al_get_display_height(display);
		Game::getGame()->getGraphicsMan()->resolution_x = fullscreenX;
		Game::getGame()->getGraphicsMan()->resolution_y = fullscreenY;
	}
	else
	{
		Game::getGame()->getGraphicsMan()->resolution_x = resX;
		Game::getGame()->getGraphicsMan()->resolution_y = resY;
	}
	Game::getGame()->getGraphicsMan()->updateAspectMultiplier(false);
	if (Game::getGame()->currentScreen != NULL)
	{
		Game::getGame()->setupScreens();
	}
}

void SettingsManager::setShowFPS(bool b)
{
	settingsDirty = true;
	showFPS = b;
	applyShowFPS();
}
bool SettingsManager::getShowFPS()
{
	return showFPS;
}
void SettingsManager::applyShowFPS()
{
	Game::getGame()->setFPSVisibility(showFPS);
}

void SettingsManager::setFPS(unsigned int f)
{
	if (targetFPS != f)
	{
		settingsDirty = true;
		targetFPS = f;
		applyFPS();
	}
}
unsigned int SettingsManager::getFPS()
{
	return targetFPS;
}
void SettingsManager::applyFPS()
{
	Game::getGame()->setUpdateTimer(targetFPS);
}
