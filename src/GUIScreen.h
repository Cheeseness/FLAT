/*
Copyright 2021-2023 Josh "Cheeseness" Bush

This file is part of FLAT.

FLAT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FLAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FLAT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUISCREEN_H
#define GUISCREEN_H

#include <allegro5/allegro.h>

#include <vector>
#include <algorithm>

#include "GUIWidget.h"
#include "GUIScroller.h"
#include "GUILine.h"
#include "GUIButton.h"
#include "GUIBindingButton.h"
#include "GUIImage.h"
#include "GUISlider.h"
#include "GUIText.h"
#include "GUIToggle.h"

#define FOCUS_DIRECTION_LEFT 0
#define FOCUS_DIRECTION_RIGHT 1
#define FOCUS_DIRECTION_UP 2
#define FOCUS_DIRECTION_DOWN 3

class GUIScreen
{
	public:
		GUIScreen();
		GUIScreen(ALLEGRO_BITMAP* image);
		virtual ~GUIScreen();

		void draw(vec2 aspectMultiplier);
		void show();
		void close();
		bool isClosing();
		void addWidget(GUIWidget* w);
		GUIWidget* getFocusedWidget();
		int getFocusedWidgetIndex();
		void addFocusableWidget(GUIWidget* w);
		void setFocusedWidget(int i);
		void setFocusedWidget(GUIWidget* w);
		void focusNextWidget(int direction);
		void setDefaultFocusWidget(int i);
		void setDefaultFocusWidget(GUIWidget* w);
		void processMouse(vec2 pos, bool click);
		void doFocusedAction();
		bool isDirty();
		void setDirty(bool d);

	private:
		ALLEGRO_BITMAP* backgroundImage;
		std::vector <GUIWidget*> widgets;
		std::vector <GUIWidget*> focusableWidgets;
		int focusedWidget;
		int defaultFocusWidget;
		int clickedWidget;
		bool clickReady;
		bool dirty;
		bool closing;
};

#endif //GUISCREEN_H
