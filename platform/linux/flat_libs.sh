#!/bin/bash
# FLAT Launcher Shell Script
# Written by Ethan "flibitijibibo" Lee, adapted by Josh "Cheeseness" Bush

# Move to script's directory
cd `dirname $0`

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./libs/
./FLAT.x86_64 $@

