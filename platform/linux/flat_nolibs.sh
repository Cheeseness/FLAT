#!/bin/bash
# FLAT Launcher Shell Script
# Written by Ethan "flibitijibibo" Lee, adapted by Josh "Cheeseness" Bush
#
# Use this one if you don't want to make use of the bundled deps <3

# Move to script's directory
cd `dirname $0`

./FLAT.x86_64 $@

